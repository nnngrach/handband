// Чтобы сделать кнопку кастомной - присвоить ей этот класс

import UIKit

@IBDesignable
class HRButton: UIButton {
    
    
    
    // Закругление кнопки
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    
    // Толщина рамки вокруг кнопки
    @IBInspectable var borderWith: CGFloat = 0 {
        didSet {
            self.layer.borderWidth = borderWith
        }
    }
    
    
    // Цвет обводки
    @IBInspectable var borderColor: UIColor = UIColor.clear {
        didSet {
            self.layer.borderColor = borderColor.cgColor
        }
    }
    
    
    
    // Прозрачность тени под кнопкой
    @IBInspectable var shadowOpacity: Float = 0.0 {
        didSet {
            self.layer.shadowOpacity = shadowOpacity
        }
    }
    
    
    // Смещение тени
    @IBInspectable var shadowOffset: CGSize = CGSize(width: 0, height: 0) {
        didSet {
            self.layer.shadowOffset = shadowOffset
        }
    }
    
    
    // Размытие тени
    @IBInspectable var shadowCornerRadius: CGFloat = 0 {
        didSet {
            self.layer.shadowRadius = shadowCornerRadius
        }
    }
    
    
    // Цвет тени
    @IBInspectable var shadowColor: UIColor = UIColor.clear {
        didSet {
            self.layer.shadowColor = shadowColor.cgColor
        }
    }
    
    
    /// Отсекание по границе ???
    @IBInspectable var _clipsToBounds: Bool {
        set { clipsToBounds = newValue }
        get { return clipsToBounds }
    }
    
    
    
    
    
    // ====================================
    // Долговременное "вдавливание" и "отщелкивание" кнопок
    
    
    
    // Цвет который будет принимать нажатая кнопка
    @IBInspectable var pressedColor: UIColor = UIColor.black {
        didSet {
        }
    }
    
//    // Цвет который будет принимать обводка нажатой кнопка
//    @IBInspectable var pressedBorderColor: CGColor? = UIColor.black.cgColor {
//        didSet {
//        }
//    }

    // Цвет который будет принимать обводка нажатая кнопка
    @IBInspectable var pressedBorderColor: UIColor = UIColor.clear {
        didSet {
        }
    }
    
    
    
    
    
    // Изменение цвета при нажатии на кнопку
    
    var tempColor: UIColor = UIColor.blue
    var tempBorderColor: UIColor = UIColor.green
    // UIColor.black.cgColor
    
    
    override var isHighlighted: Bool {
        
        // сохранить установленный в настройках цвет
        willSet {
            if !isHighlighted {
                tempColor = self.backgroundColor!
                tempBorderColor = borderColor
            }
        }
        
        didSet {
            
            // при нажатии - заменить цвет
            if isHighlighted {
                self.backgroundColor = self.pressedColor
                self.layer.borderColor = self.pressedBorderColor.cgColor
                
            // при отпускании - вернуть сохраненное значение
            } else if !holdDown {
                self.backgroundColor = tempColor
                self.layer.borderColor = tempBorderColor.cgColor
            }
        }
    }
    
    
    
    
    
  
    // Вдавить и оставить нажатой
    var holdDown = false {
        didSet {
            if holdDown ==  true {
                //self.isEnabled = false
                self.backgroundColor = pressedColor
                //self.tintColor = pressedTintColor
                
            } else {
                //self.isEnabled = true
                self.backgroundColor = copyOffcolor
            }
        }
    }
    
    // Для обмена значениями между двумя переменными нужна треться
    // не смог никак сохранить ее, кроме как так
    // так что для утапливаемых кнопок пока что придется
    // в редакторе интерфейса устанавливать
    // один и тот цвет в двух полях
    // Background color  и в   CopyOfColor
    @IBInspectable var copyOffcolor: UIColor = UIColor.black {
        didSet {
            
        }
    }

}
