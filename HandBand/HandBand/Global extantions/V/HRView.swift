// Чтобы сделать вью кастомным - присвоить ему этот класс

import UIKit

@IBDesignable class HRView: UIView {
    
    
    // Закругление
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    
    
    // Толщина рамки вокруг
    @IBInspectable var borderWith: CGFloat = 0 {
        didSet {
            self.layer.borderWidth = borderWith
        }
    }
    
    // Цвет обводки
    @IBInspectable var borderColor: UIColor = UIColor.clear {
        didSet {
            self.layer.borderColor = borderColor.cgColor
        }
    }
    
    
    
    // Прозрачность тени
    @IBInspectable var shadowOpacity: Float = 0.0 {
        didSet {
            self.layer.shadowOpacity = shadowOpacity
        }
    }
    
    
    // Смещение тени
    @IBInspectable var shadowOffset: CGSize = CGSize(width: 0, height: 0) {
        didSet {
            self.layer.shadowOffset = shadowOffset
        }
    }
    
    
    // Размытие тени
    @IBInspectable var shadowCornerRadius: CGFloat = 0 {
        didSet {
            self.layer.shadowRadius = shadowCornerRadius
        }
    }
    
    
    // Цвет тени
    @IBInspectable var shadowColor: UIColor = UIColor.clear {
        didSet {
            self.layer.shadowColor = shadowColor.cgColor
        }
    }
    
    
    /// Отсекание по границе ???
    @IBInspectable var _clipsToBounds: Bool {
        set { clipsToBounds = newValue }
        get { return clipsToBounds }
    }
    
    
    
    
    
    
    /*
    // Настройки линейного градиента
    
    
    @IBInspectable var isLinear: Bool  = true {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var firstGradientColor: UIColor = UIColor.clear {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var secondGradientColor: UIColor = UIColor.clear {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var startPoint: CGPoint = CGPoint(x: 0.0, y: 1.0){
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var endPoint: CGPoint = CGPoint(x: 0.0, y: 0.0){
        didSet {
            updateView()
        }
    }
    
    override class var layerClass: AnyClass {
        get {
            return CAGradientLayer.self
        }
    }
    
    
    func updateView() {
        let layer = self.layer as! CAGradientLayer
        layer.colors = [ firstGradientColor.cgColor, secondGradientColor.cgColor ]
//        layer.startPoint = startPoint
//        layer.endPoint = endPoint
    }
    
    
    
    
     
     
     
     
     
    // Настройки радиального градиентв
    
    @IBInspectable var insideGradientColor: UIColor = .clear
    @IBInspectable var outsideGradientColor: UIColor = .clear
    
    
    override func draw(_ rect: CGRect) {
        let colors = [ insideGradientColor.cgColor, outsideGradientColor.cgColor ] as CFArray
        
        let gradient = CGGradient(colorsSpace: nil, colors: colors, locations: nil)
        
        if isLinear {
//            let layer = self.layer as! CAGradientLayer
//            layer.colors = [ firstGradientColor.cgColor, secondGradientColor.cgColor ]
//            layer.startPoint = startPoint
//            layer.endPoint = endPoint
            
            UIGraphicsGetCurrentContext()!.drawLinearGradient(gradient!, start: startPoint, end: endPoint, options: .drawsBeforeStartLocation)
            
        } else {
            
            let center = CGPoint(x: bounds.size.width/2, y: bounds.size.height/2)
            let endRadius = min(frame.width, frame.height) / 2      // можно заменить на MAX
            
            UIGraphicsGetCurrentContext()!.drawRadialGradient(gradient!, startCenter: center, startRadius: 0.0, endCenter: center, endRadius: endRadius, options: CGGradientDrawingOptions.drawsAfterEndLocation)
        }
        
        
    }
  */
    
    
}
