// Расширение строки: позволяет обращаться к каждому символу строки по его индексу
//
// "abcde"[0] == "a"
// "abcde"[0...2] == "abc"
// "abcde"[2..<4] == "cd"


import Foundation

extension String {
    
    subscript (i: Int) -> Character {
        return self[index(startIndex, offsetBy: i)]
    }
    
    subscript (i: Int) -> String {
        return String(self[i] as Character)
    }
    
    subscript (r: Range<Int>) -> String {
        var result = ""
        for i in r.lowerBound...r.upperBound {
            let charIndex = index(startIndex, offsetBy: i)
            result.append(self[charIndex])
        }
        return result
    }
    
    
    //    Backup: Dont work in Swift 4.2
    //    subscript (r: Range<Int>) -> String {
    //        let start = index(startIndex, offsetBy: r.lowerBound)
    //        let end = index(startIndex, offsetBy: r.upperBound)
    //        return String(self[Range(start ..< end)])
    //    }
}
