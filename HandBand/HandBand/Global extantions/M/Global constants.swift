//// Constant MIDI numbers
//
//let minMidiValue: UInt8 = 0
//let maxMidiValue: UInt8 = 127
//
//let firstNoteNubmer: UInt8 = 0
//let lastNoteNumber: UInt8 = 127
//
//let firstMidiChannel: UInt8 = 0
//let lastMidiChannel: UInt8 = 15
//
//let drumPadMode = 0
//
//let chordButtonsSoundsetNumber:Int = 8
//let WhiteKeysSoundset = 10
//let BlackKeysSoundset = 11
//
//let buttonsForTriggeringSoundsets = 0...7
//let buttonsForHotkeysAndChords = 8...15
//let plusButtonNumber = 11
//let minusButtonNumber = 14
//let sharpButtonNumber = 15
//
//
//
//
//// Diaposon of notes to detecting key combinations.
//// All another keys just plays melody.
//
//private let startNoteOfHotkeySector: UInt8 = 40
//private let endNoteOfHotkeySector: UInt8 = 60
//let diapasonOfHotkeyNotes = startNoteOfHotkeySector ..< endNoteOfHotkeySector
//
//
//
//// Names of all standart Midi Messages types
//
//enum midiCommandNames {
//    case noteOn
//    case noteOff
//    case polyPressute
//    case controllChange
//    case programmChange
//    case channelPressure
//}
//
//
//
//// Bool-like enum with 3 parametres
//// It will be use with HotKey Detection modules
//
//enum tripplePositionSwitcher {
//    case on
//    case any
//    case off
//}
//
//
//// Different helpers
//
//let mustBeZero: UInt8 = 0
