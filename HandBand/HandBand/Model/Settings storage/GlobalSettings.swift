// Хранилище общих для записи/загрузки общих настрек


import Foundation

final class GlobalSettings {
    static let singleton = GlobalSettings()
    let ud = UserDefaults.standard
    let disk = HRFiles.shared

    
    // MARK: - Init
    private init () {
        defaultInit()
        setModeLetter()
    }
    
    
    // =====================
    // MARK: - Меню настроек
    
    // Режим отображения: дрампады, фортепиано
    var setMode:Int {
        get {
            return UserDefaults.standard.integer(forKey: "Plaing mode")
        }
        set {
            UserDefaults.standard.set( newValue, forKey: "Plaing mode")
            setModeLetter()
            ScaleTransformer.singleton.calculateCurrentScale()
        }
    }
    
    // В зависимости от выбранного режима к названию файла изображения с аккордом будет прибавляться один символ
    var modeLetter = "d "
    func setModeLetter() {
        switch setMode {
        case 0, 1:
            modeLetter = "d "
        case 2:
            modeLetter = "h "
        default:
            modeLetter = "p "
        }
    }
    
    // Отображать клавиши левой руки
    var setLeft:Bool {
        get {
            return UserDefaults.standard.bool(forKey: "Setting Show Left Hand")
        }
        set {
            UserDefaults.standard.set( newValue, forKey: "Setting Show Left Hand")
        }
    }
    
    // Отображать клавиши правой руки
    var setRight:Bool {
        get {
            return UserDefaults.standard.bool(forKey: "Setting Show Right Hand")
        }
        set {
            UserDefaults.standard.set( newValue, forKey: "Setting Show Right Hand")
        }
    }
    
    // Разблокировать первый MIDI-канал
    var setUnclockFirstChannel:Bool {
        get {
            return UserDefaults.standard.bool(forKey: "Unlock Midi channel 1")
        }
        set {
            UserDefaults.standard.set( newValue, forKey: "Unlock Midi channel 1")
        }
    }
    
    // Проверка: оба тумблера могут быть выключены только при первом включении приложения
    func defaultInit() {
        if !setLeft && !setRight {
            setLeft = true
        }
    }
    


    
    // ===========================================
    // MARK: - Переменные для обработки интерфейса
    
    // Номер отображаемого на экране пресета
    var showingPreset = 0
    
    // Номер текущего пэда - его настройки будут отображаться на интерфейсе
    var showingPad = 0
    
    // Номер строки с инструментом на которую нажал пользователь
    var showingInstrument = 0
    
    
    // Включен или нет режим редактирования песни
    var isSongEditing = false {
        didSet {
            //print("IS SONG EDITING - ", isSongEditing)
        }
    }

    
    // Включен или нет режим перемещения строки песни
    var isSongStringMoving = false
    
    // Включено или нет меню выбора аккордов
    var isChordPicking = false
    
    // Строка и номер аккорда, для которого отображается меню
    var pickingChord = [0, 0]
    
    // Строка песенника, режим(1 - ноты, 2 -аккорды, 3 - октава), номер строки-октавы и номер ноты, для которой отображается меню
    var pickedNote = [0, 0, 0, 0]
    
   
    // показываем завершился ли кастомный переход
    var isOnStyleScreen = false
    
    // Строка Midi CC и номер поля (cc, value), для которых отображается пикер
    var pickingCC = [0, 0]
    
    
    
    // Октава для клавиш правой руки
    var keysOctave = 0
    
    // Текущее транспонирование для правой и левой руки
    var transpose:Int {
        get {
            return UserDefaults.standard.integer(forKey: "Transpose")
        }
        set {
            UserDefaults.standard.set( newValue, forKey: "Transpose")
            ScaleTransformer.singleton.calculateCurrentScale()
        }
    }
    
    
    // Переменная для скроллинга страниц в песеннике
    var songString:Int = 0 {
        didSet {
            let upLimit = HRFileOperations.shared.loadedSong.songStrings.count
            if songString < 0 {
                self.songString = 0
            } else if songString >= upLimit {
                songString = upLimit - 1
            }
            NotificationCenter.default.post(name: NSNotification.Name.init("Song string changed"),  object: nil)
        }
    }
    
    
    

    
    
//    // Обнуляем файл User Defalts
//    func erasePresetListDef () {
//        let domain = Bundle.main.bundleIdentifier!
//        UserDefaults.standard.removePersistentDomain(forName: domain)
//        UserDefaults.standard.synchronize()
//    }
//
//    // Распечатать содержиое User Defalts
//    func printUserDef () {
//        print(Array(UserDefaults.standard.dictionaryRepresentation().keys))
//    }
    
}
