// Структуры для базы данных с настройками

import Foundation


struct Song : Codable {
    
    var name:String
    var songStrings:[SongString]
    var presetList:[Preset]
    var createdAt:Date
    
    init() {
        name = "Default songname"
        songStrings = Array.init(repeating: SongString(), count: 16)
        presetList = Array.init(repeating: Preset(), count: 8)
        createdAt = Date()
    }
}




struct SongString : Codable {
    
    var mode:Int
    var text:String
    var chords:[ChordData]
    var notes:[OctaveRow]
    
    init() {
        mode    = 0
        text    = ""
        chords  = Array.init(repeating: ChordData(), count: 8)
        notes   = [OctaveRow()]
    }
}




struct OctaveRow : Codable {
    var label:String
    var notes:[Note]
    init() {
        label = "III"
        notes = [Note(label: "", position: 0, duration: 0)]
    }
}

struct Note : Codable {
    var label:String
    var position:Double
    var duration:Double
}



struct ChordData : Codable {
    var label:String
    var type:Int
    var chordTonality:Int
    var chordType:Int
    var preset:Int
    var transpose:Int
    
    init() {
        label           = ""    // надпись на миниатюре аккорда
        type            = 0     // 0 - пустой, 1 - аккорд, 2 - пресет, 3 - транспонирование
        chordTonality   = 0     // тональность аккорда
        chordType       = 0     // тип аккорда (минор, мажор итд)
        preset          = 0     // номер пресета
        transpose       = 0     // значение транспонирования
    }
}





struct Preset : Codable {
    
    var name:String
    var storeBankLetter:Int
    var storeBankNumber:Int
    var createdAt:Date
    
    var useMPE:Bool
    var useSmartScales:Bool
    var useSmartScaleForWhiteKeys:Bool
    var useSmartScaleForBlackKeys:Bool
    var useRegularScaleFowWhite:Bool
    var regularScaleNumber:Int
    
    var instrumentsMidiSets:[Instrument]
    
    init () {
        name                = "Preset with no name"
        storeBankLetter     = 0
        storeBankNumber     = 0
        createdAt           = Date()
        instrumentsMidiSets = [Instrument(number: 0)]
        useMPE                      = false
        useSmartScales              = true
        useSmartScaleForWhiteKeys   = true
        useSmartScaleForBlackKeys   = false
        useRegularScaleFowWhite     = false
        regularScaleNumber          = 0
    }

}





struct Instrument : Codable {
    
    var name:String
    var instrumentType:Int      // 0 - мелодический инструмент, 1 - ударные
    var rootOctave:Int
    var midiChannel:Int
    var noteRange:Int
    var useAutoInversion:Bool
    var maxVelocity:Float
    var midiCompressorMode:Int   // 0 - выключен, 1 - мягкий, 2 - жесткий, 3 - фиксированная громкость
    var usedCC: [toggleCC]
    var startPresetMidiMessages: [presetMidiCC]
    var pads:[PadInstrumentSettings]

    
    init (number:Int) {
        var labelNumber = 0
        number > 15  ?  (labelNumber = 16)  :  (labelNumber = (number + 1))
        
        name            = "Instrument \(labelNumber)"
        instrumentType  = 0
        rootOctave      = 2
        midiChannel     = number + 2
        maxVelocity     = 127
        midiCompressorMode = 0
        noteRange = 0
        useAutoInversion = true
    
        usedCC = [ toggleCC(name: "Send Sustain", isOn: true),            // генерировать собственный сустейн при нажатии на аккордовые пэды
                   toggleCC(name: "Use Sustain", isOn: true),             //  принимать сустейн с внешних устройств
                   toggleCC(name: "Use Pitchban", isOn: false),
                   toggleCC(name: "Use Modulation", isOn: false),
                   toggleCC(name: "Use Aftertouch", isOn: true),
                   toggleCC(name: "Use other MIDI CC messages", isOn: false) ]
        
        
        startPresetMidiMessages = [presetMidiCC(name: "Programm Change", type: 1, isOn: false, ccNumber: 0, value: 0),
                                   presetMidiCC(name: "Transpose", type: 2, isOn: false, ccNumber: 0, value: 0),
                                   presetMidiCC(name: "Modulation", type: 0, isOn: false, ccNumber: 1, value: 0),
                                   presetMidiCC(name: "Volume", type: 0, isOn: false, ccNumber: 7, value: 127),
                                   presetMidiCC(name: "Pan", type: 0, isOn: false, ccNumber: 10, value: 0),
                                   presetMidiCC(name: "Expression", type: 0, isOn: false, ccNumber: 11, value: 0),
                                   presetMidiCC(name: "Cutoff (Brightness)", type: 0, isOn: false, ccNumber: 74, value: 64),
                                   presetMidiCC(name: "Reverb (Effect 1)", type: 0, isOn: false, ccNumber: 91, value: 0),
                                   presetMidiCC(name: "Tremolo (Effect 2)", type: 0, isOn: false, ccNumber: 92, value: 0),
                                   presetMidiCC(name: "Chorus (Effect 3)", type: 0, isOn: false, ccNumber: 93, value: 0),
                                   presetMidiCC(name: "Phaser (Effect 4)", type: 0, isOn: false, ccNumber: 94, value: 0) ]
        
        pads = []
        for _ in 0 ... 11 {            // 8 пэдов, звук нажатия пресета, аккорда, клавиш (+1) !!!
            pads.append(PadInstrumentSettings())
        }
    }
}





struct toggleCC: Codable {
    var name:String
    var isOn:Bool
}



struct presetMidiCC: Codable {
    var name:String
    var type:Int            // 0 - MidiCC, 1 - ProgrammChange, 2 - Transpose
    var isOn:Bool
    
    var ccNumber:Int
    var value:Int
}



struct Pad : Codable {
    var instrumetsList: [PadInstrumentSettings]
    
    init() {
        instrumetsList = [PadInstrumentSettings()]
    }
}





struct PadInstrumentSettings : Codable {
    
    var isOn:Bool
    var style:Int
    var inversion:Int
    var relativeOctave:Int
    var relativeVelocity:Float
    
    var drumm1IsOn:Bool
    var drumm2IsOn:Bool
    var drumm3IsOn:Bool
    var drumm4IsOn:Bool
    
    var drumm1Note:Int
    var drumm2Note:Int
    var drumm3Note:Int
    var drumm4Note:Int
    
    init () {
        isOn             = true
        style            = 7
        inversion        = 0
        relativeOctave   = 2
        relativeVelocity = 100
        
        drumm1IsOn       = false
        drumm2IsOn       = false
        drumm3IsOn       = false
        drumm4IsOn       = false
        
        drumm1Note       = 36
        drumm2Note       = 42
        drumm3Note       = 38
        drumm4Note       = 46
    }
}
