import Foundation

class PianoRollFormatAdapter {
    let files = HRFileOperations.shared
    
    
    // Load note data
    
    func convertStoringToDisplayingNoteFormat(atRow: Int) -> [MIDITimeTableRowData] {
        var notesInPianoRollFormat: [MIDITimeTableRowData] = []
        let notesInStoredFormat = files.loadedSong.songStrings[atRow].notes
        
        for storedOctave in notesInStoredFormat {
            var newOctaveRow = MIDITimeTableRowData(
                cells: [],
                headerCellView: HeaderCellView(title: storedOctave.label),
                cellView: { cellData in
                    let title = cellData.data as? String ?? ""
                    return CellView(title: title)
            })
            
            for storedNote in storedOctave.notes {
                newOctaveRow.cells.append(MIDITimeTableCellData(data: storedNote.label,
                                                                position: storedNote.position,
                                                                duration: storedNote.duration))
            }
            
            notesInPianoRollFormat.append(newOctaveRow)
        }
        return notesInPianoRollFormat
    }
    
    
    
    
    // Store note data
    
    func convertDisplayingToStoringNoteFormat(atRow: Int, pianoRollData: [MIDITimeTableRowData]) {
        var notesInStoringFormat: [OctaveRow] = []
        
        for pianoRollRow in pianoRollData {
            var octaveToStoring = OctaveRow()
            
            for pianoRollCell in pianoRollRow.cells {
                octaveToStoring.notes.append(Note(label: "\(pianoRollCell.data)",
                                               position: rounding(value: pianoRollCell.position) ,
                                               duration: rounding(value: pianoRollCell.duration)))
            }
            notesInStoringFormat.append(octaveToStoring)
        }
        
        files.loadedSong.songStrings[atRow].notes = notesInStoringFormat
       // files.resaveSong()
    }
    
    
    
    
    
    // round all notes to 16th note lenght (1/4 of bar lenght)
    func rounding(value:Double) -> Double {
        let scale: Double = 4
        return round(value * scale) / scale
    }
    
}
