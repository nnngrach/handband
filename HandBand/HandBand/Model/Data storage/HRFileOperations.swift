// Комбинации функций файлового менеджера для решения задач этого приложения

import UIKit

final class HRFileOperations {
    static let shared = HRFileOperations()
    let sets = GlobalSettings.singleton
    let disk = HRFiles.shared
    let poster = NotificationCenter.default
    let postName = NSNotification.Name.self
    let songFile = "handband"
    let presetFile = "hbpreset"
    let instrumentFile = "hbinstrument"
    
    let queue = DispatchQueue.global(qos: .utility)
 
    
    private init () {
        viewPresetsFolder()
        viewAllSongFolders()
        viewSelectedSongFolder()
        viewInstrumentsFolder()
        loadSong()
        // disk.printDocsPatch()
        
        // Получатель уведомления что пресет изменился и пора обновить настройки
        NotificationCenter.default.addObserver(self, selector: #selector(updatePresetSettings(n:)), name: NSNotification.Name.init("PresetChanged"), object: nil)
    }

    
    
    

    // MARK: - Переменные файлового менеджера
    
    // Буфер для обмена настройками между интерфейсом и файлами на диске
    var loadedSong = Song ()

    
    // Содержимое папок для отображение на экране
    var instrumentFolderList:[String] = []
    var presetFolderList:[String] = []
    var songFoldersList:[String] = []
    var songList:[String] = []
    
    // Последняя загруженная папка
    var folder:String {
        get {
            return UserDefaults.standard.string(forKey: "Current song folder") ?? "Factory"
        }
        set {
            UserDefaults.standard.set( newValue, forKey: "Current song folder")
        }
    }
    
    
    // Последняя загруженная песня
    var songNumber:Int {
        get {
            return UserDefaults.standard.integer(forKey: "Current song")
        }
        set {
            UserDefaults.standard.set( newValue, forKey: "Current song")
        }
    }
    
    
    // Последний загруженный пресет
    var preset:Int {
        get {
            return UserDefaults.standard.integer(forKey: "Current preset")
        }
        set {
            UserDefaults.standard.set( newValue, forKey: "Current preset")
        }
    }
    
    // Последний загруженный инструмент
    var instrument:Int {
        get {
            return UserDefaults.standard.integer(forKey: "Current instrument")
        }
        set {
            UserDefaults.standard.set( newValue, forKey: "Current instrument")
        }
    }
    
    
    
    
    
    
    // MARK: - Просмотр файлов и папок
    
    func viewSelectedSongFolder() {
         songList = disk.viewDirecoryForFiletype(subFolder1: "Songs", subFolder2: folder, fileType: songFile)   ??  [""]
    }
    
    
    func viewAllSongFolders() {
        songFoldersList = disk.viewDirecory(subFolder1: "Songs", subFolder2: nil)  ??  [""]
    }
    
    func viewPresetsFolder() {
        presetFolderList = disk.viewDirecoryForFiletype(subFolder1: "Presets", subFolder2: nil, fileType: presetFile)   ??  [""]
    }
    
    func viewInstrumentsFolder() {
        instrumentFolderList = disk.viewDirecoryForFiletype(subFolder1: "Instruments", subFolder2: nil, fileType: instrumentFile)   ??  [""]
    }
    
    
    
    
    
    // MARK: - Загрузить
    
    // Загрузка песни
    func loadSongBy(number:Int){
        songNumber = number
        loadSong()
    }
    
    // В случае некорректных данных нужно сгенерировать дефолтную.
    func loadSong() {
        if songList.count > 0  &&  songNumber >= 0  &&  songNumber < (songList.count) {
            let name = songList[songNumber]
            
            if name.hasSuffix(songFile) {
                loadedSong = disk.load(fileName: name, subFolder1: "Songs", subFolder2: folder, type: Song.self)
            }
            
        } else {
            songNumber = 0
            loadedSong = Song()
        }
    }
    
   
    
    // Загрузить настройки пресета
    func loadPreset(index:Int) {
        let name = presetFolderList[index]
        
        if name.hasSuffix(presetFile) {
            loadedSong.presetList[preset] = disk.load(fileName: name, subFolder1: "Presets", subFolder2: nil, type: Preset.self)
        }
    }
    
    
    // Загрузить настройки инструмента
    func loadInstrument(index:Int) {
        let name = instrumentFolderList[index]
        
        if name.hasSuffix(instrumentFile) {
            loadedSong.presetList[preset].instrumentsMidiSets[sets.showingInstrument] = disk.load(fileName: name, subFolder1: "Instruments", subFolder2: nil, type: Instrument.self)
        }
    }
    
    
    
    
    
    // MARK: - Создать файл или папку
    func addSongFolder() {
        let name = renameTheCopy(subFolder1: "Songs", subFolder2: nil, fileName: "New folder", fileType: "")
        disk.createDirectory(subFolder: "Songs", folderName: name)
        viewAllSongFolders()
    }
    
    func addSong() {
        var newSong = Song()
        let name = renameTheCopy(subFolder1: "Songs", subFolder2: folder, fileName: "New song", fileType: songFile)
        newSong.name = removeSuffix(text: name, suffix: songFile)
        disk.save(object: newSong, fileName: name, subFolder1: "Songs", subFolder2: folder)
        viewSelectedSongFolder()
    }
    
    func addPreset () {
        var newPreset = Preset()
        let name = renameTheCopy(subFolder1: "Presets", subFolder2: nil, fileName: "New preset", fileType: presetFile)
        newPreset.name = removeSuffix(text: name, suffix: presetFile)
        disk.save(object: newPreset, fileName: name, subFolder1: "Presets", subFolder2: nil)
        viewPresetsFolder()
    }
    
    
    
    func addInstrument() {
        let number = 1
        var newInstrument = Instrument(number: number)
        let name = renameTheCopy(subFolder1: "Instruments", subFolder2: nil, fileName: "New instrument", fileType: instrumentFile)
        newInstrument.name = removeSuffix(text: name, suffix: instrumentFile)
        disk.save(object: newInstrument, fileName: name, subFolder1: "Instruments", subFolder2: nil)
        viewInstrumentsFolder()
    }
    
    
    func insertInstrument() {
        let number = loadedSong.presetList[preset].instrumentsMidiSets.count
        loadedSong.presetList[preset].instrumentsMidiSets.append( Instrument(number: number))
        resaveSong()
    }
    
    
    // Подобрать уникальное имя для нового файла
    func renameTheCopy(subFolder1: String?, subFolder2: String?, fileName: String, fileType: String) -> String {
        var newName = fileName + "." + fileType
        var counter = 1
        
        while disk.checkFile(subFolder1: subFolder1, subFolder2: subFolder2, fileName: newName) != nil {
            newName = fileName + " " + String(counter) + "." + fileType
            counter += 1
        }
        return newName
    }
    
    // Удаляет расширение файла и точку. Использует string extantion
    func removeSuffix(text:String, suffix:String) -> String {
        let lenght = text.count - suffix.count - 1
        return text[0 ..< lenght]
    }
    
    
    func renameCurrentSong(newName: String) {
        deleteSong(index: songNumber)
        loadedSong.name = newName
        resaveSong()
        viewSelectedSongFolder()
    }
    
    
    func renameSongFolder(oldName:String, newName:String) {
        disk.moveDirectory(subFolder: "Songs", oldFolderName: oldName, newFolderName: newName)
        folder = newName
        viewAllSongFolders()
    }
    
    
    
    
    
    
    // MARK: - Создать копию
    
    func dublicateCurrentSong() {
        var newSong = loadedSong
        var newName = loadedSong.name
        newName = renameTheCopy(subFolder1: "Songs", subFolder2: folder, fileName: newName, fileType: songFile)
        newSong.name = removeSuffix(text: newName, suffix: songFile)
        
        disk.save(object: newSong, fileName: newName, subFolder1: "Songs", subFolder2: folder)
        viewSelectedSongFolder()
    }
    
    func dublicatePreset() {
        var newPreset = loadedSong.presetList[preset]
        var name = newPreset.name
        name = renameTheCopy(subFolder1: "Presets", subFolder2: nil, fileName: name, fileType: presetFile)
        newPreset.name = removeSuffix(text: name, suffix: presetFile)
        
        disk.save(object: newPreset, fileName: name, subFolder1: "Presets", subFolder2: nil)
        viewPresetsFolder()
    }
    
    func exportPreset(with name:String) {
        var newPreset = loadedSong.presetList[preset]
        newPreset.name = name
        loadedSong.presetList[preset].name = name
        
        let fileName = name + "." + presetFile
        disk.save(object: newPreset, fileName: fileName, subFolder1: "Presets", subFolder2: nil)
        viewPresetsFolder()
        
        resaveSong()
    }
    
    func exportInstrument(with name:String) {
        var newInstumnt = loadedSong.presetList[preset].instrumentsMidiSets[sets.showingInstrument]
        newInstumnt.name = name
        loadedSong.presetList[preset].instrumentsMidiSets[sets.showingInstrument].name = name
        let fileName = name + "." + instrumentFile
        disk.save(object: newInstumnt, fileName: fileName, subFolder1: "Instruments", subFolder2: nil)
        viewInstrumentsFolder()
        resaveSong()
    }
    
    
    
    func dublicateInstrumentFile() {
        var newInstrument = loadedSong.presetList[preset].instrumentsMidiSets[sets.showingInstrument]
        var name = newInstrument.name
        name = renameTheCopy(subFolder1: "Instruments", subFolder2: nil, fileName: name, fileType: instrumentFile)
        newInstrument.name = removeSuffix(text: name, suffix: instrumentFile)
        
        disk.save(object: newInstrument, fileName: name, subFolder1: "Instruments", subFolder2: nil)
        viewInstrumentsFolder()
    }
    
    
    func dublicateInstrument() {
        let count = loadedSong.presetList[preset].instrumentsMidiSets.count
        loadedSong.presetList[preset].instrumentsMidiSets.append( Instrument(number: count))
        loadedSong.presetList[preset].instrumentsMidiSets[count] = loadedSong.presetList[preset].instrumentsMidiSets[sets.showingInstrument]
        resaveSong()
    }
    
    
    
    
    
    
    
    
    
    
    
    // MARK: - Сохранить
    func resaveSong() {
        let fileName = loadedSong.name + "." + songFile
        
        queue.async {
            self.disk.save(object: self.loadedSong, fileName: fileName, subFolder1: "Songs", subFolder2: self.folder)
            self.viewSelectedSongFolder()
            
            DispatchQueue.main.async {
                self.poster.post(name: self.postName.init("Song is saved"), object: nil)
            }
        }
    }
    
    
    
    
    
    // MARK: - Удалить
    func deleteSongFolder(index:Int) {
        let name = songFoldersList[index]
        disk.delete(fileName: name, subFolder1: "Songs", subFolder2: nil)
        songFoldersList.remove(at: index)
    }
    
    func deleteSong(index:Int) {
        let name = songList[index]
        disk.delete(fileName: name, subFolder1: "Songs", subFolder2: folder)
        viewSelectedSongFolder()
        
        if index <= songNumber {
            songNumber -= 1
        }
        
        if songList.count == 0 {
            folder = "Factory"
            songNumber = 0
        }
        
        loadSong()
    }
    
    func deletePreset(at index: Int) {
        let name = presetFolderList[index]
        disk.delete(fileName: name, subFolder1: "Presets", subFolder2: nil)
        viewPresetsFolder()
    }
    
    
    func deleteInstrumentFile(at index: Int) {
        let name = instrumentFolderList[index]
        disk.delete(fileName: name, subFolder1: "Instruments", subFolder2: nil)
        viewInstrumentsFolder()
        resaveSong()
    }
    
    func deleteInstrument(at index: Int) {
        loadedSong.presetList[preset].instrumentsMidiSets.remove(at: index)
        resaveSong()
    }

    
    
    
    
    
    
    // Действие при получении уведомления
    @objc func updatePresetSettings(n: NSNotification) {
    }

}
