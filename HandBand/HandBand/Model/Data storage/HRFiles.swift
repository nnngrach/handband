// Файловый менеджер - базовые функции

import Foundation
class HRFiles {
    static let shared = HRFiles()
    private init() {
        checkExistingDirectories()
        printDocsPatch()
        printBundlePatch()
    }
    
    let queue = DispatchQueue.global(qos: .utility)
    
    func printDocsPatch() {
        print(NSURL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]))
    }
    
    func printBundlePatch() {
        print(Bundle.main.resourceURL!)
    }
    
    
    
    
    // Сгенерировать путь до подпапки в директории Документы
    func patchTo(subFolder1:String?, subFolder2:String?) -> URL {
        var folderPatch = NSURL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
        //print(folderPatch)
        
        if subFolder1 != nil {
            folderPatch = folderPatch.appendingPathComponent(subFolder1!)! as NSURL
        }
        if subFolder2 != nil {
            folderPatch = folderPatch.appendingPathComponent(subFolder2!)! as NSURL
        }
        return folderPatch as URL
    }
    
    

    
    // Просмотреть содержимое папки (с сортировкой по дате создания)
    func viewDirecory(subFolder1:String?, subFolder2:String?) -> [String]? {
        let folderPatch = patchTo(subFolder1: subFolder1, subFolder2: subFolder2)
        let fm = FileManager.default
        

    
        if let filesInDirectory = try? fm.contentsOfDirectory(at: folderPatch,
                                                              includingPropertiesForKeys: [.contentModificationDateKey],
                                                              options:.skipsHiddenFiles) {

                                                                return filesInDirectory.map { url in
                                                                    (url.lastPathComponent, (try? url.resourceValues(forKeys: [.contentModificationDateKey]))?.contentModificationDate ?? Date.distantPast)
                                                                    }
                                                                    .sorted(by: { $0.1 > $1.1 }) // sort descending modification dates
                                                                    .map { $0.0 } // extract file names

        } else {
            return nil
        }
    }
    
 
    
    // Просмотреть содержимое папки (с сортировкой по дате создания и отфильтровывая файлы нужного расширения
    func viewDirecoryForFiletype(subFolder1:String?, subFolder2:String?, fileType:String) -> [String]? {
        let folderPatch = patchTo(subFolder1: subFolder1, subFolder2: subFolder2)
        let fm = FileManager.default
        
        if let filesInDirectory = try? fm.contentsOfDirectory(at: folderPatch,
                                                              includingPropertiesForKeys: [.contentModificationDateKey],
                                                              options:.skipsHiddenFiles) {
            
            return filesInDirectory.filter{ $0.lastPathComponent.hasSuffix(fileType) }.map { url in
                (url.lastPathComponent, (try? url.resourceValues(forKeys: [.contentModificationDateKey]))?.contentModificationDate ?? Date.distantPast)
                }
                .sorted(by: { $0.1 > $1.1 }) // sort descending modification dates
                .map { $0.0 } // extract file names
            
        } else {
            return nil
        }
    }

    
    
    
    // Проверить наличие файла или папки по указанному адресу
    func checkFile(subFolder1:String?, subFolder2:String?, fileName:String) -> String? {
        do {
            let folderPatch = patchTo(subFolder1: subFolder1, subFolder2: subFolder2)
            let filesInDirectory = try FileManager.default.contentsOfDirectory(atPath: folderPatch.path)
           
            if filesInDirectory.count > 0 {
                if filesInDirectory.contains(fileName) {
                    //print(fileName, " найден")
                    return fileName

                } else {
                    //print("Файл не обнаружен")
                    return nil
                }
            }
        } catch let error as NSError {
            print(error)
        }
        return nil
    }
    
    
    
    
    
    // MARK: - Работа с директориями
    
    // Создать подпапку в директории Документы
    func createDirectory(subFolder:String?, folderName:String) {
        let folderPatch = patchTo(subFolder1: subFolder, subFolder2: folderName)
        
        do {
            try FileManager.default.createDirectory(atPath: folderPatch.path, withIntermediateDirectories: true, attributes: nil)
        } catch let error as NSError {
            NSLog("Unable to create directory \(error.debugDescription)")
        }
    }
    
    
    // Переместить или переименовать папку
    func moveDirectory(subFolder:String?, oldFolderName:String, newFolderName:String) {
        let oldFolderPatch = patchTo(subFolder1: subFolder, subFolder2: oldFolderName).path
        let newFolderPatch = patchTo(subFolder1: subFolder, subFolder2: newFolderName).path
        
        do{
            try FileManager.default.moveItem(atPath: oldFolderPatch, toPath: newFolderPatch)
        } catch   {
            print("error")
        }
    }
    
    
    // Удалить подпапку в директории Документы
    func deleteDirectory(subFolder:String?, folderName:String) {
        let folderPatch = patchTo(subFolder1: subFolder, subFolder2: folderName)
        
        do {
            try FileManager.default.removeItem(at: folderPatch)
        } catch let error as NSError {
            NSLog("Unable to create directory \(error.debugDescription)")
        }
    }
    
    
    
    

    
    
    // MARK: - Чтение и запись файлов
    
    // Сохранить в файл данные в формате Data
    func saveData (data:Data, fileName:String, subFolder1:String?, subFolder2:String?) {
        
        let folderPatch = patchTo(subFolder1: subFolder1, subFolder2: subFolder2)
        let url = folderPatch.appendingPathComponent(fileName, isDirectory: false)
        
        do {
            if FileManager.default.fileExists(atPath: url.path) {
                try FileManager.default.removeItem(at: url)
            }
            FileManager.default.createFile(atPath: url.path, contents: data, attributes: nil)

        }catch{
            fatalError(error.localizedDescription)
        }
    }
    
    
    // Закодировать и сохранить объект протокала Codable
    func save <T:Encodable> (object:T, fileName:String, subFolder1:String?, subFolder2:String?) {
        let folderPatch = patchTo(subFolder1: subFolder1, subFolder2: subFolder2)
        let url = folderPatch.appendingPathComponent(fileName, isDirectory: false)
        
        let encoder = JSONEncoder()
        
        do {
            let data = try encoder.encode(object)
            if FileManager.default.fileExists(atPath: url.path) {
                try FileManager.default.removeItem(at: url)
            }
            FileManager.default.createFile(atPath: url.path, contents: data, attributes: nil)
            
        }catch{
            fatalError(error.localizedDescription)
        }
    }
    
    
    
    
    
    // Раскодировать и загрузить объект протокала Codable
    func load <T:Decodable> (fileName:String, subFolder1:String?, subFolder2:String?, type:T.Type) -> T {
        let folderPatch = patchTo(subFolder1: subFolder1, subFolder2: subFolder2)
        let url = folderPatch.appendingPathComponent(fileName, isDirectory: false)
        
        if !FileManager.default.fileExists(atPath: url.path) {
            fatalError("File not found at path \(url.path)")
        }
        
        if let data = FileManager.default.contents(atPath: url.path) {
            do {
                let model = try JSONDecoder().decode(type, from: data)
                return model
            }catch{
                return Song() as! T
                //fatalError(error.localizedDescription)
            }
            
        }else{
            fatalError("Data unavailable at path \(url.path)")
        }
    }
    
    
    
    // Раскодировать объект протокала Codable и загрузить данные в формате Data
    func loadData (fileName:String, subFolder1:String?, subFolder2:String?) -> Data? {
        let folderPatch = patchTo(subFolder1: subFolder1, subFolder2: subFolder2)
        let url = folderPatch.appendingPathComponent(fileName, isDirectory: false)
        print(url)
        
        if !FileManager.default.fileExists(atPath: url.path) {
            fatalError("File not found at path \(url.path)")
        }
        
        if let data = FileManager.default.contents(atPath: url.path) {
            return data
            
        }else{
            fatalError("Data unavailable at path \(url.path)")
        }
        
    }
    
    
    
    
    // Раскодировать файлы в папке и вернуть массив из полученных объектов
    func loadAll <T:Decodable> (type:T.Type, subFolder1:String?, subFolder2:String?) -> [T] {
        do {
            let folderPatch = patchTo(subFolder1: subFolder1, subFolder2: subFolder2)
            let files = try FileManager.default.contentsOfDirectory(atPath: folderPatch.path)
            
            var modelObjects = [T]()
            
            for fileName in files {
                modelObjects.append(load(fileName: fileName, subFolder1: subFolder1, subFolder2: subFolder2, type: type))
            }
            
            return modelObjects
            
            
        }catch{
            fatalError("could not load any files")
        }
    }
    
    
    
    // Удалить файл
    func delete (fileName:String, subFolder1:String?, subFolder2:String?) {
        let folderPatch = patchTo(subFolder1: subFolder1, subFolder2: subFolder2)
        let url = folderPatch.appendingPathComponent(fileName, isDirectory: false)
        
        if FileManager.default.fileExists(atPath: url.path) {
            do {
                try FileManager.default.removeItem(at: url)
            }catch{
                fatalError(error.localizedDescription)
            }
        }
    }
    
    
    
    
    
    
    
    // MARK: - Проверка файлов при запуске
    
    
    // действия при первом включении программы - проверка каталогов
    func checkExistingDirectories() {
        
        // если папка с пресетами или песнями еще не создана, то
        // создать папку со стандартным набором пресетов или песен
        
        if checkFile(subFolder1: nil, subFolder2: nil, fileName: "Presets") == nil {
            createDirectory(subFolder: nil, folderName: "Presets")
            copyFromBundle(bundleFolder: "Presets", copySubfolder: nil, copyFolder: "Presets")
        }
        
        if checkFile(subFolder1: nil, subFolder2: nil, fileName: "Instruments") == nil {
            createDirectory(subFolder: nil, folderName: "Instruments")
            copyFromBundle(bundleFolder: "Instruments", copySubfolder: nil, copyFolder: "Instruments")
        }

        if checkFile(subFolder1: "Songs", subFolder2: nil, fileName: "Factory") == nil {
            createDirectory(subFolder: "Songs", folderName: "Factory")
            copyFromBundle(bundleFolder: "Songs", copySubfolder: "Songs", copyFolder: "Factory")
            UserDefaults.standard.set("Factory", forKey: "Current song folder")
            UserDefaults.standard.set("Default Song", forKey: "Current song")
            UserDefaults.standard.set(0, forKey: "Current preset")
        }
    }
    
    
    
    
    
    
    // скопировать файлы с дефолтными настройками
    // из папки Bundle в папку Documents
    func copyFromBundle(bundleFolder:String, copySubfolder:String?, copyFolder:String) {
        let filemgr = FileManager.default
        filemgr.delegate = self as? FileManagerDelegate
        let dirPaths = filemgr.urls(for: .documentDirectory, in: .userDomainMask)
        let docsURL = dirPaths[0]
        var docsFolder = ""
        
        if copySubfolder != nil {
            let subPatch = copySubfolder! + "/" + copyFolder
            docsFolder = docsURL.appendingPathComponent(subPatch).path
        } else {
            docsFolder = docsURL.appendingPathComponent(copyFolder).path
        }
        
        let folderPath = Bundle.main.resourceURL!.appendingPathComponent(bundleFolder).path
        copyFiles(pathFromBundle: folderPath, pathDestDocs: docsFolder)
    }
    
    
    private func copyFiles(pathFromBundle : String, pathDestDocs: String) {
        let fileManagerIs = FileManager.default
        fileManagerIs.delegate = self as? FileManagerDelegate
        
        do {
            let filelist = try fileManagerIs.contentsOfDirectory(atPath: pathFromBundle)
            try? fileManagerIs.copyItem(atPath: pathFromBundle, toPath: pathDestDocs)
            
            for filename in filelist {
                try? fileManagerIs.copyItem(atPath: "\(pathFromBundle)/\(filename)", toPath: "\(pathDestDocs)/\(filename)")
            }
        } catch {
            print("\nError\n")
        }
    }
    
    
    
    
    
//    Чтобы при создании дубликатов файлов получались уникальные имена
//    можно добавлять к названию время и дату создания
//    func getTime() -> String {
//        let formatter = DateFormatter()
//        formatter.dateFormat = "dd-MM-yy  HH-mm-ss"
//        return formatter.string(from: Date())
//    }
}
