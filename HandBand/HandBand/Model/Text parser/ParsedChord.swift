//
//  ParsedChords.swift
//  HandBand
//
//  Created by HR_book on 11.05.2018.
//  Copyright © 2018 H.Rach. All rights reserved.
//

import Foundation

struct ParsedChord {
    var label: String = ""
    var type: Int = 0
    var tonality: Int = 0
}
