// Парсит тексты песен. Отделяет слова от аккордов.
import Foundation

final class HRSongTextParser {
    static let shared = HRSongTextParser()
    private init() {}
    let sets = GlobalSettings.singleton
    let files = HRFileOperations.shared
    let k = HRKeyEquality.singleton
    
    // режим обработки аккордов - задается через интерфейс
    var isH = true
    
    
    
    // MARK: - Программа распознавания текста и аккордов
    
    func analyze(text: String) {
        
        // обнулить все имеющиеся строки песенника
        files.loadedSong.songStrings = []
        
        // разбить входящий текст по строкам
        let byStringArray = text.components(separatedBy: "\n")
        
        
        // для каждой строки
        for i in 0 ..< byStringArray.count {
            
            // добавить ее текст в песенник
            files.loadedSong.songStrings.append(SongString())
            files.loadedSong.songStrings[i].text  = String(byStringArray[i])
            
            // провести распознание аккорда
            let chords = chordFinder(in: byStringArray[i])
            if chords.count > 0 {
                
                // перевести строку песенника в режим отображения аккордов
                files.loadedSong.songStrings[i].mode = 1
                
                // добавить аккорды к данным строки
                for j in 0 ..< 8 {
                    let chordData = chords[j]
                    
                    files.loadedSong.songStrings[i].chords[j].label = chordData.label
                    
                    if chordData.label != "" {
                        files.loadedSong.songStrings[i].chords[j].type = 1
                        files.loadedSong.songStrings[i].chords[j].chordType = chordData.type
                        files.loadedSong.songStrings[i].chords[j].chordTonality = chordData.tonality
                    } else {
                        files.loadedSong.songStrings[i].chords[j].type = 0
                    }
                    
                    
//                    sets.loadedSong.songStrings[i].chords[j].type = 1
//
//                    if chords[j] != "???" {
//                        sets.loadedSong.songStrings[i].chords[j].type = 1
//                    }
                    
                }
            }
        }
        
        
        // сохранить распознанную песню
        NotificationCenter.default.post(name: NSNotification.Name.init("Song Text Updated"),  object: nil)
        files.resaveSong()
    }
    
    
    
  // ========================================================
    
    
    
    
    
    
    // MARK: - Распознать аккорды в строке
    private func chordFinder(in text: String) -> [ParsedChord] {
        // Символы, которые содержатся в аккордах
        let charSet: Set<String> = ["A", "B", "C", "D", "E", "F", "G", "H",
                                    "M", "m", "s", "u", "d", "i", "a", "u", "g", "j", "b",
                                    "0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
                                    "#", "+", "-", "|", "/" ,"°", "o", " "]
        
        
        
        var result = [ParsedChord]()
        var chatactersOk = true
        
        // отсекаем строки с неаккордовыми символами
        for character in text {
            if !charSet.contains( String(character)) {
                chatactersOk = false
                break
            }
        }
        
        
        if chatactersOk  &&  text.count > 0 {
            
            // чистим от двойных пробелов, уточняем значение аккорда "B" и разделяем по словам
            let clearString = text.deleteDoubleSpaces()
            let modifedString = editChordB(isH: self.isH, text: clearString)
            let byWordsArray = modifedString.components(separatedBy: NSCharacterSet.whitespacesAndNewlines)
            
            if byWordsArray.count > 8 {
                // ====== вывести алерт!
                print("В следующей строке найдено более 8 аккордо. Разделите эту строку на две и запустите анализ повторно.")
                print(text)
            }
            
            
            let quantazedArray = quantizeChordsWithCount(rawArray: byWordsArray)
            result = chordIdentity(rawArray: quantazedArray)
        }
        
        return result
    }
    
    
    
    
    
    
    
    // MARK: - Уточить значение аккордов B и H
    private func editChordB (isH: Bool, text: String) -> String {
        var result = ""
        for char in text {
            
            if char == "B" {
                if isH {
                    result.append(Character("B"))
                } else{
                    result.append(Character("A"))
                    result.append(Character("#"))
                }
                
            } else if char == "H" {
                result.append(Character("B"))
                
            } else {
                result.append(char)
            }
        }
        return result
    }
    
    
    
    

    

    
    
    // MARK: - Распределить найденные аккорды
    // в зависимости от их количества
    private func quantizeChordsWithCount(rawArray: [String]) -> [String] {
        var resultArray = Array.init(repeating: "", count: 8)
        
        switch rawArray.count {
        case 1:
            resultArray[0] = rawArray[0]
        case 2:
            resultArray[0] = rawArray[0]
            resultArray[4] = rawArray[1]
        case 3:
            resultArray[0] = rawArray[0]
            resultArray[3] = rawArray[1]
            resultArray[6] = rawArray[2]
        case 4:
            resultArray[0] = rawArray[0]
            resultArray[2] = rawArray[1]
            resultArray[4] = rawArray[2]
            resultArray[6] = rawArray[3]
        case 5:
            resultArray[0] = rawArray[0]
            resultArray[2] = rawArray[1]
            resultArray[4] = rawArray[2]
            resultArray[5] = rawArray[3]
            resultArray[7] = rawArray[4]
        case 6:
            resultArray[0] = rawArray[0]
            resultArray[1] = rawArray[1]
            resultArray[3] = rawArray[2]
            resultArray[4] = rawArray[3]
            resultArray[6] = rawArray[4]
            resultArray[7] = rawArray[5]
            
        case 7...8:
            for i in 0 ..< rawArray.count {
                resultArray[i] = rawArray[i]
            }
        default:
            print("Ошибка распределения распознанных аккордов")
        }
        return resultArray
    }
   
    
   
    
    
    // MARK: - Опознать аккорды по базе
    // [название аккорда, тип, тональность]
    private func chordIdentity(rawArray: [String]) -> [ParsedChord] {
//        var result = Array(repeating: ["", 0, 0], count: 8)
        var result = Array(repeating: ParsedChord(), count: 8)
        
        for i in 0 ..< rawArray.count {
            
            if rawArray[i] != "" && rawArray[i] != "|" {
                if k.comboChords.keys.contains(rawArray[i]) {
                    result[i].label = rawArray[i]
                    result[i].type = k.comboChords[rawArray[i]]![1] as! Int
                    result[i].tonality = k.comboChords[rawArray[i]]![2] as! Int
                } else {
                    result[i].label = "???"
                }
            }
        }
        return result
    }
    
//    private func chordIdentity(rawArray: [String]) -> [String] {
//        var result = rawArray
//
//        for i in 0 ..< rawArray.count {
//
//            if rawArray[i] != "" && rawArray[i] != "|" {
//                if !k.combo.keys.contains(rawArray[i]) {
//                    result[i] = "???"
//                }
//            }
//
//        }
//
//        return result
//    }
    
    
    
    

}







// MARK: - Удалить двойные пробелы
extension String {
    func deleteDoubleSpaces() -> String {
        let components = self.components(separatedBy: NSCharacterSet.whitespacesAndNewlines)
        return components.filter { !$0.isEmpty }.joined(separator: " ")
    }
}
