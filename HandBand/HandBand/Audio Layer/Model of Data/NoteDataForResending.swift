import Foundation

struct NoteDataForResending {
    var noteNumber: UInt8?
    var noteNumberBeforeProcessing: UInt8?
    
    var velocity: UInt8
    var channel: UInt8?
    
    var comboKeyNumber: Int?
}
