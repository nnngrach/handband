// Музыкальные константы
// Буду ссылаться на них чтобы избегать "волшебных чисел"

import Foundation

class MusicDataBase {
    
    // Constant MIDI numbers
    
    let firstMidiChannel: UInt8 = 0
    let lastMidiChannel: UInt8 = 15
    
    let minMidiValue: UInt8 = 0
    let maxMidiValue: UInt8 = 127
    
    let firstNoteNubmer: UInt8 = 0
    let lastNoteNumber: UInt8 = 127
    
    
    let diapasonOfHotkeyNotes: (Range<UInt8>) = (40 ..< 60)
    let diapasonOfScreenMelodycKeys: (Range<UInt8>) = (60 ..< 73)
    let diapasonOfMelodycKeys: (Range<UInt8>) = (60 ..< 128)
    
    let drumPadMode = 0
    let twoOctaveKeyboardsMode = 2
    
    let chordButtonsSoundset = 8
    let presetTriggeringSoundset = 9
    let whiteMelodycKeysSoundset = 10
    let blackMelodycKeysSoundset = 11
    
    let buttonsForTriggeringSoundsets = 0...7
    let buttonsForHotkeysAndChords = 8...15
    let plusButtonNumber = 11
    let minusButtonNumber = 14
    let sharpButtonNumber = 15
    
    let velocityForScreenButtons: UInt8 = 110
    let mustBeZero: UInt8 = 0
    


    
    
    
    // Names of all standart Midi Messages types
    
    enum midiCommandNames {
        case noteOn
        case noteOff
        case polyPressute
        case controllChange
        case programmChange
        case channelPressure
    }
    
    
    
    // Bool-like enum with 3 parametres
    // It will be use with HotKey Detection modules
    
    enum tripplePositionSwitcher {
        case on
        case any
        case off
    }
    
}



