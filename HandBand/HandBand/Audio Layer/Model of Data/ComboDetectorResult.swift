import Foundation

struct ComboDetectorResult {
    let message: String?
    let isSounding: Bool
    let tonality: Int?
    let chordType: Int?
}
