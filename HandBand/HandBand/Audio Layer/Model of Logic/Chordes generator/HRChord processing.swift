// Алгоритм генерации аккордовых нот
import Foundation
extension NoteGenerator {

    func chordProcessing(soundSet: Int, playVelocity: UInt8, notenumber: UInt8?) -> [[UInt8]] {
        
        // номер пресета с которым будем работать
        let preset = files.preset
        var generatedNotes = [[UInt8]]()
        
        
        // пройтись по списку со всеми дорожками этого пэда
        for i in 0 ..< files.loadedSong.presetList[preset].instrumentsMidiSets.count {
            
            // для тех, которые включены произвести сгенерировать аккора
            if files.loadedSong.presetList[preset].instrumentsMidiSets[i].pads[soundSet].isOn {
                
                
                // собрать настройки активного иструмента
                let tonality =      self.tonality
                let keysOctave =    sets.keysOctave
                let liveTranspose = UserDefaults.standard.integer(forKey: "Transpose")
                
                let type =           files.loadedSong.presetList[preset].instrumentsMidiSets[i].instrumentType
                let channel =        UInt8(files.loadedSong.presetList[preset].instrumentsMidiSets[i].midiChannel)
                let rootOctave =     files.loadedSong.presetList[preset].instrumentsMidiSets[i].rootOctave
                let maxVelocity =    files.loadedSong.presetList[preset].instrumentsMidiSets[i].maxVelocity
                let compressorMode = files.loadedSong.presetList[preset].instrumentsMidiSets[i].midiCompressorMode
                let sendingSustain = files.loadedSong.presetList[preset].instrumentsMidiSets[i].usedCC[0].isOn
                let usingSustain =   files.loadedSong.presetList[preset].instrumentsMidiSets[i].usedCC[1].isOn
              
                let noteRange =      files.loadedSong.presetList[preset].instrumentsMidiSets[i].noteRange
                let useAutoInvesion = files.loadedSong.presetList[preset].instrumentsMidiSets[i].useAutoInversion

                let style =          files.loadedSong.presetList[preset].instrumentsMidiSets[i].pads[soundSet].style
                let relativeOctave = files.loadedSong.presetList[preset].instrumentsMidiSets[i].pads[soundSet].relativeOctave
                let inversion =      files.loadedSong.presetList[preset].instrumentsMidiSets[i].pads[soundSet].inversion
                let volume =         files.loadedSong.presetList[preset].instrumentsMidiSets[i].pads[soundSet].relativeVelocity
                
                let drum1 =          UInt8(files.loadedSong.presetList[preset].instrumentsMidiSets[i].pads[soundSet].drumm1Note)
                let drum2 =          UInt8(files.loadedSong.presetList[preset].instrumentsMidiSets[i].pads[soundSet].drumm2Note)
                let drum3 =          UInt8(files.loadedSong.presetList[preset].instrumentsMidiSets[i].pads[soundSet].drumm3Note)
                let drum4 =          UInt8(files.loadedSong.presetList[preset].instrumentsMidiSets[i].pads[soundSet].drumm4Note)
                let drummNote = [drum1, drum2, drum3, drum4]
                
                let drum1On =          files.loadedSong.presetList[preset].instrumentsMidiSets[i].pads[soundSet].drumm1IsOn
                let drum2On =          files.loadedSong.presetList[preset].instrumentsMidiSets[i].pads[soundSet].drumm2IsOn
                let drum3On =          files.loadedSong.presetList[preset].instrumentsMidiSets[i].pads[soundSet].drumm3IsOn
                let drum4On =          files.loadedSong.presetList[preset].instrumentsMidiSets[i].pads[soundSet].drumm4IsOn
                let drummsOn =         [drum1On, drum2On, drum3On, drum4On]
                
                var chord =            [Int]()
                var octaveUP =         0
                
                
                
                
                // ==============================
                // Обычный режим - не барабанный
                // ==============================
                
                
                if type == 0 {
                    
                   
                    var newInversion = inversion
                    
                    // Для аккордов из 3 и более нот рассчитать смену обращения.
                    if useAutoInvesion  &&  style > 4 {
                        var inversionData = autoInversion(inversion: inversion, tonality: tonality, noteRange: noteRange, chordType: style)
                        newInversion = inversionData[0]
                        octaveUP = inversionData[1]
                    }
                    
                    // сгенерировать ноты аккорда для активного инструмента
                    chord = chords.chordMatrix[style][chordType][newInversion]
                    
                    
                    // Для всех входящих нот
                    if soundSet < 10  {
                        
                        
                        // В режиме без смены обращений перенести ноты
                        // выходящие за границу диапозона на октаву вверх
                        if style <= 4   &&   tonality < noteRange {
                            octaveUP += 12
                        }
                    
                        
                        // рассчитать смещение нот относительно этого аккорда
                        offset = (rootOctave * 12) + (relativeOctave * 12) + tonality + octaveUP + liveTranspose
                        
                        
                        // Для нот из мелодической октавы
                    } else {
                        offset = Int(notenumber!) - 36 + (rootOctave * 12) + (relativeOctave * 12)  + (keysOctave * 12) + liveTranspose
                    }
                    
                    
                    
                    // уменьшить громкость инструмента, в зависимости от настроек
                    // (будем считать, что громкость у всех нот одинаковая)
                    let velocity = midiCompressor(mode: compressorMode,
                                                  noteNelocity: playVelocity,
                                                  instrumentMaxVelocity: maxVelocity,
                                                  padRelativeVolume: volume)
                    
                    
                    
                    
                    // После этого пройтись по всем нотам в получившемся аккорде
                    for i in chord {
                        
                        // При нажатии комбинации аккордов включаем сустейн
                        // (если это разрешено в настройках)
                        if soundSet == 8  &&  sendingSustain && usingSustain {
                            sendToMidiOut(command: .controllChange, channel: channel - 1, note: 64, value: 127)
                        }
                        
                        // дополнительно отфильтровываем сустейн для этого канала
                        // приходящий от подключенной к синтезатору педали
                        if !usingSustain {
                            sendToMidiOut(command: .controllChange, channel: channel - 1, note: 64, value: 0)
                        }
                        
                        
                        //  и сохраняем включенные ноты
                        //  в формате: [ нота - громкость - канал ]
                        let note = [UInt8(i + offset), velocity, channel - 1]
                        generatedNotes.append(note)
                        
                    }
                    
                    
                    
                    
                    
                    
                    
                    // ==============================
                    // Барабанный режим
                    // ==============================
                } else {
                    
                    // Обработка сустейна
                    if soundSet == 8  &&  usingSustain {
                        sendToMidiOut(command: .controllChange, channel: channel - 1, note: 64, value: 127)
                    }
                    
                    if !usingSustain {
                        sendToMidiOut(command: .controllChange, channel: channel - 1, note: 64, value: 0)
                    }
                    
                    // Настроить громкость
                    let velocity = midiCompressor(mode: compressorMode,
                                                  noteNelocity: playVelocity,
                                                  instrumentMaxVelocity: maxVelocity,
                                                  padRelativeVolume: volume)
                    
                    // сохранить ноты для последующего воспроизведения
                    for i in 0 ... 3 {
                        if drummsOn[i] {
                            generatedNotes.append([drummNote[i], velocity, channel - 1])
                        }
                    }
                }
            }
        }
        
        return generatedNotes
    }

}
