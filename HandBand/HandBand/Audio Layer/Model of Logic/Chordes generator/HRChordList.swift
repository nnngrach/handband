// База данных для генерации аккордовых звуков


import Foundation

final class HRChordList {
    static let shared = HRChordList()
   
    var chordMatrix: [[[[Int]]]]
    
    // Ноты из которых будут складываться аккорды
    
    private let A: [Int]      = [0, 4, 7, 12]
    private let Am: [Int]     = [0, 3, 7, 12]
    private let A7: [Int]     = [0, 4, 7, 10]
    private let Am7: [Int]    = [0, 3, 7, 10]
    private let Asus2: [Int]  = [0, 2, 7, 12]
    private let Asus4: [Int]  = [0, 5, 7, 12]
    private let A6: [Int]     = [0, 4, 9, 12]
    private let Am6: [Int]    = [0, 3, 9, 12]
    private let Aaug7: [Int]  = [0, 4, 8, 10]
    private let Adim7: [Int]  = [0, 3, 6, 9]
    private let AM7: [Int]    = [0, 4, 7, 11]
    
    private var notes = [[Int]]()
    
    
    
    
    
    
    private init () {
        
        // Чтобы сделать итерацию заносим ноты в массив
        notes = [A, Am, A7, Am7, Asus2, Asus4, A6, Am6, Aaug7, Adim7, AM7]
        
        
        
    // Создаем многомерный массив наполненный нулями
    // Чтобы можно было организовать цикл For и заполнить
        
        // типовая ячейка чтобы задать тип массиву
        let cell: [Int] = [0]
        
        // 4 варианта обращения каждого аккорда
        let inversion = Array.init(repeating: cell, count: 4)
        
        // 11 типов аккордов
        let chord = Array.init(repeating: inversion, count: 11)
        
        // и 9 вариантов взятия нот из этих аккордов
        chordMatrix = Array.init(repeating: chord, count: 10) // добавил один
        
        
        
//===========================================================
//===========================================================
        // Матрица аккордов
        // стиль - тип аккорда - обращение - список нот
        
        // Унисоны
        for i in 0 ... 10 {
            chordMatrix[0][i][0] = [notes[i][0]]
            chordMatrix[0][i][1] = [notes[i][1]]
            chordMatrix[0][i][2] = [notes[i][2]]
            chordMatrix[0][i][3] = [notes[i][3]]
        }
        
        // Терции
        for i in 0 ... 10 {
            chordMatrix[1][i][0] = [notes[i][0], notes[i][1]]
            chordMatrix[1][i][1] = [notes[i][1], notes[i][2]]
            chordMatrix[1][i][2] = [notes[i][2], notes[i][3]]
            chordMatrix[1][i][3] = [notes[i][3], (notes[i][0] + 12)]
        }
        
        // Квинты
        for i in 0 ... 10 {
            chordMatrix[2][i][0] = [notes[i][0], notes[i][2]]
            chordMatrix[2][i][1] = [notes[i][1], notes[i][3]]
            chordMatrix[2][i][2] = [notes[i][2], (notes[i][0] + 12)]
            chordMatrix[2][i][3] = [notes[i][3], (notes[i][1] + 12)]
        }
        
        // Октавы
        for i in 0 ... 10 {
            chordMatrix[3][i][0] = [notes[i][0], (notes[i][0] + 12)]
            chordMatrix[3][i][1] = [notes[i][1], (notes[i][1] + 12)]
            chordMatrix[3][i][2] = [notes[i][2], (notes[i][2] + 12)]
            chordMatrix[3][i][3] = [notes[i][3], (notes[i][3] + 12)]
        }
        
        // Power Chords
        for i in 0 ... 10 {
            chordMatrix[4][i][0] = [notes[i][0], notes[i][2],        (notes[i][0] + 12)]
            chordMatrix[4][i][1] = [notes[i][1], notes[i][3],        (notes[i][1] + 12)]
            chordMatrix[4][i][2] = [notes[i][2], (notes[i][0] + 12), (notes[i][2] + 12)]
            chordMatrix[4][i][3] = [notes[i][3], (notes[i][1] + 12), (notes[i][3] + 12)]
        }
        
        

        
        // Обычные аккорды 3 ноты
        for i in 0 ... 10 {
            if notes [i][3] == 12 {
                chordMatrix[5][i][0] = [notes[i][0], notes[i][1],           notes[i][2]]
                chordMatrix[5][i][1] = [notes[i][1], notes[i][2],           (notes[i][0] + 12)]
                chordMatrix[5][i][2] = [notes[i][2], (notes[i][0] + 12),    (notes[i][1] + 12)]
                chordMatrix[5][i][3] = [(notes[i][0] + 12),   (notes[i][1] + 12),   (notes[i][2] + 12)]
            } else {
                chordMatrix[5][i][0] = [(notes[i][3] - 12),  notes[i][1],           notes[i][2]]
                chordMatrix[5][i][1] = [notes[i][1],         notes[i][2],           notes[i][3]]
                chordMatrix[5][i][2] = [notes[i][2],         notes[i][3],          (notes[i][1] + 12)]
                chordMatrix[5][i][3] = [notes[i][3],        (notes[i][1] + 12),    (notes[i][2] + 12)]
            }
        }
        
        
        
        // Обычные аккорды 4 ноты
        for i in 0 ... 10 {
            if notes [i][3] == 12 {
                chordMatrix[6][i][0] = [notes[i][0],          notes[i][1],          notes[i][2],         (notes[i][0] + 12)]
                chordMatrix[6][i][1] = [notes[i][1],          notes[i][2],         (notes[i][0] + 12),   (notes[i][1] + 12)]
                chordMatrix[6][i][2] = [notes[i][2],         (notes[i][0] + 12),   (notes[i][1] + 12),   (notes[i][2] + 12)]
                chordMatrix[6][i][3] = [(notes[i][0] + 12),  (notes[i][1] + 12),   (notes[i][2] + 12),   (notes[i][0] + 24)]
            } else {
                chordMatrix[6][i][0] = [notes[i][0],          notes[i][1],           notes[i][2],        (notes[i][3])]
                chordMatrix[6][i][1] = [notes[i][1],          notes[i][2],           notes[i][3],        (notes[i][1] + 12)]
                chordMatrix[6][i][2] = [notes[i][2],          notes[i][3],          (notes[i][1] + 12),  (notes[i][2] + 12)]
                chordMatrix[6][i][3] = [notes[i][3],         (notes[i][1] + 12),    (notes[i][2] + 12),  (notes[i][0] + 24)]
            }
        }
        
        

        
        // Расширенные аккорды
        for i in 0 ... 10 {
            if notes [i][3] == 12 {
                chordMatrix[7][i][0] = [ notes[i][0],          notes[i][2],           (notes[i][0] + 12),       (notes[i][1] + 12)]
                chordMatrix[7][i][1] = [ notes[i][1],         (notes[i][0] + 12),     (notes[i][1] + 12),       (notes[i][2] + 12)]
                chordMatrix[7][i][2] = [ notes[i][2],         (notes[i][1] + 12),     (notes[i][2] + 12),       (notes[i][0] + 24)]
                chordMatrix[7][i][3] = [(notes[i][0] + 12),   (notes[i][2] + 12),     (notes[i][0] + 24),       (notes[i][1] + 24)]
            } else {
                chordMatrix[7][i][0] = [notes[i][0],           notes[i][2],            notes[i][3],             (notes[i][1] + 12)]
                chordMatrix[7][i][1] = [notes[i][1],           notes[i][3],           (notes[i][1] + 12),       (notes[i][2] + 12)]
                chordMatrix[7][i][2] = [notes[i][2],          (notes[i][1] + 12),     (notes[i][2] + 12),       (notes[i][3] + 12)]
                chordMatrix[7][i][3] = [notes[i][3],          (notes[i][2] + 12),     (notes[i][0] + 24),       (notes[i][1] + 24)]
            }
        }
        
        
        
        // Широкие аккорды
        for i in 0 ... 10 {
            if notes [i][3] == 12 {
                chordMatrix[8][i][0] = [ notes[i][0],           notes[i][2],           (notes[i][1] + 12),       (notes[i][2] + 12)]
                chordMatrix[8][i][1] = [ notes[i][1],          (notes[i][0] + 12),     (notes[i][2] + 12),       (notes[i][0] + 24)]
                chordMatrix[8][i][2] = [ notes[i][2],          (notes[i][1] + 12),     (notes[i][0] + 24),       (notes[i][1] + 24)]
                chordMatrix[8][i][3] = [(notes[i][0] + 12),    (notes[i][2] + 12),     (notes[i][1] + 24),       (notes[i][2] + 24)]
            } else {
                chordMatrix[8][i][0] = [(notes[i][3] - 12),     notes[i][2],           (notes[i][1] + 12),       (notes[i][2] + 12)]
                chordMatrix[8][i][1] = [ notes[i][1],           notes[i][3],           (notes[i][2] + 12),       (notes[i][3] + 12)]
                chordMatrix[8][i][2] = [ notes[i][2],          (notes[i][1] + 12),     (notes[i][3] + 12),       (notes[i][1] + 24)]
                chordMatrix[8][i][3] = [ notes[i][3],          (notes[i][2] + 12),     (notes[i][1] + 24),       (notes[i][2] + 24)]
            }
        }
        
        
        // Гитароподобные аккорды E - A - G - C
        
        for i in 0 ... 10 {
            if notes [i][3] == 12 {
                chordMatrix[9][i][0] = [notes[i][0],    notes[i][2],   (notes[i][0] + 12),  (notes[i][1] + 12),  (notes[i][2] + 12),  (notes[i][0] + 24) ]
                chordMatrix[9][i][1] = [notes[i][0],    notes[i][2],   (notes[i][0] + 12),  (notes[i][1] + 12),  (notes[i][2] + 12) ]
                chordMatrix[9][i][2] = [notes[i][0],    notes[i][1],    notes[i][2],        (notes[i][0] + 12),  (notes[i][1] + 12),  (notes[i][0] + 24) ]
                chordMatrix[9][i][3] = [notes[i][0],   (notes[i][1]),  (notes[i][2]),       (notes[i][0] + 12),  (notes[i][1] + 12) ]
            } else {
                chordMatrix[9][i][0] = [notes[i][0],    notes[i][2],   (notes[i][3]),       (notes[i][1] + 12),  (notes[i][3] + 12),  (notes[i][0] + 24) ]
                chordMatrix[9][i][1] = [notes[i][0],    notes[i][2],   (notes[i][3]),       (notes[i][1] + 12),  (notes[i][2] + 12) ]
                chordMatrix[9][i][2] = [notes[i][0],    notes[i][1],    notes[i][2],        (notes[i][0] + 12),  (notes[i][1] + 12),  (notes[i][3] + 12) ]
                chordMatrix[9][i][3] = [notes[i][0],   (notes[i][1]),  (notes[i][3]),       (notes[i][0] + 12),  (notes[i][1] + 12) ]
            }
        }
    }
}
