// Измененяет высоту входящих нот согласно предустановленой тональности

import Foundation
extension NoteGenerator {
    
    
    // MARK: Midi Compressor
    // Компрессор - Изменяет кривую громкости в зависимости от выбранного режима
    func midiCompressor(mode: Int, noteNelocity: UInt8, instrumentMaxVelocity: Float, padRelativeVolume: Float) -> UInt8 {
        var result = Float(noteNelocity)
        
        switch mode {
        case 1:
            // логарифмическая (мягкая)
            result = log(result) * 26.2
        case 2:
            // параболическая (жесткая)
            result = sqrtf(result * 127) / 2 + 63
        case 3:
            // фиксированная громкость (максимальное значение)
            result = 127
        default:
            // линейная громкость
            break
        }
        
        
        // При необходимости убавляет громкость в зависимости от установленных настроек
        result = result * (instrumentMaxVelocity / 100) * (padRelativeVolume / 100)
        
        // Перестраховка от выхода за предел допустимых значений
        if result < 1 {result = 0}
        if result > 126 {result = 127}
        
        return UInt8(result)
    }
    
    
    
    
    
    
    // MARK: Sustain for all channels
    // Включение и выключение сустейна для всех каналов
    
    func sustain(on: Bool) {
        if on {
            for i in 0 ... 15 {
                sendToMidiOut(command: .controllChange, channel: UInt8(i), note: 64, value: 127)
            }
            
        } else {
            for i in 0 ... 15 {
                sendToMidiOut(command: .controllChange, channel: UInt8(i), note: 64, value: 0)
            }
        }
    }
    
    
    
    
    
    
    // MARK: Pitchband processing
    
    func pitchOn(value: UInt16) {
        let preset = files.preset
        for i in 0 ..< files.loadedSong.presetList[preset].instrumentsMidiSets.count {
            let channel = UInt8(files.loadedSong.presetList[preset].instrumentsMidiSets[i].midiChannel)
            let isOn = files.loadedSong.presetList[preset].instrumentsMidiSets[i].usedCC[2].isOn
            
            if isOn {
                sendPitchToMidiOut(channel: channel, value: value)
            }
        }
    }
    
    
    
    
//    enum midiCommandNames {
//        case noteOn
//        case noteOff
//        case polyPressute
//        case controllChange
//        case programmChange
//        case channelPressure
//    }
    
//    func getNumberFor(commandName: midiCommandNames) -> Int{
//        switch commandName {
//        case .noteOff:
//            return 0
//        case .noteOn:
//            return 1
//        case .polyPressute:
//            return 2
//        case .controllChange:
//            return 3
//        case .channelPressure:
//            return 5
//        case .programmChange:
//            return 4
//        }
//    }
    
    
    
    // MARK: All other midi messages processing
    // Обработка всех Midi событий, кроме нажатия нот и питчбэнда
    
    func makeCustomMidiMessage(command: MusicDataBase.midiCommandNames, value1: UInt8, value2: UInt8) {
        let preset = files.preset
        
        var number = 0
        //number = getNumberFor(commandName: command)
        
        // получает номер переменной в массиве разрешенных midi событий
        switch command {
        case .polyPressute:
            number = 4

        case .channelPressure:
            number = 4

        case .programmChange:
            number = 5

        case .controllChange:
            if value1 != 64 {
                number = 3
            } else {
                // sustain
                number = 1
            }

        default:
            break
        }
        
        
        // пройтись по списку со всеми дорожками.
        // если на дорожке разрешено использовать этот тип midi событий
        // то послать его на воспроизведение с канала этой дорожки
        
        for i in 0 ..< files.loadedSong.presetList[preset].instrumentsMidiSets.count {
            let channel = UInt8(files.loadedSong.presetList[preset].instrumentsMidiSets[i].midiChannel)
            let isOn = files.loadedSong.presetList[preset].instrumentsMidiSets[i].usedCC[number].isOn

            if isOn {
                sendToMidiOut(command: command, channel: channel, note: value1, value: value2)
            }
        }
    }
    
   
    
    

    
    
    
    

    
    
    
    // MARK: Midi panic
    // Принудительное отключение всех звучащих нот
    
    func midiPanic() {
        for channel in 0 ... 15 {
            for noteNumber in 0 ... 127 {
                sendToMidiOut(command: .noteOff, channel: UInt8(channel), note: UInt8(noteNumber), value: 0)
            }
            sendToMidiOut(command: .controllChange, channel: UInt8(channel), note: 0, value: 123)
        }
        
        for padNumber in 0 ... 15 {
            HRKeyEquality.singleton.pressedComboKeys[padNumber] = 0
        }
        
        for keyNumber in 0 ... 12 {
            HRKeyEquality.singleton.keysON[keyNumber] = 0
        }
        
        
        
        DispatchQueue.main.async(execute: {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "sendToDisplay"),
                    object: nil,
                    userInfo: [
                        "text": "MIDI Panic!"
                ])
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "screenPadsupdate"),
                                            object: nil,
                                            userInfo: [
                                                "text": ""
                ])
        })
    }
    
    
    
    
    
    
    
    // MARK: Send midi out
    // Отправка сгенерированных нот обратно на Аудиуконтроллер
    // с помощью делегирования
    
    func sendToMidiOut(command:MusicDataBase.midiCommandNames, channel:UInt8, note:UInt8, value:UInt8) {
        delegate?.receiveGeneratedMidiMessage(command, channel, note, value)
    }
    
    func sendPitchToMidiOut(channel:UInt8, value:UInt16) {
        delegate?.receiveGeneratedPitch(channel, value)
    }
    
    
 
    
}
