// Контроллер для преобразования всех собранных настроек
// в комбинацию нот и для отправки этих нот на MIDI-выход

import AudioKit
import UIKit

final class NoteGenerator {
    static let singleton = NoteGenerator()
    private init() {
        sustain(on: false)
    }
    
    let chords = HRChordList.shared
    let sets = GlobalSettings.singleton
    let files = HRFileOperations.shared
    var delegate: NoteGeneratorListenerDelegate?
    let musicData = MusicDataBase()
    

    // В этом массиве будут сохраняться включенные ноты
    // Чтобы их всех потом можно было выключть
    // Даже если настройки успеют измениться
    var toPlay = Array.init(repeating: [[UInt8]](), count: 12)
    var toPlaySolo = Array.init(repeating: [[UInt8]](), count: 128)
    var toPlaySoloBlack = Array.init(repeating: [[UInt8]](), count: 128)

    
    // Рассчитываемое смещение нот относительно аккорда Am
    var offset: Int = 0                        //   -7 ... + 7
    var chordType = 0                          //   maj, min, sus ...
    var tonality = 0                           //   0 - A, 1 - A# ...
    var currentChordName = "Am"
    var canPlayThis = true
    


    
    func changeChord(tonality: Int, chordType: Int, currentChordName: String) {
        self.tonality = tonality
        self.chordType = chordType
        self.currentChordName = currentChordName
    }
    
    
    // MARK : Chord ON
    // Играть все звуки для выбранного пэда (а аккордовом регистре)
    
    func makeNotesOnForChord(noteNumber: UInt8?, playVelocity: UInt8, soundSet: Int) {
        
        // проверяем, не является ли комбинация клавиш служебной (беззвучной)
        if canPlayThis {
            
            // генерирует аккордовые ноты, в зависимости от нажатой комбинации.
            // одновременно сохраняет ноты в массив, чтобы можно было потом их все выключить
            toPlay[soundSet] = chordProcessing(soundSet: soundSet, playVelocity: playVelocity, notenumber: noteNumber)
            
            // включает сгенерированные ноты
            for message in toPlay[soundSet] {
                let note = message[0]
                let velocity = message[1]
                let channel = message[2]
                
                sendToMidiOut(command: .noteOn, channel: channel, note: note, value: velocity)
            }
        }
    }
    
    
    
    
    
    // MARK : Chord OFF
    // Выключить все звучащие ноты выбранного пэда
    
    func makeChordNotesOff(padNum: Int) {
        if padNum >= 0 {
            // пройтись по сохраненному ранее массиву с перечнем запущенных нот
            for i in 0 ..< toPlay[padNum].count {
                
                // и отключить найденные ноты
                sendToMidiOut(command: .noteOff, channel: toPlay[padNum][i][2], note: toPlay[padNum][i][0], value: 0)
                
                // выключить сустейн
                if padNum == 8  {
                    sendToMidiOut(command: .controllChange, channel: toPlay[padNum][i][2], note: 64, value: 0)
                }
            }
        }
    }
    
    
    
    
    
    // MARK : Solo On
    // Обработать ноты мелодического регистра
    
    func makeNotesOnForMelody (scaledNote: UInt8, originalNote: UInt8, velocity: UInt8, soundSet: Int) {
        let notes = chordProcessing(soundSet: soundSet, playVelocity: velocity, notenumber: scaledNote)
        
        for message in notes {
            let note = message[0]
            let velocity = message[1]
            let channel = message[2]
            sendToMidiOut(command: .noteOn, channel: channel, note: note, value: velocity)
        }
        
        if soundSet == 10 {
            toPlaySolo[Int(originalNote)] = notes
        } else {
            toPlaySoloBlack[Int(originalNote)] = notes
        }
        
    }
    
    
    
    
    // MARK : Solo Off
    func makeMelodicNotesOff (origNote: UInt8, pad: Int) {
        var notes = [[UInt8]]()
        
        if pad == 10 {
            notes = toPlaySolo[Int(origNote)]
        } else {
            notes = toPlaySoloBlack[Int(origNote)]
        }
        
        for message in notes {
            let note = message[0]
            let channel = message[2]
            sendToMidiOut(command: .noteOff, channel: channel, note: note, value: 0)
        }
        
        toPlaySolo[Int(origNote)] = []
    }
    
    
    
    
    
    
    
    // MARK: Screen buttons
    // Обработка экранных кнопок
    func playScreenNotesOn(row: Int, chordInRow: Int) {
        let chordData = files.loadedSong.songStrings[row].chords[chordInRow]
        print(chordData)
        
        
        switch chordData.type {
        case 1:
            // аккорд
            chordType = chordData.chordType
            tonality = chordData.chordTonality
            makeNotesOnForChord(noteNumber: 36, playVelocity: 110, soundSet: 8)
            ScaleTransformer.singleton.calculateCurrentScale()
            
        case 2:
            // пресет
            files.preset = chordData.preset
            makeNotesOnForChord(noteNumber: 1, playVelocity: 110, soundSet: 9)
            
        case 3:
            // транспонирование
            sets.transpose = chordData.transpose
        default:
            // пустой
            break
        }
    }
    
    
    
    
    func playScreenNotesOff(row: Int, chordInRow: Int) {
        let chordData = files.loadedSong.songStrings[row].chords[chordInRow]
        if chordData.type == 1 {
            makeChordNotesOff(padNum: 8)
        }
    }
    
}
