// Обработчик способов взятия аккорда

import Foundation
extension NoteGenerator {

    // Автосмена обращения аккордов
    func autoInversion(inversion: Int, tonality: Int, noteRange: Int, chordType: Int) -> [Int] {
        var newInversion = inversion
        var octaveChange = 0
        var inversionData = [0, 0]
        
        if chordType != 9 {
            let firstSplit = noteRange + 5
            let secondSplit = noteRange + 8
            
            if tonality >= firstSplit {
                inversionData = changeInversion(currenInversion: newInversion)
                newInversion = inversionData[0]
                octaveChange += inversionData[1]
            }
            
            if tonality >= secondSplit {
                inversionData = changeInversion(currenInversion: newInversion)
                newInversion = inversionData[0]
                octaveChange += inversionData[1]
            }
            
            
        } else {
            inversionData = guitarChordsInversion(tonality: tonality)
            newInversion = inversionData[0]
            octaveChange = inversionData[1]
        }
        
        
        return [newInversion, octaveChange]
    }
    
    
    
    // Сменить обращение аккорда в обычном режиме
    func changeInversion(currenInversion: Int) -> [Int] {
        var newInversion = currenInversion
        var ocatveCorrecting = 0
        
        switch currenInversion {
        case 0:
            newInversion = 2
            ocatveCorrecting = -12
        case 1:
            newInversion = 0
        case 2:
            newInversion = 1
        default:
            newInversion = 1
        }
        
        return [newInversion, ocatveCorrecting]
    }
    
    
    
    // Настройка звука для гитароподобных аккордов
    func guitarChordsInversion(tonality: Int) -> [Int] {
        var newInversion = 0
        var ocatveCorrecting = 0
        
        switch tonality {
        case 4, 5, 6, 8:
            //E F F# G#
            newInversion = 0
            ocatveCorrecting = -12
        // -1
        case 9, 10, 11, 1:
            //A A# H C#
            newInversion = 1
            ocatveCorrecting = -12
        case 2, 3:
            //D D#
            newInversion = 1
        case 7:
            //G
            newInversion = 2
            ocatveCorrecting = -12
        case 0:
            //C
            newInversion = 3
        default:
            newInversion = 1
            ocatveCorrecting = -12
        }
        
        return [newInversion, ocatveCorrecting]
    }
}
