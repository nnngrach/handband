// Контроллер для распознавания сочетаний клавиш

import UIKit

final class HotKeyController {
    static let singleton = HotKeyController()
    private  init() {}
    
    let chorder = NoteGenerator.singleton
    let k = HRKeyEquality.singleton
    let sets = GlobalSettings.singleton
    let files = HRFileOperations.shared
    var delegate: DetecedComboListenerDelegate?
   
    var padsOn = Array.init(repeating: false, count: 16)
    var messsage = ""
    var chorderReadedKeys = true
    var chorderReadedStopKeys = true
    var canChangeChord = true

    // Задержка для распознавания комбинаций клавиш
    // usleep(1000000)  // will sleep for 1 second
    let delayTime: UInt32 = 20000
    //let delayTime: UInt32 = 10000
    

    
    // =========================================================================
    // Обработка аккордовых пэдов (посылать на эту функцию только их!)
    
    // проблема: пока пользователь нажимает две-три кнопки для комбинации аккорда
    // успевает прозвучать по мажорному аккорду для каждой клавиши
    
    // просто отфильтруем этот период времени! будем играть аккорд не сразу, а с небольшой задержкой:
    // дождемся пока пользователь нажмем все кнопки и только потом будем воспроизводить звук
    
    
    
    func playChord (velocity: UInt8) {
        if chorderReadedKeys {
            
            // первая нота аккорда принята. теперь
            // пока пользователь не отпустит аккорд
            // не принимаем на обработку новые аккорды,
            // чтобы не прозвучало сразу несколько аккордов
            
            chorderReadedKeys = false
            chorderReadedStopKeys = true
            
            // Выжидаем, пока все пэды будут нажаты
            // и отправляем команду распознать аккорд
            
            usleep(delayTime)
            
            if let detectingResult = chordAnalysis() {
                if detectingResult.isSounding {
    
                    delegate?.receiveDetectedChordOn(result: detectingResult, velocity: velocity)
                    
                    // теперь пока пользователь на отпустит все кнопки
                    // не будем давать давать аккорду меняться
                    
                    canChangeChord = false
                }
            }
        }
    }
    

    

    
    // Снять аккорд - Включается каждый раз, когда
    // пользователь отпускает аккордовую кнопку
    
    func stopChord () {
        // не будем выключать аккорд
        // пока нажата хотябы одна из аккордовых кнопок
        
        if k.check(combo: k.comboChords["Chord OFF"]![0] as! [Int])  {
            
            delegate?.receiveDetectedChordOff()
            
            chorderReadedStopKeys = false

            //включаем прием новых аккордов
    
            chorderReadedKeys = true
            canChangeChord = true
        }
    }
    
    
    
    
    
    // Для определения служебных комбинаций клавиш
     
    func hotkeysAnalisys() {
        
        for i in k.comboSystem {
            if k.check(combo: k.comboSystem[i.key]![0] as! [Int]) {
                let label = i.key
                let value = i.value[2] as! Int
                delegate?.receiveDetectedSystemCombos(label: label, value: value)
                break
            }
        }
    }
    
    
    //    func waitToCombo () {
    //        usleep(delayTime)
    //        hotkeysAnalisys()
    //    }

}
