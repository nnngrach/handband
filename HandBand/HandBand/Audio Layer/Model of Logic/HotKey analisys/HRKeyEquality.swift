// Сравнивает нажатые и отжатые кнопки
// в текущий момент времени
// с хранящимися в базе комбинациями


import UIKit

final class HRKeyEquality {
    static let singleton = HRKeyEquality()
    private init() {}
    
    
    // массив с текущим состояним кнопок:
    // 0 - не нажата
    // 1 - нажата
    var pressedComboKeys = Array.init(repeating: 0, count: 16)
    var keysON = Array.init(repeating: 0, count: 13)
    
    
    // массив с условиями срабатывания комбинации:
    // 0 - должна быть не нажата
    // 1 - должна быть нажата
    // 2 - не важно
    // TODO: Заменить на enum (on, off, any)
    let comboChords = KeyCombinationsDatabase.comboChords
    let comboSystem = KeyCombinationsDatabase.comboSystem
    let chordTypes = KeyCombinationsDatabase.chordTypes
    
    
    
    func check(combo: [Int]) -> Bool {
        
        for i in 0 ..< pressedComboKeys.count {
            if combo[i] != 2 {
                if combo[i] != pressedComboKeys[i] {
                    return false
                }
            }
        }
        return true
    }
    
    
    
}
