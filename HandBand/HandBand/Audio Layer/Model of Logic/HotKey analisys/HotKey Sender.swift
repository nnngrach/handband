// Генерирует и отправляет на дисплей названия аккордов

import Foundation
extension HotKeyController {
    
    // Рассчитать название звучащей ноты в зависимости от текущего транспонирования
    
    func keyName(num: Int) -> String  {
        let sets = GlobalSettings.singleton
        let noteNames = KeyCombinationsDatabase.noteNamesFromA
       
        var number = num
        number = (number + sets.transpose) % 12
        
        if number >= 12 {
            number -= 12
        } else if number < 0 {
            number += 12
        }
        
        return noteNames[number]
    }
    
    
    
    
    
    
    // Отправить уведомления с техтом аккорда на дисплейчик
    
    func sendToDisplay(text: String) {
        let nc = NotificationCenter.default
        DispatchQueue.main.async(execute: {
            nc.post(name: NSNotification.Name(rawValue: "sendToDisplay"),
                    object: nil,
                    userInfo: [
                        "text": text
                ])
        })
    }
    
}
