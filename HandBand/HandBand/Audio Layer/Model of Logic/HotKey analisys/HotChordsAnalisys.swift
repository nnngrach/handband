// Назначает действия распознанным аккордам

import Foundation
extension HotKeyController {
    
    func transposedTonality() -> Int {
        let transpose = UserDefaults.standard.integer(forKey: "Transpose")
        let tonality = Int(chorder.tonality)
        return tonality + transpose + 3
    }
    
    
    func isSoundingChord(_ combo: String) -> Bool {
        return (k.comboChords[combo]![1] as! Int) < 12
    }
    
    
    func chordAnalysis() -> ComboDetectorResult? {
        messsage = "Empty"

        // пока удерживается аккорд на сустейне
        // новый аккорд играть не нужно
        
        if canChangeChord {
            
            for i in k.comboChords {
                
                if k.check(combo: i.value[0] as! [Int]) {
                    
                    if isSoundingChord(i.key) {
                        // TODO: перенести в аудиоконтроллер
                        chorder.tonality = k.comboChords[i.key]![2] as! Int
                        chorder.chordType = k.comboChords[i.key]![1] as! Int
                        messsage = keyName(num: transposedTonality()) + k.chordTypes[chorder.chordType]
                        
                        
                        let tonality = k.comboChords[i.key]![2] as! Int
                        let chordType = k.comboChords[i.key]![1] as! Int
                        let message = keyName(num: transposedTonality()) + k.chordTypes[chordType]
                        print (message)
                        return ComboDetectorResult(message: message,
                                                   isSounding: true,
                                                   tonality: tonality,
                                                   chordType: chordType)
                        
                    } else {
                        // TODO: перенести в аудиоконтроллер
                        chorder.canPlayThis = false
                        
                        return ComboDetectorResult(message: nil,
                                                   isSounding: false,
                                                   tonality: nil,
                                                   chordType: nil)
                    }
                }
            }
        }
        
        return nil
    }
    
}
