import Foundation
import AudioKit

extension AudioController: DetecedComboListenerDelegate {

    func receiveDetectedChordOn(result: ComboDetectorResult, velocity: UInt8) {
        noteGenerator.changeChord(tonality: result.tonality!,
                                  chordType: result.chordType!,
                                  currentChordName: result.message!)
        
        scaleTransformer.calculateCurrentScale()
        sender.sendToDisplay(text: result.message!)
        
        noteGenerator.makeNotesOnForChord(noteNumber: musicBase.mustBeZero,
                                          playVelocity: velocity,
                                          soundSet: musicBase.chordButtonsSoundset)
    }
    
    
    
    func receiveDetectedChordOff() {
        noteGenerator.makeChordNotesOff(padNum: musicBase.chordButtonsSoundset)
        noteGenerator.canPlayThis = true
    }
    
    
    
    func receiveDetectedSystemCombos(label: String, value: Int) {
        var messsage = ""
        
        switch label {
        case "Page +", "Page -":
            let countOfScrollingStrings = 4
            messsage = label
            sets.songString += countOfScrollingStrings * value
            noteGenerator.canPlayThis = false
            
        case "Transp 0":
            messsage = "Transp OFF"
            sets.transpose = 0
            //sets.keysOctave = 0
            noteGenerator.canPlayThis = false
            
        case "Transp +", "Transp -":
            var t = UserDefaults.standard.integer(forKey: "Transpose")
            t += value
            sets.transpose = t
            messsage = "Transpose \(t)"
            noteGenerator.canPlayThis = false
            
        case "Octave +", "Octave -":
            sets.keysOctave += value
            messsage = "Octave \(sets.keysOctave)"
            noteGenerator.canPlayThis = false
            
        case "Scale ON", "Scale OFF":
            messsage = label
            let state = value == 1 ? true : false
            files.loadedSong.presetList[files.preset].useSmartScales = state
            files.resaveSong()
            scaleTransformer.calculateCurrentScale()
            
            // TODO: Заменить нотификацию?
            NotificationCenter.default.post(name: NSNotification.Name.init("smartScalesChanged"),  object: nil)
            
        case "Preset sound":
            noteGenerator.canPlayThis = false
            noteGenerator.canPlayThis = true
            noteGenerator.makeNotesOnForChord(noteNumber: nil,
                                              playVelocity: musicBase.velocityForScreenButtons,
                                              soundSet: musicBase.presetTriggeringSoundset)
            
        case "Preset 1", "Preset 2", "Preset 3", "Preset 4", "Preset 5", "Preset 6", "Preset 7", "Preset 8", "Preset 5 keys", "Preset 6 keys", "Preset 7 keys", "Preset 8 keys":
            
            let suffix = " keys"
            
            if label.hasSuffix(suffix) {
                messsage = label [0 ..< (label.count - suffix.count)]
            } else {
                messsage = label
            }
            
            files.preset = value
            noteGenerator.canPlayThis = false
            noteGenerator.canPlayThis = true
            noteGenerator.makeNotesOnForChord(noteNumber: nil,
                                              playVelocity: musicBase.velocityForScreenButtons,
                                              soundSet: musicBase.presetTriggeringSoundset)
            
        default:
            break
        }
        
        
        
        if messsage != "" {
            sender.sendToDisplay(text: messsage)
        }
    }
}
