// При получении Midi ноты от модуля AudioInOut
// нужно определить, находится ли она
// в секторе для мелодической игры
// или в секторе для распознавания горячих клавиш.
// После этого провести трансформацию нот по соответсвующему алгоритму
// и отправить их на воспроизведение.
// Или просто вызвать изменение настроек, если включен режим MIDI Learn.

// PS: Часть бизнес-логики не попала на эту страницу
// потому, что некоторые методы запускаются с помощью делегации.
// И чтобы было проще понимать логику работы программы,
// я оставил себе подсказки о том, какие методы будут запущены далее,
// используя для этого комментарии типа           // -> doSomething()


import Foundation
import AudioKit

extension AudioController: AudioInListenerDelegate {
    
    func receiveNoteOn(_ noteNumber: MIDINoteNumber, _ velocity: MIDIVelocity) {
        
        print("in ", noteNumber)
        
        if musicBase.diapasonOfHotkeyNotes.contains(noteNumber) {
            
            if isMidiLearnModeActive {
                
                midiLearner.assignTo(comboKeyNumber: comboKeyNumber, midiNoteNumber: noteNumber)
                isMidiLearnModeActive = false
                
            } else {
                
                let hotkeySortingResult = notePreProcessor.sortingHotkeyNoteOn(noteNumber: noteNumber, velocity: velocity)
                
                switch hotkeySortingResult.0 {
                    
                case .startSystemHotkeyDetecting:
                    detectingQueue.launchSystemHotKeysDetecter()
                    // -> receiveDetectedSystemCombos()
                    
                case .startTriggeredChordDetecting:
                    let chordInfo = hotkeySortingResult.1!
                    detectingQueue.launchChordDetecter(velocity: chordInfo.velocity)
                    // -> receiveDetectedChordOn()
                    // -> receiveGeneratedMidiMessage() -> AudioInOut
                    
                case .playSoundset:
                    let soundsetInfo = hotkeySortingResult.1!
                    noteGenerator.makeNotesOnForChord(noteNumber: soundsetInfo.noteNumber!,
                                                      playVelocity: soundsetInfo.velocity,
                                                      soundSet: soundsetInfo.comboKeyNumber!)
                    // -> receiveGeneratedMidiMessage() -> AudioInOut

                    
                default :
                    break
                }
            }
            
            
        } else {
            
            if let note = notePreProcessor.transformMelodicNoteOn(noteNumber: noteNumber, velocity: velocity) {
                noteGenerator.makeNotesOnForMelody(scaledNote: note.noteNumber!,
                                                   originalNote: note.noteNumberBeforeProcessing!,
                                                   velocity: note.velocity,
                                                   soundSet: note.comboKeyNumber!)
                // -> receiveGeneratedMidiMessage() -> AudioInOut
            }
        }
    }
    
   
    
    

    func receiveNoteOff(_ noteNumber: MIDINoteNumber) {

        if musicBase.diapasonOfHotkeyNotes.contains(noteNumber) {
            
            let hotkeySortingResult = notePreProcessor.sortingHotkeyNoteOff(noteNumber)
            
            switch hotkeySortingResult.0 {
                
            case .pauseChordDetecting:
                detectingQueue.resetChordDetecter()
                // -> receiveDetectedChordOff
                
            case .stopChord:
                detectingQueue.resetChordDetecter()
                // -> receiveDetectedChordOff
                
                let chordInfo = hotkeySortingResult.1!
                noteGenerator.makeChordNotesOff(padNum: chordInfo.comboKeyNumber!)
                // -> receiveGeneratedMidiMessage -> AudioInOut
    
            default:
                break
            }
            
        } else {
            
            if let note = notePreProcessor.transformMelodicNoteOff(noteNumber) {
                noteGenerator.makeMelodicNotesOff(origNote: note.noteNumber!,
                                                  pad: note.comboKeyNumber!)
                // -> receiveGeneratedMidiMessage() -> AudioInOut
            }
        }
    }
    
    
    
    
    func receiveCC(_ controllerNumber: MIDIByte, _ value: MIDIByte) {
        noteGenerator.makeCustomMidiMessage(command: .controllChange, value1: controllerNumber, value2: value)
        // -> receiveGeneratedMidiMessage -> AudioInOut
    }
    
    
    func receivePolypressure(_ noteNumber: MIDINoteNumber, _ pressure: MIDIByte) {
        noteGenerator.makeCustomMidiMessage(command: .polyPressute, value1: noteNumber, value2: pressure)
        // -> receiveGeneratedMidiMessage -> AudioInOut
    }
    
    
    func receiveChannelAftertouch(_ pressure: MIDIByte) {
        noteGenerator.makeCustomMidiMessage(command: .channelPressure, value1: musicBase.mustBeZero, value2: pressure)
        // -> receiveGeneratedMidiMessage -> AudioInOut
    }
    
    func receivePitchWheel(_ value: MIDIWord) {
        noteGenerator.pitchOn(value: value)
        // -> receiveGeneratedPitch -> AudioInOut
    }
    
}
