// При получении Midi-сообщениий от модуля NoteGenerator
// нужно вызвать соответствующую команду в модуле AudioInOut
// чтобы отправить сгенерированные ноты на выход аудио-движка.

import Foundation
import AudioKit

extension AudioController: NoteGeneratorListenerDelegate {
    
    func receiveGeneratedMidiMessage(_ command:MusicDataBase.midiCommandNames, _ channel:UInt8, _ note:UInt8, _ value:UInt8) {
        switch command {
        case .noteOff:
            audioInOut.noteOff(channel: channel, noteNumber: note, velocity: value)
        case .noteOn:
            print("out ", note)
            audioInOut.noteOn(channel: channel, noteNumber: note, velocity: value)
        case .polyPressute:
            audioInOut.sendPolyPressureData(channel: channel, noteNumber: note, value: value)
        case .controllChange:
            audioInOut.ccOut(channel: channel, cc: note, value: value)
        case .channelPressure:
            audioInOut.sendChannelPressureData(channel: channel, value: value)
        case .programmChange:
            audioInOut.programmChangeOut(channel: channel, number: value)
        }
    }
    
    
    func receiveGeneratedPitch(_ channel:UInt8,_ value:UInt16) {
        audioInOut.sendPitchBendData(channel: channel, value: value)
    }
    
}
