// Главный аудио-контроллер.
// Принимает Midi-ноты, сортирует, посылает их на на обработку,
// после чего отправляет обработанные ноты на Midi-выход.

import Foundation
import AudioKit

final class AudioController {
    static let singleton = AudioController()
    private init() {
        audioInOut.delegate = self
        detecterKeyCombos.delegate = self
        noteGenerator.delegate = self
        detecterComboKeyNumbers.loadNoteNumbersForComboKeys()
    }
    
    let audioInOut = AudioInOut.singleton
    
    let detecterComboKeyNumbers = DetecterComboKeyNumbers.singleton
    let detectingQueue = DetectingQueue.singleton
    let detecterKeyCombos = HotKeyController.singleton
    
    
    let notePreProcessor = NotePreProcessor()
    let scaleTransformer = ScaleTransformer.singleton
    
    let noteGenerator = NoteGenerator.singleton
    
    let midiLearner = MidiLearner()
    
    let sender = SenderToScreenButtonsUpdater()
    
    //TODO: А точно ли здесь нужны глобальные переменные
    
    let sets = GlobalSettings.singleton
    let files = HRFileOperations.shared
    let musicBase = MusicDataBase()
    
    //TODO: Не все эти классы обязаны быть синглтонами. Переделать.
    
    var isMidiLearnModeActive = false
    var comboKeyNumber = 0
}


