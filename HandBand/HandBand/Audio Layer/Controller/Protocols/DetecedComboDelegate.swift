
import Foundation

protocol DetecedComboListenerDelegate {
    
    func receiveDetectedChordOn(result: ComboDetectorResult, velocity: UInt8)
    
    func receiveDetectedChordOff()
    
    func receiveDetectedSystemCombos(label: String, value: Int)
}
