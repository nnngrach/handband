import Foundation
import AudioKit

protocol AudioInListenerDelegate {
    func receiveNoteOn(_ noteNumber: MIDINoteNumber, _ velocity: MIDIVelocity)
    func receiveNoteOff(_ noteNumber: MIDINoteNumber)
    func receiveCC(_ controller: MIDIByte, _ value: MIDIByte)
    func receivePolypressure(_ noteNumber: MIDINoteNumber, _ pressure: MIDIByte)
    func receiveChannelAftertouch(_ pressure: MIDIByte)
    func receivePitchWheel(_ pitchWheelValue: MIDIWord)
}
