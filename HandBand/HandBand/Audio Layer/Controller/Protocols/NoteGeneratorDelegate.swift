import Foundation
import AudioKit

protocol NoteGeneratorListenerDelegate {
    func receiveGeneratedMidiMessage(_ command:MusicDataBase.midiCommandNames, _ channel:UInt8, _ note:UInt8, _ value:UInt8)
    func receiveGeneratedPitch(_ channel:UInt8,_ value:UInt16)
}
