// Изменяет выстору входящих нот в соответствии с выбранным ладом

import Foundation

final class ScaleTransformer {
    static let singleton = ScaleTransformer()
    let files = HRFileOperations.shared
    let noteNames = NoteNames()
    var offsetArray = [String : [Int]] ()
    var regularSales = [[Int]] ()
    var regularSalesNames = [String] ()
    var currentScale = [String]()
    let musicData = MusicDataBase()
    
    private init() {
        fillOffsetArray()
        fillRegularScales()
        calculateCurrentScale()
        //generateArrayOfBlacks()
    }
    
    

    
    // MARK: - Все обработки высоты нот правой руки
    func process(note: UInt8, isWhiteKey: Bool) -> UInt8 {
        guard files.loadedSong.presetList[files.preset].useSmartScales else {return note}
        
        let playMode = UserDefaults.standard.integer(forKey: "Plaing mode")
        var newNote = note
        
        // режим для двухоктавных клавиатур
        if playMode == musicData.twoOctaveKeyboardsMode {
            newNote = handBandRightScale(note: newNote)
        }
        
        if isWhiteKey {
            newNote = whiteScaleSelecter(note: newNote)
            
        } else if !isWhiteKey && isBlackKeysOnSepareteChannel() {
            newNote = blackSmartScale(note: newNote)
            
        } else {
            // в двухоктавном режиме черные кнопки используются как белые
            newNote = whiteScaleSelecter(note: newNote)
        }
        
        return newNote
    }
    
    
    
    
    func isBlackKeysOnSepareteChannel() -> Bool {
        let preset = files.preset
        let smartBlack = files.loadedSong.presetList[preset].useSmartScaleForBlackKeys
        let playMode = UserDefaults.standard.integer(forKey: "Plaing mode")
        
        if playMode != musicData.twoOctaveKeyboardsMode && smartBlack {
            return true
        } else {
            return false
        }
    }
    
    
    
    // Включает ту или иную обработку белых клавиш
    // в зависимости от настроек
    
    func whiteScaleSelecter(note: UInt8) -> UInt8 {
        let preset = files.preset
        let smartWhite = files.loadedSong.presetList[preset].useSmartScaleForWhiteKeys
        let regularWhite = files.loadedSong.presetList[preset].useRegularScaleFowWhite
        var newNote = note
        
        if smartWhite {
            newNote = whiteSmartScale(note: newNote)
        } else if regularWhite {
            newNote = regularScale(note: newNote)
        }
        
        return newNote
    }
    
    
    
    
    // MARK: - Режим для двухоктавных клавиатур (Handband scale)
    // обработка аккордовой октавы
    func transformHotkeyNotesToHandbandScale(note: UInt8) -> UInt8 {
        let playMode = UserDefaults.standard.integer(forKey: "Plaing mode")
        if playMode == musicData.twoOctaveKeyboardsMode {
            switch note {
            case 48:
                return 52
            case 50:
                return 53
            case 52:
                return 55
            case 53:
                return 57
            case 55:
                return 59
            case 57:
                return 48
            case 59:
                return 50
            default:
                return note
            }
            
            
        } else {
            return note
        }
    }
    
    
    
    // обработка мелодической октавы
    func handBandRightScale(note: UInt8) -> UInt8 {
        switch note {
        case 60:
            return 64
        case 61:
            return 77
        case 62:
            return 65
        case 63:
            return 79
        case 64:
            return 67
        case 65:
            return 69
        case 66:
            return 81
        case 67:
            return 71
        case 68:
            return 83
        case 69:
            return 72
        case 70:
            return 84
        case 71:
            return 74
        case 72:
            return 76
        default:
            return note
        }
    }
    
    
    
    
    
    
    
    // MARK: - Умный лад бля белых клавиш правой руки (Smart scale)
    func whiteSmartScale(note: UInt8) -> UInt8 {
        let whiteSmartOn = HRFileOperations.shared.loadedSong.presetList[HRFileOperations.shared.preset].useSmartScaleForWhiteKeys
        if whiteSmartOn {
            let noteNumber = Int(note) % 12
            let chordName = NoteGenerator.singleton.currentChordName
            let offset = offsetArray[chordName]
            let newNote = Int(note) + offset![noteNumber]
            
            return UInt8(newNote)
        } else {
            return note
        }
    }
    
    
    
    
    
    // MARK: - Обычные (статичные) гаммы для белых клавиш
    func regularScale(note: UInt8) -> UInt8 {
        let preset = files.preset
        let scaleNumber = files.loadedSong.presetList[preset].regularScaleNumber
        let scale = regularSales[scaleNumber]
        
        let noteNumber = Int(note % 12)
        let newNote = Int(note) + scale[noteNumber]
        
        return UInt8(newNote)
    }
    
    
    
    
    
    // MARK: - Умный лад бля черных клавиш правой руки
    func blackSmartScale(note: UInt8) -> UInt8 {
        let listOfBlacks3: [UInt8 : [Int]] = [106: [1, 6], 123: [2, 8], 68: [0, 1], 99: [1, 5], 87: [2, 3], 82: [0, 3], 102: [2, 5], 70: [1, 1], 109: [2, 6], 114: [1, 7], 80: [2, 2], 111: [0, 7], 75: [0, 2], 121: [1, 8], 90: [0, 4], 118: [0, 8], 78: [1, 2], 85: [1, 3], 63: [1, 0], 97: [0, 5], 94: [2, 4], 116: [2, 7], 104: [0, 6], 92: [1, 4], 61: [0, 0], 66: [2, 0], 73: [2, 1], 126: [0, 9]]
        
        
        let listOfBlacks4: [UInt8 : [Int]] = [106: [3, 4], 123: [2, 6], 68: [3, 0], 99: [0, 4], 87: [3, 2], 82: [1, 2], 102: [1, 4], 70: [0, 1], 109: [0, 5], 114: [2, 5], 80: [0, 2], 111: [1, 5], 75: [2, 1], 121: [1, 6], 90: [0, 3], 118: [0, 6], 78: [3, 1], 85: [2, 2], 63: [1, 0], 97: [3, 3], 94: [2, 3], 116: [3, 5], 104: [2, 4], 92: [1, 3], 61: [0, 0], 66: [2, 0], 73: [1, 1], 126: [3, 6]]
        
        let fourNotesChords = [2, 3, 8, 9, 10]
        
        let blackSmartOn = HRFileOperations.shared.loadedSong.presetList[HRFileOperations.shared.preset].useSmartScaleForBlackKeys
        
        
        if blackSmartOn {
            let chordType = NoteGenerator.singleton.chordType
            let tonality = NoteGenerator.singleton.tonality + 3
            var noteInChord = 0
            var octave = 0
            var chordSubtype = 0
            
            let threeNoteChord = 5
            let fourNoteChord = 6
            
            if fourNotesChords.contains(chordType) {
                noteInChord = listOfBlacks4[note]?[0] ?? 0
                octave = listOfBlacks4[note]?[1] ?? 0
                chordSubtype = fourNoteChord
                
            } else {
                noteInChord = listOfBlacks3[note]?[0] ?? 0
                octave = listOfBlacks3[note]?[1] ?? 0
                chordSubtype = threeNoteChord
            }
            
            
            let chordNotes = HRChordList.shared.chordMatrix[chordSubtype][chordType][0]
            let offset = Int(chordNotes[noteInChord])
            
            let finalNote = ((octave + 5) * 12) + (tonality + 9) + offset
            return UInt8(finalNote)
            
            
        } else {
            return note
        }
    }
    
    
    
    
    
    
    
    
    
    
    // Определяет, какая клавиша нажата: белая или черная?
    func isWhite(pressedKey: UInt8) -> Bool {
        var result = true
        let noteNumber = pressedKey % 12
        let blackKeys: [UInt8] = [1, 3, 6, 8, 10]
        if blackKeys.contains(noteNumber) {
            result = false
        }
        
        return result
    }
    
    
    // Рассчитывает номера нот для отображения на экранной клавиатуре
    func calculateCurrentScale() {
        var note = Int()
        let transpose = GlobalSettings.singleton.transpose
        currentScale = []
        
        
        for noteNumber in musicData.diapasonOfScreenMelodycKeys {
            if isWhite(pressedKey: noteNumber) {
                note = Int(process(note: noteNumber, isWhiteKey: true)) + transpose
            } else {
                note = Int(process(note: noteNumber, isWhiteKey: false)) + transpose
            }
            
            let noteName = noteNames.noteNameFromMidiNote(note)
            currentScale.append(noteName)
        }
        
        // TODO: Нотификацию заменить
        NotificationCenter.default.post(name: NSNotification.Name.init("screenScaleUpdate"),  object: nil)
    }
    
    
    
    
    // Вспомогательная функция для генерации массива черных клавиш
    private func generateArrayOfBlacks() {
        var blacksArray = [UInt8 : [Int]]()
        var note = 0
        var octave = 0
        for noteNumber in musicData.diapasonOfMelodycKeys {
            if !isWhite(pressedKey: noteNumber) {
                blacksArray[noteNumber] = [note, octave]
                if note < 2 {
                    note += 1
                } else {
                    note = 0
                    octave += 1
                }
            }
        }
        print(blacksArray)
    }
    
    
}
