// Распределяет входящие ноты и посылает их на соответствующие обработки

import Foundation
import AudioKit

class NotePreProcessor {
    let pressedKeyStatusUpdater = KeyStatusUpdater()
    let scaleTransformer = ScaleTransformer.singleton
    let detecterComboKeyNumbers = DetecterComboKeyNumbers.singleton
    let senderToComboDetecter = DetectingQueue.singleton
    let musicBase = MusicDataBase()
    
    
    // Распределение входящих нот в мелодическом режиме
    
    func transformMelodicNoteOn(noteNumber: MIDINoteNumber, velocity: MIDIVelocity) -> NoteDataForResending? {
        
        pressedKeyStatusUpdater.updateMelodycNoteStatus(noteNubmer: noteNumber, isOn: 1)
        
        // Для всех режимов, кроме "Drumpad only" (в нем соло ноты не играют)
        if UserDefaults.standard.integer(forKey: "Plaing mode") != musicBase.drumPadMode {
            
            if scaleTransformer.isWhite(pressedKey: noteNumber) {
                
                let scaledNote = scaleTransformer.process(note: noteNumber, isWhiteKey: true)
                
                return NoteDataForResending(noteNumber: scaledNote,
                                            noteNumberBeforeProcessing: noteNumber,
                                            velocity: velocity,
                                            channel: nil,
                                            comboKeyNumber: musicBase.whiteMelodycKeysSoundset)
                
            } else {
                
                let scaledNote = scaleTransformer.process(note: noteNumber, isWhiteKey: false)
                
                let needseparateChannel = scaleTransformer.isBlackKeysOnSepareteChannel()
                
                if needseparateChannel {
                    return NoteDataForResending(noteNumber: scaledNote,
                                                noteNumberBeforeProcessing: noteNumber,
                                                velocity: velocity,
                                                channel: nil,
                                                comboKeyNumber: musicBase.blackMelodycKeysSoundset)
                    
                } else {
                    
                    return NoteDataForResending(noteNumber: scaledNote,
                                                noteNumberBeforeProcessing: noteNumber,
                                                velocity: velocity,
                                                channel: nil,
                                                comboKeyNumber: musicBase.whiteMelodycKeysSoundset)
                }
            }
            
        } else {
            return nil
        }
    }
    
    
    
    func transformMelodicNoteOff(_ noteNumber: MIDINoteNumber) -> NoteDataForResending? {
        
        pressedKeyStatusUpdater.updateMelodycNoteStatus(noteNubmer: noteNumber, isOn: 0)
        
        // Для всех режимов, кроме "Drumpad only" (в нем соло ноты не играют)
        if UserDefaults.standard.integer(forKey: "Plaing mode") != 0 {
            
            if scaleTransformer.isWhite(pressedKey: noteNumber) {
                
                return NoteDataForResending(noteNumber: noteNumber,
                                            noteNumberBeforeProcessing: nil,
                                            velocity: musicBase.mustBeZero,
                                            channel: nil,
                                            comboKeyNumber: musicBase.whiteMelodycKeysSoundset)
                
            } else {
                
                if scaleTransformer.isBlackKeysOnSepareteChannel() {
                    
                    return NoteDataForResending(noteNumber: noteNumber,
                                                noteNumberBeforeProcessing: nil,
                                                velocity: musicBase.mustBeZero,
                                                channel: nil,
                                                comboKeyNumber: musicBase.blackMelodycKeysSoundset)
                } else {
                    
                    return NoteDataForResending(noteNumber: noteNumber,
                                                noteNumberBeforeProcessing: nil,
                                                velocity: musicBase.mustBeZero,
                                                channel: nil,
                                                comboKeyNumber: musicBase.whiteMelodycKeysSoundset)
                }
            }
            
        } else {
            return nil
        }
    }
    
    
    
    
    
    
    
    // Распределение входящих нот в обычном игровом режиме (сочетаний клавиш)
    
    func sortingHotkeyNoteOn (noteNumber: MIDINoteNumber, velocity: MIDIVelocity) -> (hotkeySortingResult, NoteDataForResending?) {
        
        let scaledNote = scaleTransformer.transformHotkeyNotesToHandbandScale(note: noteNumber)
        let comboKeyNumber = detecterComboKeyNumbers.detectNumberFor(inputNoteNumber: scaledNote, isOn: 1)
        guard comboKeyNumber != nil else {return (.doNothing, nil)}
        
        
        if musicBase.buttonsForHotkeysAndChords.contains(comboKeyNumber!) {
            
            if isSystemButtonWithPlusOrMinusButtonPressed {
                return (.startSystemHotkeyDetecting, nil)
                
            } else {
                return (.startTriggeredChordDetecting,
                        NoteDataForResending(noteNumber: nil,
                                             noteNumberBeforeProcessing: nil,
                                             velocity: velocity,
                                             channel: nil,
                                             comboKeyNumber: nil))
            }
            
        } else if musicBase.buttonsForTriggeringSoundsets.contains(comboKeyNumber!) {
            
            if isSystemButtonPressed {
                return (.startSystemHotkeyDetecting, nil)
                
            } else {
                return (.playSoundset,
                        NoteDataForResending(noteNumber: scaledNote,
                                             noteNumberBeforeProcessing: nil,
                                             velocity: velocity,
                                             channel: nil,
                                             comboKeyNumber: comboKeyNumber))
            }
        }
        return (.doNothing, nil)
    }
    
    
    
    func sortingHotkeyNoteOff (_ noteNumber: MIDINoteNumber) -> (hotkeySortingResult, NoteDataForResending?) {
        let scaledNote = scaleTransformer.transformHotkeyNotesToHandbandScale(note: noteNumber)
        let comboKeyNumber = detecterComboKeyNumbers.detectNumberFor(inputNoteNumber: scaledNote, isOn: 0)
        guard comboKeyNumber != nil else {return (.doNothing, nil)}

        
        if musicBase.buttonsForHotkeysAndChords.contains(comboKeyNumber!) {
            return (.pauseChordDetecting, nil)
            
        } else {
            return (.stopChord,
                    NoteDataForResending(noteNumber: nil,
                                         noteNumberBeforeProcessing: nil,
                                         velocity: musicBase.mustBeZero,
                                         channel: nil,
                                         comboKeyNumber: comboKeyNumber!))
            
        }
    }
    
    
    
    
    // для краткости некоторые длинные условия вынесены в константы
    
    var isSystemButtonPressed: Bool {
        get {
            return HRKeyEquality.singleton.pressedComboKeys[musicBase.sharpButtonNumber] == 1
        }
    }
    
    var isSystemButtonWithPlusOrMinusButtonPressed: Bool {
        get {
            return (HRKeyEquality.singleton.pressedComboKeys[musicBase.sharpButtonNumber] == 1  &&        HRKeyEquality.singleton.pressedComboKeys[musicBase.plusButtonNumber] == 1) || HRKeyEquality.singleton.pressedComboKeys[musicBase.sharpButtonNumber] == 1  && HRKeyEquality.singleton.pressedComboKeys[musicBase.minusButtonNumber] == 1
        }
    }
    
    enum hotkeySortingResult {
        case playSoundset, startSystemHotkeyDetecting, startTriggeredChordDetecting, pauseChordDetecting, stopChord, doNothing
    }
}
