// Распознает номер комбо-клавиши, которая была нажата

import Foundation
import AudioKit

final class DetecterComboKeyNumbers {
    static let singleton = DetecterComboKeyNumbers()
    
    let keyStatusUpdater = KeyStatusUpdater()
    let settings = GlobalSettings.singleton
    let musicData = MusicDataBase()
    var countOfComboKeys: Int
    var defaultNoteNumbersForComboKeys: [UInt8]
    var customNoteNumbersForComboKeys: [UInt8]
    
    
    private init() {
        // Количество клавиш из регистра для обработки комбинаций (комбо-клавиш)
        countOfComboKeys = 16
        
        // Номера нот по умолчанию комбо-клавиш
        defaultNoteNumbersForComboKeys = [44, 45, 46, 47,
                                          49, 51, 54, 56,
                                          53, 55, 57, 59,
                                          48, 50, 52, 58]
        
        // Пользовательские номера нот для комбо-клавиш
        customNoteNumbersForComboKeys = Array.init(repeating: UInt8(48), count: countOfComboKeys)
    }
    
  
    
    
    // Опознает номер комбо-клавиши, которая была нажата
    // После этого обновляет статус "нажата/отжата"
    
    func detectNumberFor(inputNoteNumber: MIDINoteNumber, isOn: Int) -> Int? {
        if customNoteNumbersForComboKeys.contains (inputNoteNumber) {
            
            let detectedComboKeyNumber = customNoteNumbersForComboKeys.index(of: inputNoteNumber)!
            
            keyStatusUpdater.updateComboKeyStatus(comboKeyNumber: detectedComboKeyNumber, isOn: isOn)
            
            return detectedComboKeyNumber
            
        } else {
            return nil
        }
    }
    
    
    
    
    
    // При первом запуске приложения UserDefaults вернет значение 0.
    // В таком случае нужно проинициализировать настройки
    // значениями с номерами нот по умолчанию.
    
    func getDefaultValueInFirstAppLaunching (value: UInt8, number: Int) -> UInt8 {
        var result = value
        let userDefaultInitValue = 0
        
        if result == userDefaultInitValue {
            result = defaultNoteNumbersForComboKeys[number]
            UserDefaults.standard.set(result, forKey: "p\(number+1)")
        }
        return result
    }
    
    
    
    
    // Загрузка/инициализация номеров комбо-клавиш.
    // В режиме настроек "DrumPads only" (case 0)
    // пользователь может изменять номера нот для комбо клавиш.
    // Во всех остальных режимов загружаются стандартные номера.
    
    func loadNoteNumbersForComboKeys() {
        
        if settings.setMode != musicData.drumPadMode {
            customNoteNumbersForComboKeys = defaultNoteNumbersForComboKeys
            
        } else {
            for i in 0 ..< countOfComboKeys {
                var storedComboKeyNoteNumber = UInt8( UserDefaults.standard.integer(forKey: "p\(i+1)"))
                storedComboKeyNoteNumber = getDefaultValueInFirstAppLaunching(value: storedComboKeyNoteNumber, number: i)
                customNoteNumbersForComboKeys[i] = storedComboKeyNoteNumber
            }
        }
    }
 
}
