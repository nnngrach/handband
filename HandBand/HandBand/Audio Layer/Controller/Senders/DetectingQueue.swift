
import Foundation

final class DetectingQueue {
    static let singleton = DetectingQueue()
    let hk = HotKeyController.singleton
    private init() {}
    
    
    // Синхронная очередь без многопоточности
    // Для отложенной обработки клавиш в порядке их нажатия
    
    var delayQueue: OperationQueue = {
        let queue = OperationQueue()
        queue.maxConcurrentOperationCount = 1
        queue.name = "Chord Pads to Analis"
        return queue
    } ()
    
    
    
    
    func launchChordDetecter (velocity: UInt8) {
        delayQueue.addOperation {
            self.hk.playChord(velocity: velocity)
        }
    }
    
    
    func resetChordDetecter () {
        delayQueue.addOperation {
            self.hk.stopChord()
        }
    }
    
    
    func launchSystemHotKeysDetecter () {
        delayQueue.addOperation {
            self.hk.hotkeysAnalisys()
        }
    }
}
