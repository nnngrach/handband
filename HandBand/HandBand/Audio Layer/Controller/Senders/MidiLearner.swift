// MIDI Learn: Сохраняет новый номер ноты для нажатой комбо-клавиши

import Foundation
import AudioKit

class MidiLearner {
    let detecterComboKeyNumbers = DetecterComboKeyNumbers.singleton
    let screenButtonsUpdater = SenderToScreenButtonsUpdater()
    
    
    func assignTo(comboKeyNumber: Int, midiNoteNumber: MIDINoteNumber) {
        // сохранить номер сыгранной в этом режиме ноты в базу
        UserDefaults.standard.set( midiNoteNumber , forKey: "p\(comboKeyNumber)")
        
        // обновить номер в загруженном массиве
        detecterComboKeyNumbers.loadNoteNumbersForComboKeys()
        
        // послать отчет о выполнении, обновить интерфейс
        screenButtonsUpdater.screenLearnButtonsUpdate(text: "\(midiNoteNumber)")
    }
    
}
