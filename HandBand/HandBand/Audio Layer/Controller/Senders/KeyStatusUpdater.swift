// Обновляет массив нажатых и ненажатых клавиш.
// Вызывает обновление клавиш на экране.

import Foundation
import AudioKit

class KeyStatusUpdater {
    // TODO: Сыылка на файл до рефактора
    let hotkeysChecker = HRKeyEquality.singleton
    let screenUpdater = SenderToScreenButtonsUpdater()
    let musicBase = MusicDataBase()
    
    var pressedComboKeys: [Int] {
        get {
            return hotkeysChecker.pressedComboKeys
        }
    }
    
    // TODO: Почему IsOn - Int а не Bool ? Ниже тоже
    func updateComboKeyStatus(comboKeyNumber: Int, isOn: Int) {
        hotkeysChecker.pressedComboKeys[comboKeyNumber] = isOn
        screenUpdater.screenComboButtonsUpdate(tagNumber: "\(comboKeyNumber)")
    }
    
    
    func updateMelodycNoteStatus(noteNubmer: MIDINoteNumber, isOn: Int) {
        if musicBase.diapasonOfScreenMelodycKeys.contains(noteNubmer) {
            let number = Int(noteNubmer - musicBase.diapasonOfScreenMelodycKeys[0])
            hotkeysChecker.keysON[number] = isOn
            screenUpdater.screenMelodycButtonsUpdate()
        }
    }
}
