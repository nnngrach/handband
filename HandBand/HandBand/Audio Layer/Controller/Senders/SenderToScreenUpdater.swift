// Отправляет уведомления во View, чтобы обновить экранные кнопки.
// Для этого работает с интерфейсом через ассинхронную очередь.

import Foundation

class SenderToScreenButtonsUpdater {
    
    // Отправить уведомления с техтом аккорда на дисплейчик
    func sendToDisplay(text: String) {
        DispatchQueue.main.async(execute: {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "sendToDisplay"),
                    object: nil,
                    userInfo: [
                        "text": text
                ])
        })
    }

    
    
    // Сообщить о необходимости обновить комбо-клваиши на интерфейсе
    func screenComboButtonsUpdate(tagNumber: String) {
        DispatchQueue.main.async(execute: {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "screenPadsupdate"),
                                            object: nil,
                                            userInfo: [
                                                "text": tagNumber
                ])
        })
    }
    
    
    
    // Сообщить о необходимости обновить мелодические клваиши на интерфейсе
    func screenMelodycButtonsUpdate() {
        DispatchQueue.main.async(execute: {
            NotificationCenter.default.post(name: NSNotification.Name.init("screenPadsupdate"),  object: nil)
        })
    }
    
    
    
    
    // Сообщить о нербходимости обновления комбо-клавиш для экрана MIDI Learn
    func screenLearnButtonsUpdate(text: String) {
        DispatchQueue.main.async(execute: {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Pad is learned"),
                                            object: nil,
                                            userInfo: [
                                                "text": text
                ])
        })
    }
    
  
    
}
