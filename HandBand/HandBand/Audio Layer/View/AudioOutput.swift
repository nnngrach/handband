// Аудиодвижок прлучает от Аудиоконтроллера сгенерированные ноты
// и отправляет их на MIDI-выход приложения для воспроизведение

import Foundation
import AudioKit

extension AudioInOut {
    
    // Отправить старт ноты
    public func noteOn(channel: MIDIChannel, noteNumber: MIDINoteNumber, velocity: MIDIVelocity) {
        midiEngine.sendNoteOnMessage(noteNumber: noteNumber, velocity: velocity, channel: channel)
        // print("Output Note ON: \(noteNumber), Velocity: \(velocity), Channel: \(channel)")
    }
    
    
    // Отправить конец ноты
    public func noteOff(channel: MIDIChannel, noteNumber: MIDINoteNumber, velocity: MIDIVelocity) {
        midiEngine.sendNoteOffMessage(noteNumber: noteNumber, velocity: velocity, channel: channel)
        // print("Output Note OFF: \(noteNumber), Velocity: \(velocity), Channel: \(channel)")
    }
    
    // Отправить midi CC
    public func ccOut (channel: MIDIChannel, cc: MIDIByte, value: MIDIByte) {
        let event = AKMIDIEvent(controllerChange: cc, value: value, channel: channel)
        midiEngine.sendEvent(event)
        // print("Output CC: \(cc), Value: \(value), Channel: \(channel)")
    }
    
    // Отправить Programm Change
    public func programmChangeOut (channel: MIDIChannel, number: MIDIByte) {
        let event = AKMIDIEvent(programChange: number, channel: channel)
        midiEngine.sendEvent(event)
        // print("Output ProgChange: \(number), Channel: \(channel)")
    }
    
    
    
    // =============
    // Команды в формате шестнадцатеричных команд
    
    public func sendNoteOnData(channel: MIDIChannel, noteNumber: MIDINoteNumber, velocity: MIDIVelocity) {
        let noteCommand: MIDIByte = MIDIByte(0x90) + channel
        let message: [MIDIByte] = [noteCommand, noteNumber, velocity]
        let event = AKMIDIEvent(data: message)
        midiEngine.sendEvent(event)
    }
    
    public func sendNoteOffData(channel: MIDIChannel, noteNumber: MIDINoteNumber, velocity: MIDIVelocity) {
        let noteCommand: MIDIByte = MIDIByte(0x80) + channel
        let message: [MIDIByte] = [noteCommand, noteNumber, velocity]
        let event = AKMIDIEvent(data: message)
        midiEngine.sendEvent(event)
    }
    
    public func sendPolyPressureData(channel: MIDIChannel, noteNumber: MIDINoteNumber, value: MIDIByte) {
        let noteCommand: MIDIByte = MIDIByte(0xA0) + channel
        let message: [MIDIByte] = [noteCommand, noteNumber, value]
        let event = AKMIDIEvent(data: message)
        midiEngine.sendEvent(event)
    }
    
    public func sendChannelPressureData(channel: MIDIChannel, value: MIDIByte) {
        let command: MIDIByte = MIDIByte(0xD0) + channel
        let message: [MIDIByte] = [command,  value, 0]
        let event = AKMIDIEvent(data: message)
        midiEngine.sendEvent(event)
    }
    
    
    
    /// Send a pitch bend message.
    ///
    /// - Parameters:
    ///   - value: Value of pitch shifting between 0 and 16383. Send 8192 for no pitch bending.
    ///   - channel: Channel you want to send pitch bend message. Defaults 0.
    public func sendPitchBendData(channel: MIDIChannel, value: UInt16) {
        let pitchCommand = MIDIByte(0xE0) + channel
        let mask: UInt16 = 0x007F
        let byte1 = MIDIByte(value & mask) // MSB, bit shift right 7
        let byte2 = MIDIByte((value & (mask << 7)) >> 7) // LSB, mask of 127
        let message: [MIDIByte] = [pitchCommand, byte1, byte2]
        let event = AKMIDIEvent(data: message)
        midiEngine.sendEvent(event)
        //print("Output Pitchband value: \(value), channel: \(channel)")
    }
    
}
