// При получении Midi-сообщений
// Аудиодвижок просто пересылает их с помощью делегации
// на Аудиоконтроллер для дальнейшей обработки.

import Foundation
import AudioKit

extension AudioInOut {
    
    func receivedMIDINoteOn(noteNumber: MIDINoteNumber, velocity: MIDIVelocity, channel: MIDIChannel) {
        delegate?.receiveNoteOn(noteNumber, velocity)
        // print("Input Note ON   -  \(noteNumber)")
    }
    
    func receivedMIDINoteOff(noteNumber: MIDINoteNumber, velocity: MIDIVelocity, channel: MIDIChannel) {
        delegate?.receiveNoteOff(noteNumber)
        // print("Input Note OFF  -  \(noteNumber)")
    }
    
    
    
    func receivedMIDIController(_ controller: MIDIByte, value: MIDIByte, channel: MIDIChannel) {
        delegate?.receiveCC(controller, value)
        // print("Input CC: \(controller), value: \(value) ")
    }
    
    
    
    func receivedMIDIAftertouch(noteNumber: MIDINoteNumber, pressure: MIDIByte, channel: MIDIChannel) {
        delegate?.receivePolypressure(noteNumber, pressure)
        // print("Input PoliPressute Note: \(noteNumber) pressure: \(pressure) ")
        
    }
    
    
    
    
    func receivedMIDIAfterTouch(_ pressure: MIDIByte, channel: MIDIChannel) {
        delegate?.receiveChannelAftertouch(pressure)
        // print("Input Channel Aftertouch pressure: \(pressure) ")
    }
    
    
    
    func receivedMIDIPitchWheel(_ pitchWheelValue: MIDIWord, channel: MIDIChannel) {
        delegate?.receivePitchWheel(pitchWheelValue)
        // print("Pitchband: \(pitchWheelValue)")
    }
    
    
}
