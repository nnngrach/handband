// Аудио-движок на фреймворке AudioKit.
// Внимание: все операции с аудиодвижком должны находиться только в этом модуле!

import Foundation
import AudioKit

final class AudioInOut:  AKMIDIListener {
    static let singleton = AudioInOut()
    let midiEngine = AudioKit.midi
    var delegate: AudioInListenerDelegate?
    var inputNamesList = [""]

    
    // Включение аудио-движка и его стартовая настроейка
    
    private  init() {
        midiEngine.openInput("Session 1")
        midiEngine.openOutput("Session 1")
        AKSettings.bufferLength = .medium
        AKSettings.enableLogging = false
        AKSettings.playbackWhileMuted = true
        
        do {
            try AKSettings.setSession(category: .playAndRecord, with: [.defaultToSpeaker, .allowBluetooth, .allowBluetoothA2DP, .mixWithOthers])
        } catch {
            AKLog("Could not set session category.")
        }
        
        midiEngine.createVirtualPorts()
        let oscillator = AKOscillator()
        AudioKit.output = oscillator
        
        do {
            try AudioKit.start()
        } catch {
            print("AudioKit.start() failed")
        }
        
        midiEngine.addListener(self)
        inputNamesList = midiEngine.inputNames
    }
    
    
    
    
    // Действия при подключении и отключении MIDI устройств
    
    func receivedMIDISetupChange() {
        if inputNamesList != midiEngine.inputNames {
            midiEngine.closeInput()
            midiEngine.destroyVirtualPorts()
            
            // print("midi setup change, midi.inputNames: \(midiEngine.inputNames)")
            let inputNames = midiEngine.inputNames
            inputNames.forEach { inputName in
                midiEngine.openInput(inputName)
            }
            midiEngine.openOutput("Session 1")
            midiEngine.createVirtualPorts()
            
            inputNamesList = midiEngine.inputNames
        }
    }
    
    
    // Действия при получении системных сообщений
    
    func receivedMIDISystemCommand(_ data: [MIDIByte]) {
        if let command = AKMIDISystemCommand(rawValue: data[0]) {
            var newString = "MIDI System Command: \(command) \n"
            for i in 0 ..< data.count {
                newString.append("\(data[i]) ")
            }
            // print(newString)
        }
    }
    
}
