// Выбор, для какого комплекта оборудования
// нужно настроить интерфейс приложжения?


import UIKit

class PlayModeSetVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var modeTableVew: UITableView!
    @IBOutlet weak var modeLabel: UILabel!
    @IBOutlet weak var modeImageView: UIImageView!
    let sets = GlobalSettings.singleton
    let midiIdentifier = DetecterComboKeyNumbers.singleton
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let index = IndexPath(item: sets.setMode, section: 0)
        modeTableVew.selectRow(at: index, animated: true, scrollPosition: .top)
        modeLabel.text = descriptions[sets.setMode]
        modeImageView.image = UIImage(named: images[sets.setMode])
    }

    
    
    
    // MARK: TableView settings
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellNames.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell = (self.modeTableVew.dequeueReusableCell(withIdentifier: "cell") as UITableViewCell?)!
        cell.textLabel?.text = cellNames[indexPath.row]
        cell.textLabel?.textColor = .white
        
        if indexPath.row == sets.setMode {
            cell.isSelected = true
            cell.contentView.backgroundColor = #colorLiteral(red: 0.4352941176, green: 0.3411764706, blue: 0.3450980392, alpha: 1)
        } else {
            cell.isSelected = false
            cell.contentView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        sets.setMode = indexPath.row
        midiIdentifier.loadNoteNumbersForComboKeys()
        modeLabel.text = descriptions[indexPath.row]
        modeImageView.image = UIImage(named: images[indexPath.row])
        
        let selectedCell:UITableViewCell = tableView.cellForRow(at: indexPath)!
        selectedCell.contentView.backgroundColor = #colorLiteral(red: 0.4352941176, green: 0.3411764706, blue: 0.3450980392, alpha: 1)
    }
    
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let selectedCell:UITableViewCell = tableView.cellForRow(at: indexPath)!
        selectedCell.contentView.backgroundColor = UIColor.clear
    }
    
    
    
    
    
    // MARK: Text descriptions
    
    
    let cellNames = ["Drumpad only", "Drumpad + keyboard", "Two octave keyboard", "Full sized keyboard"]
    
    let descriptions = ["In this mode you can quicly plug your drumpads, set it up with MIDI Learn function and play song accompaniment. You can't use extantion MIDI-keyboards for play solo in this mode.",
                        "If you want to play melodyes on extantion MIDI_keyboard you have to change your drumpads settings. Just set up midi notes numbers like on this image. MIDI Learn not allowed in this mode",
                        "If you want get maximum from you two octave keyboard change this smart scale. Left octave uses to trigger chord. Right octave uses to playing solo. Triggerdr chord in left octave changes scale in right octave.",
                        "Octave C3 - B3 uses to trigger chords. All another octaves uses to play melodyes. No smart scales. Just transposing. But you can turn on smart scale in settings of selected preset."]
    
    let images = [      "mpc.png",
                        "drumm key.png",
                        "two oct.png",
                        "full octave.png"]
   
}
