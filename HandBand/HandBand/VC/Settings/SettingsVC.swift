// Главное меню с настройками приложения

import UIKit

class SettingsVC: UIViewController {
    
    @IBOutlet weak var switchFirchChannel: UISwitch!
    @IBOutlet weak var drumpadMidiLern: UIButton!
    let sets = GlobalSettings.singleton
    
    
    override func viewWillAppear(_ animated: Bool) {
        updateUI()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    
    func updateUI() {
        switchFirchChannel.isOn = sets.setUnclockFirstChannel
        
        if sets.setMode != 0 {
            drumpadMidiLern.isEnabled = false
            //drumpadMidiLern.backgroundColor = .black
            
            
        } else {
            drumpadMidiLern.isEnabled = true
            //drumpadMidiLern.backgroundColor = .white
        }
    }
    
    
    
    // Добавить функцию Разблокировать первый MIDI канал
    @IBAction func unlockChannelAct(_ sender: Any) {
    }
    
}
