// Стартовый экран.
// Выбирает, какой из сторибордов загрузить: клавишный или с драм-падом

import UIKit

class StartVC: UIViewController {
    let sets = GlobalSettings.singleton

    var padVc: PadVC!
    var pianoVc: PianoVC!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        selectVC()
    }
    
    func selectVC()  {
        if sets.setMode < 2 {
            padVc = self.storyboard?.instantiateViewController(withIdentifier: "PadVC") as? PadVC
            self.padVc.view.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height)
            self.addChild(self.padVc)
            self.view.addSubview(self.padVc.view)
            
            
        } else {
            pianoVc = self.storyboard?.instantiateViewController(withIdentifier: "PianoVC") as? PianoVC
            self.pianoVc.view.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height)
            self.addChild(self.pianoVc)
            self.view.addSubview(self.pianoVc.view)
        }
    }
   

}
