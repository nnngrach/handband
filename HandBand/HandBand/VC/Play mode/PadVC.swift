// Интерфейс для игры в режиме драм падов

import UIKit

class PadVC: UIViewController {
    let hk = HotKeyController.singleton
    let k = HRKeyEquality.singleton
    let chorder = NoteGenerator.singleton
    let midiIn = AudioInOut.singleton
    let midiIdentifier = DetecterComboKeyNumbers.singleton
    let sets = GlobalSettings.singleton
    
    
    // Свойства кнопок и меток
    @IBOutlet var padsOutlet: [HRButton]!
    @IBOutlet weak var displayBackground: UIView!
    @IBOutlet weak var displayText: UILabel!
    var padLinks = [Bool]()
    var note = UInt8()

   
    override func viewDidLoad() {
        super.viewDidLoad()
        labelsDefault()
        addObservers()
    }
    
    

    
    // MARK: Changing labels
    // Надписи на кнопках по умолчанию
    func labelsDefault() {
        var title:String
        let tr = sets.transpose
        
        for pad in padsOutlet {
            switch pad.tag {
            case 8:
                title = hk.keyName(num: -4 + tr)
            case 9:
                title = hk.keyName(num: -2 + tr)
            case 10:
                title = hk.keyName(num: 0 + tr)
            case 11:
                title = hk.keyName(num: 2 + tr)
            case 12:
                title = hk.keyName(num: 3 + tr)
            case 13:
                title = hk.keyName(num: 5 + tr)
            case 14:
                title = hk.keyName(num: -5 + tr)
            default:
                title = ""
            }
            
            if (pad.tag != 3) && (pad.tag != 15) {
                pad.setTitle(title, for: .normal)
            }
        }
    }
    
    
    
    // Надписи на пэдах при нажатой кнопке #
    func labelsRename() {
        var title:String
        let tr = sets.transpose
        
        for pad in padsOutlet {
            switch pad.tag {
            case 0:
                title = "Preset 5"
            case 1:
                title = "Preset 6"
            case 2:
                title = "Preset 7"
            case 4:
                title = "Preset 1"
            case 5:
                title = "Preset 2"
            case 6:
                title = "Preset 3"
            case 7:
                title = "Preset 4"
                
            case 8:
                title = hk.keyName(num: -3 + tr)
            case 9:
                title = hk.keyName(num: -1 + tr)
            case 10:
                title = hk.keyName(num: 1 + tr)
            case 11:
                title = "+"
            case 12:
                title = hk.keyName(num: 4 + tr)
            case 13:
                title = hk.keyName(num: 6 + tr)
            case 14:
                title = "-"
            default:
                title = ""
            }
            
            
            if (pad.tag != 3) && (pad.tag != 15) {
                pad.setTitle(title, for: .normal)
            }
        }
    }
    
    
    
    // Надписи на пэдах при нажатой кнопке #  и плюс или минус
    func labelsRenamePlus() {
        var title:String
        let tr = sets.transpose
        
        for pad in padsOutlet {
            switch pad.tag {
            case 4:
                title = "Smart Scale"
            case 5:
                title = "Transpose"
            case 6:
                title = "Keys Octave"
            case 7:
                title = "Page"
                
            case 8:
                title = hk.keyName(num: -3 + tr)
            case 9:
                title = hk.keyName(num: -1 + tr)
            case 10:
                title = hk.keyName(num: 1 + tr)
            case 11:
                title = "+"
            case 12:
                title = hk.keyName(num: 4 + tr)
            case 13:
                title = hk.keyName(num: 6 + tr)
            case 14:
                title = "-"
            default:
                title = ""
            }
            
            
            if (pad.tag != 3) && (pad.tag != 15) {
                pad.setTitle(title, for: .normal)
            }
        }
    }
  
    
    
    
    
    
    
    
    
    
    // MARK: Notifications
    
    func addObservers() {
        NotificationCenter.default.addObserver(forName:NSNotification.Name(rawValue: "sendToDisplay"), object:nil, queue:nil, using:displayUpdate)
        
        NotificationCenter.default.addObserver(forName:NSNotification.Name(rawValue: "screenPadsupdate"), object:nil, queue:nil, using:screenPadsupdate)
    }
    
    
    // Обновление текста на дисплейчике
    @objc func displayUpdate(notification:Notification) -> Void {
        // пробуем получить данные из уведомления
        guard
            let userInfo = notification.userInfo,
            let message  = userInfo["text"] as? String else { return }
        
        // и обновить интерфейс из ассинхронной очерели
        DispatchQueue.main.async(execute: {
            //self.padsOutlet[3].setTitle(message, for: .normal)
            self.displayText.text = message
        })
    }
    

    
    
    // При нажатии механических кнопок, сделать нажатыми соответствующие кнопки на экране
    @objc func screenPadsupdate(notification:Notification) -> Void {
        
        // обновим интерфейс из ассинхронной очерели, чтоб не ругался
        DispatchQueue.main.async(execute: {
            
            // проверяем, какие пэды сейчас нажаты
            // и нажимаем соответствующие им на экране
            for pad in self.padsOutlet {
                if self.k.pressedComboKeys[pad.tag] == 1 {
                    pad.isHighlighted = true
                } else {
                    pad.isHighlighted = false
                }
                //pad.isHighlighted = self.hk.padsOn[pad.tag]
            }
            
            
            
            // меняем метки на пэдах
            if self.k.pressedComboKeys[15] == 1 && (self.k.pressedComboKeys[11] == 1 || self.k.pressedComboKeys[14] == 1) {
                self.labelsRenamePlus()
                
            } else if self.k.pressedComboKeys[15] == 1 {
                
                self.labelsRename()
                
            } else {
                self.labelsDefault()
            }
        })
    }
    

    
    
    
    // MARK: Buttons
    
    @IBAction func panicBtn(_ sender: Any) {
        chorder.midiPanic()
        // TODO: Нарисовать все экранные кнопки отжатыми
    }
    
    
    
    @IBAction func padsGroupTouchDown(_ sender: HRButton) {
        if sets.setMode != 0 {
            note = midiIdentifier.defaultNoteNumbersForComboKeys[sender.tag]
        } else {
            note = midiIdentifier.customNoteNumbersForComboKeys[sender.tag]
        }
        
        midiIn.receivedMIDINoteOn(noteNumber: note, velocity: 110, channel: 0)
        if sender.tag == 3 {
            displayBackground.backgroundColor = #colorLiteral(red: 0.26, green: 0.5140909091, blue: 0.52, alpha: 1)
            displayText.textColor = #colorLiteral(red: 0.4479663867, green: 0.6368149614, blue: 0.6412067887, alpha: 1)
        }
    }
    
    @IBAction func padsGroupTouchUp(_ sender: HRButton) {
        if sets.setMode != 0 {
            note = midiIdentifier.defaultNoteNumbersForComboKeys[sender.tag]
        } else {
            note = midiIdentifier.customNoteNumbersForComboKeys[sender.tag]
        }
        
        midiIn.receivedMIDINoteOff(noteNumber: note, velocity: 0, channel: 0)
        if sender.tag == 3 {
            displayBackground.backgroundColor = #colorLiteral(red: 0.4, green: 0.568627451, blue: 0.5725490196, alpha: 1)
            displayText.textColor = #colorLiteral(red: 0.1333333333, green: 0.3019607843, blue: 0.3960784314, alpha: 1)
        }

    }
}
