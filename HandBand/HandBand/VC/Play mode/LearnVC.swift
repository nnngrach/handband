// Интерфейс Midi-Learn

// (Если у пользователя не типичная midi-клавиатура)
// (то он может "обучить" программу для работы с ней.)
// (Для этого ему нужно вручную нажать механическую кнопку)
// (и соответсвующую ей кнопку на экране. И так для всех кнопок)

import UIKit

class LearnVC: UIViewController {
    @IBOutlet var allButtons: [HRButton]!
    let audioController = AudioController.singleton

    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateLabels()

        NotificationCenter.default.addObserver(forName:NSNotification.Name(rawValue: "Pad is learned"), object:nil, queue:nil, using:whenMidiLerned)
    }

    
    @IBAction func padPtn(_ sender: HRButton) {
        audioController.comboKeyNumber = sender.tag

        if audioController.isMidiLearnModeActive {
            audioController.isMidiLearnModeActive = true
            allButtons[audioController.comboKeyNumber - 1].setTitle("MIDI Learn", for: .normal)
            holdAll()
        } else {
            audioController.isMidiLearnModeActive = false
            allButtons[sender.tag - 1].isHighlighted = false
            unholdAll()
        }
    }
    
  
  
    @objc func whenMidiLerned(notification:Notification) -> Void {
        DispatchQueue.main.async(execute: {
            self.unholdAll()
        })
    }
    
    
    
    // выделить все кнопки
    func holdAll() {
        for button in allButtons {
            button.backgroundColor = button.pressedColor
        }
    }

    
    
    // снять выделение со всех кнопок
    func unholdAll () {
        audioController.isMidiLearnModeActive = false
        updateLabels()
        
        for button in allButtons {
            button.backgroundColor = button.copyOffcolor
        }
    }
    
    
    
    
    // обновить надписи на кнопках
    func updateLabels() {
        for button in allButtons {
            // получаем из базы номер ноты соответствующий тегу кнопки
            let noteText = String( UserDefaults.standard.integer(forKey: "p\(button.tag)") )
            button.setTitle(noteText, for: .normal)
        }
    }
    
}
