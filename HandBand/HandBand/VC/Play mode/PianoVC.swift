// Интерфейс для ингры в фортепианном режиме

import UIKit

class PianoVC: UIViewController {
    @IBOutlet weak var displayLabel: UILabel!
    @IBOutlet var buttonsLeft: [HRButton]!
    @IBOutlet var rightButtons: [HRButton]!
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label3: UILabel!
    @IBOutlet weak var label4: UILabel!
    
    
    let midiIn = AudioInOut.singleton
    let midiIdentifier = DetecterComboKeyNumbers.singleton
    let hk = HotKeyController.singleton
    let chorder = NoteGenerator.singleton
    let k = HRKeyEquality.singleton
    let sets = GlobalSettings.singleton
    let scale = ScaleTransformer.singleton
    
    let colorRegular = #colorLiteral(red: 0.5411764706, green: 0.5490196078, blue: 0.5294117647, alpha: 1)
    let colorPlus = #colorLiteral(red: 0.3961697277, green: 0.4019113179, blue: 0.3875573423, alpha: 1)
    var mode = 0

    
    override func viewDidLoad() {
        super.viewDidLoad()
        leftLabelsDefault()
        rightOctaveRename()
        addObservers()
    }

    
    
    // MARK: Notifications
    
    func addObservers() {
        NotificationCenter.default.addObserver(forName:NSNotification.Name(rawValue: "sendToDisplay"), object:nil, queue:nil, using:displayUpdate)
        
        NotificationCenter.default.addObserver(forName:NSNotification.Name(rawValue: "screenPadsupdate"), object:nil, queue:nil, using:screenPadsupdate)
        
        NotificationCenter.default.addObserver(forName:NSNotification.Name(rawValue: "screenScaleUpdate"), object:nil, queue:nil, using:screenScaleUpdate)
        
        NotificationCenter.default.addObserver(forName:NSNotification.Name(rawValue: "smartScalesChanged"), object:nil, queue:nil, using:screenScaleUpdate)
    }
    
    
    // Действие по уведомлению
    @objc func displayUpdate(notification:Notification) -> Void {
        // пробуем получить данные из уведомления
        guard
            let userInfo = notification.userInfo,
            let message  = userInfo["text"] as? String else { return }
        
        // и обновть интерфейс из ассинхронной очереди
        DispatchQueue.main.async(execute: {
            self.displayLabel.text = message
        })
    }


    
    @objc func screenPadsupdate(notification:Notification) -> Void {        
        // обновим интерфейс из ассинхронной очерели, чтоб не ругался
        DispatchQueue.main.async(execute: {
                self.leftOctaveRename()
            })
    }
    
    
    @objc func screenScaleUpdate(notification:Notification) -> Void {
        DispatchQueue.main.async(execute: {
            self.leftOctaveRename()
            self.rightOctaveRename()
        })
    }
    
    
    
    
    
    
    // MARK: Labels renaming
    
    // Общий алгоритм переименования меток на клавишах
    func leftOctaveRename() {
        // меняем метки на пэдах
        if (k.pressedComboKeys[15] == 1)  &&  (k.pressedComboKeys[11] == 1 || k.pressedComboKeys[14] == 1) {
            self.leftLabelsRenamePlus()
            
        } else if k.pressedComboKeys[15] == 1 {
            leftLabelsRename()
            
        } else {
            leftLabelsDefault()
        }
        
        pressTheButtons()
    }
    

    

    
    // проверяем, какие пэды сейчас нажаты
    // и нажимаем соответствующие им на экране
    func pressTheButtons() {
        var number = 0
        
        for pad in buttonsLeft {
            number = pad.tag
            if sets.setMode == 2 {
                switch pad.tag {
                case 12:
                    number = 14
                case 13:
                    number = 8
                case 14:
                    number = 9
                case 8:
                    number = 10
                case 9:
                    number = 11
                case 10:
                    number = 12
                case 11:
                    number = 13
                default:
                    break
                }
            }
            
            if k.pressedComboKeys[number] == 1 {
                pad.isHighlighted = true
            } else {
                pad.isHighlighted = false
            }
        }
        
        for pad in rightButtons {
            if k.keysON[pad.tag] == 1 {
                pad.isHighlighted = true
            } else {
                pad.isHighlighted = false
            }
        }
    }
    
    
    
    
    // MARK: Renaming labels
    // Обновление надвисей на кнопках
    
    // Левая октава - по мумолчанию
    func leftLabelsDefault() {
        var title:String
        let tr = sets.transpose
        label1.transform = CGAffineTransform(rotationAngle: -CGFloat.pi / 2)
        label2.transform = CGAffineTransform(rotationAngle: -CGFloat.pi / 2)
        label3.transform = CGAffineTransform(rotationAngle: -CGFloat.pi / 2)
        label4.transform = CGAffineTransform(rotationAngle: -CGFloat.pi / 2)
        label1.text = ""
        label2.text = ""
        label3.text = ""
        label4.text = ""
        
        
        for pad in buttonsLeft {
            
            if sets.setMode != 2 {
                switch pad.tag {
                case 8:
                    title = hk.keyName(num: -4 + tr)
                case 9:
                    title = hk.keyName(num: -2 + tr)
                case 10:
                    title = hk.keyName(num: 0 + tr)
                case 11:
                    title = hk.keyName(num: 2 + tr)
                case 12:
                    title = hk.keyName(num: 3 + tr)
                case 13:
                    title = hk.keyName(num: 5 + tr)
                case 14:
                    title = hk.keyName(num: -5 + tr)
                default:
                    title = ""
                }
                
            } else {
                switch pad.tag {
                case 8:
                    title = hk.keyName(num: 0 + tr)
                case 9:
                    title = hk.keyName(num: 2 + tr)
                case 10:
                    title = hk.keyName(num: 3 + tr)
                case 11:
                    title = hk.keyName(num: 5 + tr)
                case 12:
                    title = hk.keyName(num: -5 + tr) // c
                case 13:
                    title = hk.keyName(num: -4 + tr)
                case 14:
                    title = hk.keyName(num: -2 + tr)
                default:
                    title = ""
                }
            }
            
            if pad.tag != 15 {
                pad.setTitle(title, for: .normal)
            }
            
            if ![4, 5, 6, 7, 15].contains(pad.tag) {
                pad.backgroundColor = colorRegular
            }
        }
    }
   
    
    // Левая октава  +  кнопка #
    func leftLabelsRename() {
        var title:String
        var color = colorRegular
        let tr = sets.transpose
        label1.text = "Preset 1"
        label2.text = "Preset 2"
        label3.text = "Preset 3"
        label4.text = "Preset 4"
        
        for pad in buttonsLeft {
            color = colorRegular
            
            if sets.setMode != 2 {
                switch pad.tag {
                case 8:
                    title = hk.keyName(num: -3 + tr)
                case 9:
                    title = hk.keyName(num: -1 + tr)
                case 10:
                    title = hk.keyName(num: 1 + tr)
                case 11:
                    title = "+"
                    color = colorPlus
                case 12:
                    title = hk.keyName(num: 4 + tr)
                case 13:
                    title = hk.keyName(num: 6 + tr)
                case 14:
                    title = "-"
                    color = colorPlus
                default:
                    title = ""
                }
                
            } else {
                switch pad.tag {
                case 13:
                    title = hk.keyName(num: -3 + tr)
                case 14:
                    title = hk.keyName(num: -1 + tr)
                case 8:
                    title = hk.keyName(num: 1 + tr)
                case 9:
                    title = "+"
                    color = colorPlus
                case 10:
                    title = hk.keyName(num: 4 + tr)
                case 11:
                    title = hk.keyName(num: 6 + tr)
                case 12:
                    title = "-"
                    color = colorPlus
                default:
                    title = ""
                }
            }
            
            if pad.tag != 15 {
                pad.setTitle(title, for: .normal)
            }
            
            if ![4, 5, 6, 7, 15].contains(pad.tag) {
                pad.backgroundColor = color
            }
        }
    }
    
    
    
    // Левая октава,  кнопка #  и кнопка плюс или минус
    func leftLabelsRenamePlus() {
        var title:String
        var color = colorRegular
        let tr = sets.transpose
        label1.text = "On/Off Scale"
        label2.text = "Transpose"
        label3.text = "Keys Octave"
        label4.text = "Next Page"
        
        
        for pad in buttonsLeft {
            color = colorRegular
            
            if sets.setMode != 2 {
                switch pad.tag {
                case 8:
                    title = hk.keyName(num: -3 + tr)
                case 9:
                    title = hk.keyName(num: -1 + tr)
                case 10:
                    title = hk.keyName(num: 1 + tr)
                case 11:
                    title = "+"
                    color = colorPlus
                case 12:
                    title = hk.keyName(num: 4 + tr)
                case 13:
                    title = hk.keyName(num: 6 + tr)
                case 14:
                    title = "-"
                    color = colorPlus
                default:
                    title = ""
                }
            } else {
                switch pad.tag {
                case 13:
                    title = hk.keyName(num: -3 + tr)
                case 14:
                    title = hk.keyName(num: -1 + tr)
                case 8:
                    title = hk.keyName(num: 1 + tr)
                case 9:
                    title = "+"
                    color = colorPlus
                case 10:
                    title = hk.keyName(num: 4 + tr)
                case 11:
                    title = hk.keyName(num: 6 + tr)
                case 12:
                    title = "-"
                    color = colorPlus
                default:
                    title = ""
                }
            }
            
            if pad.tag != 15 {
                pad.setTitle(title, for: .normal)
            }
            
            if ![4, 5, 6, 7, 15].contains(pad.tag) {
                pad.backgroundColor = color
            }

        }
    }
    
 
    // Переименование правой октавы
    func rightOctaveRename() {
        for button in rightButtons {
            var text = scale.currentScale[button.tag]
            text = text[0 ..< text.count - 1]
            button.setTitle(text, for: .normal)
        }
    }
    
    
    
    
    
    
    // MARK: Actions
    
    @IBAction func leftBtnDown(_ sender: HRButton) {
        print(sender.tag)
        let note = midiIdentifier.defaultNoteNumbersForComboKeys[sender.tag]
        midiIn.receivedMIDINoteOn(noteNumber: note, velocity: 110, channel: 0)
    }
    
    
    @IBAction func leftBtnUp(_ sender: HRButton) {
        let note = midiIdentifier.defaultNoteNumbersForComboKeys[sender.tag]
        midiIn.receivedMIDINoteOff(noteNumber: note, velocity: 0, channel: 0)
    }
    
    
    
    @IBAction func rightBtnDown(_ sender: HRButton) {
        print(sender.tag)
        let note = UInt8(sender.tag + 60)
        midiIn.receivedMIDINoteOn(noteNumber: note, velocity: 110, channel: 0)
    }
    
    
    @IBAction func rightBtnUp(_ sender: HRButton) {
        let note = UInt8(sender.tag + 60)
        midiIn.receivedMIDINoteOff(noteNumber: note, velocity: 0, channel: 0)
    }
    
    
    
    @IBAction func panicBtn(_ sender: Any) {
        chorder.midiPanic()
    }
    
    
    // Можно добавить какой-нибудь интерактив при нажатии на экранчик
    @IBAction func displayBtn(_ sender: Any) {
    }
    
}
