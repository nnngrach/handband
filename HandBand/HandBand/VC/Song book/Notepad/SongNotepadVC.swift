// Блокнот, в который можно вставить скопированный текст песни из интернета,
// подредактировать его на свой вкус,
// и пропустить через систему распознавания аккордов,
// чтобы не заниматься оформлением песни с нуля.

import UIKit

class SongNotepadVC: UIViewController, UITextViewDelegate {
    @IBOutlet weak var songTextView: UITextView!
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var switcher: UISwitch!
    let sets = GlobalSettings.singleton
    let parser = HRSongTextParser.shared

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.songTextView.delegate = self
        switcher.isOn = !parser.isH
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(liftTextViewWithKeyboard(notification:)),
                                               name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(liftTextViewWithKeyboard(notification:)),
                                               name: UIResponder.keyboardWillHideNotification, object: nil)
        
//        NotificationCenter.default.addObserver(self,
//                                               selector: #selector(showAlert(notification:)),
//                                               name: NSNotification.Name(rawValue: "Song string is too long"), object: nil)
    }
    
    
    
    
    
    
    // MARK: - При переходе в режим редактирования
    func textViewDidBeginEditing(_ textView: UITextView) {
        songTextView.backgroundColor = .white
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        songTextView.backgroundColor = #colorLiteral(red: 0.8196078431, green: 0.8196078431, blue: 0.8196078431, alpha: 1)
    }
    
    // Скрывать клавиатуру
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        //self.view.endEditing(true)
        self.songTextView.resignFirstResponder()
    }
    
    
    
    // MARK: - Поднимать текстовое при появлении клавиатуры
    @objc func liftTextViewWithKeyboard(notification: Notification) {
        let userInfo = notification.userInfo
        let keyboardEndFrameScreenCoordinats = (userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let keyboardEndFrame = self.view.convert(keyboardEndFrameScreenCoordinats, to: view.window)
        
        if notification.name == UIResponder.keyboardWillHideNotification {
            songTextView.contentInset = UIEdgeInsets.zero
        }else{
            songTextView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardEndFrame.height, right: 0)
            songTextView.scrollIndicatorInsets = songTextView.contentInset
        }
        
        songTextView.scrollRangeToVisible(songTextView.selectedRange)
    }
    
    
// Если строки слишком длинные, то стоит вывести совет - разбить их пополам
//
//    @objc func showAlert(notification: Notification) {
//        let userInfo = notification.userInfo
//        let keyboardEndFrameScreenCoordinats = (userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
//        let keyboardEndFrame = self.view.convert(keyboardEndFrameScreenCoordinats, to: view.window)
//
//        if notification.name == Notification.Name.UIKeyboardWillHide {
//            songTextView.contentInset = UIEdgeInsets.zero
//        }else{
//            songTextView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardEndFrame.height, right: 0)
//            songTextView.scrollIndicatorInsets = songTextView.contentInset
//        }
//
//        songTextView.scrollRangeToVisible(songTextView.selectedRange)
//   }
    
    
    
    
    // MARK: - Вставить текст из буффера обмена
    func pasteText() -> String {
        let pasteBoard = UIPasteboard.general
        return pasteBoard.string!
    }
    
    
    // Запуск анализатора
    @IBAction func btn1Act(_ sender: Any) {
        // Добавить алерт с предупреждением, что существующий текст песни юудет перезаписан !!!!!
        parser.analyze(text: songTextView.text)

    }
    
    // Кнопка "Вставить"
    @IBAction func btn2Act(_ sender: Any) {
        songTextView.text = pasteText()
    }
    
    // Смена режима распознавания
    @IBAction func switchAct(_ sender: Any) {
        parser.isH = !switcher.isOn
    }
    
}
