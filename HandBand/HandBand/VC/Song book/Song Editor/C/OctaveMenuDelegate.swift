import UIKit

protocol OctaveMenuDelegate {
    func showMenu(for row: Int, to octave: Int)
}



// Show pop-up "Octave" menu when long-pressing to piano roll header

extension SongViewVC: OctaveMenuDelegate {
    
    
    
    func showMenu(for row: Int, to octaveRow: Int) {
        let midiHeaderWidth: CGFloat = 40
    
        let menuItem1: UIMenuItem = UIMenuItem(title: "Add octave up", action: #selector(addOctaveUp))
        let menuItem2: UIMenuItem = UIMenuItem(title: "Add octave down", action: #selector(addOctaveDown))
        let menuItem3: UIMenuItem = UIMenuItem(title: "Delete", action: #selector(deleteOctave))
        let menuItems: NSArray = [menuItem1, menuItem2, menuItem3]
        
        let cell = self.songTableView.cellForRow(at: IndexPath(row: row, section: 0))!
        octaveMenu.menuItems = menuItems as? [UIMenuItem]
        octaveMenu.arrowDirection = .up
        octaveMenu.setTargetRect(CGRect(x: 0, y: 0, width: midiHeaderWidth, height: cell.frame.height), in: cell)
        octaveMenu.setMenuVisible(true, animated: true)
        
        triggeredRow = row
        triggeredOctave = octaveRow
        
        let octaveLabel = files.loadedSong.songStrings[row].notes[octaveRow].label
        offsetFromOctave = pickerOctaveNamesArray.index(of: octaveLabel)!
    }
    
    
        override var canBecomeFirstResponder: Bool {
        return true
    }
    
    
    // Actions
    
    @objc func addOctaveUp() {
        var newOctaveNumber = offsetFromOctave + 1
        if newOctaveNumber > pickerOctaveNamesArray.count {
            newOctaveNumber = pickerOctaveNamesArray.count
        }
        let newOctaveLabel = pickerOctaveNamesArray[newOctaveNumber]
        
        files.loadedSong.songStrings[triggeredRow].notes.insert(OctaveRow(), at: triggeredOctave)
        files.loadedSong.songStrings[triggeredRow].notes[triggeredOctave].label = newOctaveLabel
        
        files.resaveSong()
        let index = IndexPath(row: triggeredRow, section: 0)
        songTableView.reloadRows(at:[index], with: .automatic)
    }
    
    
    @objc func addOctaveDown() {
        var newOctaveNumber = offsetFromOctave - 1
        if newOctaveNumber < 0 {
            newOctaveNumber = 0
        }
        let newOctaveLabel = pickerOctaveNamesArray[newOctaveNumber]
        
        files.loadedSong.songStrings[triggeredRow].notes.insert(OctaveRow(), at: triggeredOctave + 1)
        files.loadedSong.songStrings[triggeredRow].notes[triggeredOctave + 1].label = newOctaveLabel
        
        files.resaveSong()
        let index = IndexPath(row: triggeredRow, section: 0)
        songTableView.reloadRows(at:[index], with: .automatic)
    }
    
    @objc func deleteOctave() {
        files.loadedSong.songStrings[triggeredRow].notes.remove(at: triggeredOctave)
        files.resaveSong()
        let index = IndexPath(row: triggeredRow, section: 0)
        songTableView.reloadRows(at:[index], with: .automatic)
    }

    

}
