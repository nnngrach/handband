// Настройки всплывающего меню PickerView с выбором аккордов

import UIKit
extension SongViewVC: UIPickerViewDelegate, UIPickerViewDataSource {
    
    // Показать или скрыть меню выбора нот и аккордов
    func noteSelectPickerView(willShowing: Bool, animated: Bool) {
        let duration = (animated ? 0.4 : 0)
        chordPickerView.reloadAllComponents()
        
           // Развернуть пикер
        if willShowing && !sets.isChordPicking {
            loadPreSelectedPickerValues()
            sets.isChordPicking = true
            padSize = CGRect(x: 0,
                             y: self.view.bounds.height * 3 / 4,
                             width: self.view.bounds.width,
                             height: self.view.bounds.height / 4)
            
            UIView.animate(withDuration: duration) {
                self.outView.frame = self.padSize
            }
            
    
            
            // если меню уже на экране, то нужно просто его обновить
        } else if willShowing && sets.isChordPicking{
            chordPickerView.reloadAllComponents()
            
            
            
            // свернуть пикер
        } else {
            sets.isChordPicking = false
            padSize = CGRect(x: 0,
                             y: self.view.bounds.height,
                             width: self.view.bounds.width,
                             height: self.view.bounds.height / 4)
            
            UIView.animate(withDuration: duration) {
                self.outView.frame = self.padSize
            }
        }
        
    }
    
    
    
    // MARK: - Настройки пикера
    
    // sets.pickingNote[1] =  0 - текст, 1 - аккорды, 2 - ноты, 3 - подпись октавы
    // в режиме акктордов в пикере две крутилки (components)
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        if sets.pickedNote[1] == 1 {
            return 2
        } else {
            return 1
        }
    }
    
    
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        var count = 0
        
        switch sets.pickedNote[1] {
        case 1:
            if component == 0 {
                count = pickerLeftArray.count
                
            } else {
                switch pickerLeftRow {
                case 0:
                    count = 1
                case 13:
                    count = pickerPresetArray.count
                case 14:
                    count = pickerTransposeArray.count
                default:
                    count = pickerChordArray.count
                }
            }
            
        case 2:
            count =  pickerNotesArray.count
        case 3:
            count =  pickerOctaveNamesArray.count
        default:
            count = 0
        }
        
        return count
    }
    
    
    
    
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        var titleData = "- - -"
        
        switch sets.pickedNote[1] {
        case 1:
            if component == 0 {
                titleData = pickerLeftArray[row]
                
            } else {
                switch pickerLeftRow {
                case 0:
                    titleData = "- - -"
                case 13:
                    titleData = pickerPresetArray[row]
                case 14:
                    titleData = pickerTransposeArray[row]
                default:
                    titleData = pickerChordArray[row]
                }
            }
            
        case 2:
            titleData = pickerNotesArray[row]
        case 3:
            titleData = pickerOctaveNamesArray[row]
        default:
            break
        }
        
        
        let myTitle = NSAttributedString(string: titleData, attributes: [NSAttributedString.Key.font:UIFont(name: "Georgia", size: 15.0)!,NSAttributedString.Key.foregroundColor:UIColor.white])
        return myTitle
    }
    
    

    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        switch sets.pickedNote[1] {
        case 1:
            
            if component == 0 {
                pickerLeftRow = row
                chordTextL = pickerLeftArray[row]
                
                switch row {
                case 14:
                    // сделать чтобы по умолчанию выдиралась 12 строка с значением 0
                    pickerType = 3
                    chordPickerView.selectRow(0, inComponent: 1, animated: true)
                    chordPickerView.reloadAllComponents()
                    chordTextR = pickerTransposeArray[0]
                case 13:
                    pickerType = 2
                    chordPickerView.selectRow(0, inComponent: 1, animated: true)
                    chordTextR = pickerPresetArray[0]
                    //            case 3, 10:
                    //                pickerType = 1
                    //                pickerChordTonality = row
                    //                chordPickerView.selectRow(1, inComponent: 1, animated: true)
                //                chordTextR = pickerChordArray[1]
                case 0:
                    pickerType = 0
                    chordTextL = "- - -"
                    chordTextR = "- - -"
                default:
                    pickerType = 1
                    pickerChordTonality = row - 1
                    chordPickerView.selectRow(0, inComponent: 1, animated: true)
                    chordTextR = pickerChordArray[0]
                }
                
                pickerView.reloadComponent(1)
                
                
                
            } else {
                switch pickerLeftRow {
                case 0:
                    chordTextR = "- - -"
                case 13:
                    pickerPreset = row
                    chordTextR = pickerPresetArray[row]
                case 14:
                    pickerTranspose = row
                    chordTextR = pickerTransposeArray[row]
                default:
                    pickerChordType = row
                    chordTextR = pickerChordArray[row]
                }
            }
            
    
            
        case 2:
             pickerNoteLabel = pickerNotesArray[row]
        case 3:
             pickerOctaveLabel = pickerOctaveNamesArray[row]
        default:
            break
        }
    }
    
    
    
    
    
    func loadPreSelectedPickerValues() {
        switch sets.pickedNote[1] {
        case 1:
            break
        case 2:
            let data = sets.pickedNote
            let noteLabel = files.loadedSong.songStrings[data[0]].notes[data[2]].notes[data[3]].label
            if let noteNumber = pickerNotesArray.index(of: noteLabel) {
                chordPickerView.selectRow(noteNumber, inComponent: 0, animated: true)
                pickerNoteLabel = noteLabel
            }

            
        case 3:
            let data = sets.pickedNote
            let octaveLabel = files.loadedSong.songStrings[data[0]].notes[data[2]].label
            if let octaveNumber = pickerOctaveNamesArray.index(of: octaveLabel) {
                pickerOctaveLabel = octaveLabel
                chordPickerView.selectRow(octaveNumber, inComponent: 0, animated: true)
            }
            
        default:
            break
        }
    }
    
}
