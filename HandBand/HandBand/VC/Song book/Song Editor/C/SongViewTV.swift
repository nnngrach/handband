import UIKit
extension SongViewVC: UITableViewDelegate, UITableViewDataSource  {
    

    // MARK: - Настройки таблицы
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return files.loadedSong.songStrings.count
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = songTableView.dequeueReusableCell(withIdentifier: "Song View Cell") as! SongViewCell

        cell.currentRow = indexPath.row
        cell.displaySelectedMode()
        cell.enableEditing()
        cell.songTextField.text = files.loadedSong.songStrings[indexPath.row].text
        cell.addNibChords()
        cell.midiTableViewSetup(row: indexPath.row)

        cell.addShortPressGesture()
        cell.addLongPressGesture()
        
        cell.delegate = self
        
        //cell.rowData = cell.adapter.adapterStoredNotesToPianoRoll(atRow: indexPath.row)
        //cell.addObservers()
        return cell
    }
    
    
    // Высота строк для строк с текстом, аккордами, нотам
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let splitterLineHeight = 2
        
        switch files.loadedSong.songStrings[indexPath.row].mode {
        case 0:
            return 40
        case 1:
            return 60
        case 2:
            return CGFloat(files.loadedSong.songStrings[indexPath.row].notes.count * 60 + splitterLineHeight)
            //return 120
        default:
            return 40
        }
    }
    
    
    // разрешает перетаскивать ячейки
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    
    // действия при перетаскивании
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let deletedRow = files.loadedSong.songStrings.remove(at: sourceIndexPath.row)
        files.loadedSong.songStrings.insert(deletedRow, at: destinationIndexPath.row)
        files.resaveSong()
    }
    
    
    
    // удаляет красные кружочки слева (в режиме редактирования)
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }
    
    
    // удаляет отступы слева (в режиме редактирования)
    func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    
    
    
    
    
    // MARK: - Меню свайпов ячейки
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let deleteAction = UITableViewRowAction(style: .normal, title: "Delete") { (action:UITableViewRowAction, indexPath:IndexPath) in
            self.deleteCell(at: indexPath.row)
        }
        
        let changeMode = UITableViewRowAction(style: .normal, title: "Mode") { (action:UITableViewRowAction, indexPath:IndexPath) in
            self.changeMode(at: indexPath.row)
        }
        
        let pasteAction = UITableViewRowAction(style: .normal, title: "Paste") { (action:UITableViewRowAction, indexPath:IndexPath) in
            self.pasteCell(at: indexPath.row)
        }
        
        let copyAction = UITableViewRowAction(style: .normal, title: "Copy") { (action:UITableViewRowAction, indexPath:IndexPath) in
            self.copyCell(at: indexPath.row)
        }
        
        
        let addAction = UITableViewRowAction(style: .normal, title: "Add") { (action:UITableViewRowAction, indexPath:IndexPath) in
            self.addCell(at: indexPath.row)
        }
        
        addAction.backgroundColor = #colorLiteral(red: 0.5411764706, green: 0.6705882353, blue: 0.5529411765, alpha: 1)
        copyAction.backgroundColor = #colorLiteral(red: 0.4004528569, green: 0.5802476264, blue: 0.6941176471, alpha: 1)
        pasteAction.backgroundColor = #colorLiteral(red: 0.2705882353, green: 0.3607843137, blue: 0.4823529412, alpha: 1)
        changeMode.backgroundColor = #colorLiteral(red: 0.4, green: 0.3568627451, blue: 0.462745098, alpha: 1)
        deleteAction.backgroundColor = #colorLiteral(red: 0.5450980392, green: 0.2941176471, blue: 0.3882352941, alpha: 1)
        
        if sets.isSongEditing && !sets.isChordPicking {
            return [deleteAction, changeMode, pasteAction, copyAction, addAction]
        } else {
            return []
        }
    }
    

    
    
    
    
    
    // MARK: - Действия по свайпу
    
    // Добавить ячейку
    func addCell(at index: Int) {
        files.loadedSong.songStrings.insert(SongString(), at: index + 1)
        files.resaveSong()
        let indexPath = IndexPath(item: index + 1, section: 0)
        songTableView.insertRows(at: [indexPath], with: .automatic)
    }
    
    // Скопировать ячейку
    func copyCell(at index: Int) {
        copiedCell = files.loadedSong.songStrings[index]
    }
    
    // Вставить ячейку
    func pasteCell(at index: Int) {
        files.loadedSong.songStrings.insert(copiedCell, at: index + 1)
        files.resaveSong()
        let indexPath = IndexPath(item: index + 1, section: 0)
        songTableView.insertRows(at: [indexPath], with: .automatic)
    }
    
    // Удалить ячейку
    func deleteCell(at index: Int) {
        files.loadedSong.songStrings.remove(at: index)
        files.resaveSong()
        let indexPath = IndexPath(item: index, section: 0)
        songTableView.deleteRows(at: [indexPath], with: .automatic)
    }
    
    
    // Сменить стиль ячейки - текст -> аккорды -> ноты
    func changeMode(at index: Int) {
        let mode = files.loadedSong.songStrings[index].mode
        files.loadedSong.songStrings[index].mode  =  mode + 1 <= 2  ? mode + 1  :  0
        files.resaveSong()
        
        let indexPath = IndexPath(item: index, section: 0)
        songTableView.reloadRows(at: [indexPath], with: .middle)
    }
    
    
    
    
    
    // Чтобы при выделении новой ноты в MidiTableView сборосить выделение для предыдущей
    func reloadRowsWithoutSelectedNote() {
        for i in 0 ..< files.loadedSong.songStrings.count {
            
            // нужно сделать обновление всех строк песенника TableView кроме выделенной
            if i != sets.pickedNote[0] {
                let index = IndexPath(row: i, section: 0)
                songTableView.reloadRows(at: [index], with: .none)
            }
        }
    }

}
