// Редактор песен - основное окно (Не доделан)

import UIKit

class SongViewVC: UIViewController {
    @IBOutlet weak var songTableView: UITableView!
    @IBOutlet weak var littlePadsContainer: UIView!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet var outView: UIView!
    @IBOutlet var outKeysView: UIView!
    @IBOutlet weak var chordPickerView: UIPickerView!
    @IBOutlet var pianoKeys: [HRButton]!
    
    
    let midiIn = AudioInOut.singleton
    let sets = GlobalSettings.singleton
    let disk = HRFiles.shared
    let chorder = NoteGenerator.singleton
    let files = HRFileOperations.shared
    let scale = ScaleTransformer.singleton
    
    
    var isLoaded = false
    var isNibLoaded = false
    var padSize = CGRect()
    var littlePadsIsShoving = false
    var copiedCell = SongString()
    var keysMenuShowed = false

    
    var octaveMenu = UIMenuController.shared
    var triggeredRow = 0
    var triggeredOctave = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        songTableView.dataSource = self
        songTableView.delegate = self
        self.navigationItem.title = files.loadedSong.name
        
        editingModeSetup()
        chordMenuSetup()
        keysMenuSetup()
        keysRename()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(reloadUnselectedRows(n:)),
                                               name: NSNotification.Name.self.init("Reload strings without selected note"), object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(showNoteOrChordSelectorMenu(n:)),
                                               name: NSNotification.Name.self.init("Show note or chord selector menu"), object: nil)
        
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(selectChord(n:)),
                                               name: NSNotification.Name.self.init("Select Chord Button"), object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(updateTable(n:)),
                                               name: NSNotification.Name.self.init("Song Text Updated"), object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(nextPage(n:)),
                                               name: NSNotification.Name.self.init("Song string changed"), object: nil)
        NotificationCenter.default.addObserver(forName:NSNotification.Name(rawValue: "screenScaleUpdate"), object:nil, queue:nil, using:screenScaleUpdate)
        
        NotificationCenter.default.addObserver(forName:NSNotification.Name(rawValue: "smartScalesChanged"), object:nil, queue:nil, using:screenScaleUpdate)
    }

    
    override func viewDidDisappear(_ animated: Bool) {
        isLoaded = false
    }
    
    // Скрывать клавиатуру
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.view.endEditing(true)
    }
    


    
    
    @objc func updateTable(n: NSNotification) {
        songTableView.reloadData()
    }
    
    
    
    @objc func reloadUnselectedRows(n: NSNotification) {
        reloadRowsWithoutSelectedNote()
    }
    
    
    @objc func screenScaleUpdate(notification:Notification) -> Void {
        DispatchQueue.main.async(execute: {
            self.keysRename()
        })
    }
    
    // Переименование названий нот на клавишах
    func keysRename() {
        for button in pianoKeys {
            var text = scale.currentScale[button.tag]
            text = text[0 ..< text.count - 1]
            button.setTitle(text, for: .normal)
        }
    }
    
    
    // Перемотка текста
    @objc func nextPage(n: NSNotification) {
        let indexPath = IndexPath(item: sets.songString, section: 0)
        DispatchQueue.main.async(execute: {
            self.songTableView.scrollToRow(at: indexPath, at: .top, animated: true)
        })
    }
    
    
    
    
    
    // Перезапуск midi движка
    @IBAction func midiPanic(_ sender: Any) {
        chorder.midiPanic()
        
//        let menu = UIMenuController.shared
//        let item1 = UIMenuItem(title: "MyItem1", action: #selector(testFunc(_:)))
//        let item2 = UIMenuItem(title: "MyItem2", action: #selector(testFunc(_:)))
//        let items = [item1, item2]
//        menu.menuItems = items
//        menu.arrowDirection = UIMenuControllerArrowDirection.up
//        menu .setTargetRect(self.view.frame, in: self.view)
//
//        menu.setMenuVisible(true, animated: true)
    }
    
    
    
    
    
    
    
    
    // MARK: - Переход в режим редактирования
    @IBAction func editBtn(_ sender: Any) {
        sets.isSongEditing = !sets.isSongEditing
        songTableView.reloadData()
        // print("Editing ",sets.isSongEditing)
        // print("Chord picking  ",sets.isChordPicking)
        
        
        if sets.isSongEditing {
            //installLongPressGesture()
            editButton.backgroundColor = #colorLiteral(red: 0.5921568627, green: 0.3294117647, blue: 0.3254901961, alpha: 1)

            
        } else {

            songTableView.isEditing = false
            editButton.backgroundColor = #colorLiteral(red: 0.3333064318, green: 0.3333490491, blue: 0.333286792, alpha: 1)
            //removeLongPressGeture()

            
            if sets.isChordPicking {
                //changePickerSize(withDuration: 0.3)
                noteSelectPickerView(willShowing: true, animated: true)
            }
        }
        
        
        if !littlePadsIsShoving {
            changePadSise(withAnimationTime: 0.3)
        }
    }
    
    

    func editingModeSetup() {
        sets.isSongEditing = false
        sets.isSongStringMoving = false
        changePadSise(withAnimationTime: 0)
    }
    
    
    
    
    
    // MARK: - Долгое нажатие для перемещения ячеек

//    func installLongPressGesture() {
//        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(sender:)))
//        songTableView.addGestureRecognizer(longPress)
//        songTableView.allowsSelection = false
//    }
//
//    func removeLongPressGeture() {
//        for gesture in songTableView.gestureRecognizers! {
//            if let recognizer = gesture as? UILongPressGestureRecognizer {
//                songTableView.removeGestureRecognizer(recognizer)
//            }
//        }
//    }
//
//    @objc func handleLongPress(sender: UILongPressGestureRecognizer) {
//        if sender.state == .began  &&  sets.isSongEditing {
//            songTableView.isEditing = !songTableView.isEditing
//            sets.isSongStringMoving = !sets.isSongStringMoving
//            songTableView.reloadData()
//        }
//    }

    
    
    
    
    
    
    // MARK: - Скрыть или показать панель с клавишами
    
    @IBAction func buttonsHidingKeyboard(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    
    @IBAction func showKeys(_ sender: Any) {
        if !littlePadsIsShoving  {
            changePadSise(withAnimationTime: 0.5)
        }
        if !sets.isChordPicking {
            changeKeysSize(withDuration: 0.5)
        }
    }
    
    
    func keysMenuSetup() {
        keysMenuShowed = true
        view.addSubview(outKeysView)
        outKeysView.translatesAutoresizingMaskIntoConstraints = false
        changeKeysSize(withDuration: 0)
    }
    
    
    func changeKeysSize(withDuration: Double) {
        keysMenuShowed = !keysMenuShowed
        
        if keysMenuShowed {
            padSize = CGRect(x: 0,
                             y: self.view.bounds.height * 3 / 4,
                             width: self.view.bounds.width,
                             height: self.view.bounds.height / 4)
            
        } else {
            padSize = CGRect(x: 0,
                             y: self.view.bounds.height,
                             width: self.view.bounds.width,
                             height: self.view.bounds.height / 4)
        }
        
        
        UIView.animate(withDuration: withDuration) {
            self.outKeysView.frame = self.padSize
        }
    }
    
    
    
    
    
    // MARK: - Скрыть или показать панель с пэдами
    
    @IBAction func showPads(_ sender: Any) {
        if keysMenuShowed {
            changeKeysSize(withDuration: 0.5)
        }
        if !sets.isChordPicking {
            changePadSise(withAnimationTime: 0.5)
        }
    }
    
   
    func changePadSise(withAnimationTime:Double) {
        if littlePadsIsShoving {
            padSize = CGRect(x: 0,
                             y: self.view.bounds.height * 3 / 4,
                             width: self.view.bounds.width,
                             height: self.view.bounds.height / 4)
            
        } else {
            padSize = CGRect(x: 0,
                             y: self.view.bounds.height,
                             width: self.view.bounds.width,
                             height: self.view.bounds.height / 4)
        }
        
        UIView.animate(withDuration: withAnimationTime) {
            self.littlePadsContainer.frame = self.padSize
        }
        
        littlePadsIsShoving = !littlePadsIsShoving
    }
    
    
    
    
    
   
    
    
    
    
    
    
    
    // Начать разворачивание окна для выбора нот и аккордов
    @objc func showNoteOrChordSelectorMenu(n: NSNotification) {
        noteSelectPickerView(willShowing: true, animated: true)
        //print("Show note or chord selector menu")
        
//        switch sets.pickingNote[1] {
//        case 1:
//            print("Режим аккордов")
//        case 2:
//            print("Режим нот")
//
//        default:
//            break
//        }
    
    }

    
    
   

    
    
    // MARK: - Скрыть или показать меню выбора аккордов
    
    var pickerType = 0
    var pickerChordTonality = 0
    var pickerChordType = 0
    var pickerPreset = 0
    var pickerTranspose = 0
    var pickerLeftRow = 0
    var pickerNote = 0
    var pickerLabel = ""
    var pickerNoteLabel = ""
    var pickerOctaveLabel = ""
    var chordTextL = "- - -"
    var chordTextR = "- - -"
    
    let pickerNotesArray =            ["C", "C#", "D", "D#", "E", "F", "F#",
                                 "G", "G#", "A", "A#", "B"]
    let pickerLeftArray =       ["- - -", "C", "C#", "D", "D#", "E", "F", "F#",
                                 "G", "G#", "A", "A#", "B", "Preset ", "Transp "]
    let pickerChordArray =      ["", "m", "7", "m7", "sus2", "sus4", "6", "m6", "dim7", "aug", "M7", ]
    let pickerPresetArray =     ["1", "2", "3", "4", "5", "6", "7", "8"]
    let pickerTransposeArray = ["-12", "-11", "-10", "-9", "-8", "-7",
                                "-6", "-5", "-4", "-3", "-2", "-1", "0",
                                "+1", "+2", "+3", "+4", "+5", "+6",
                                "+7", "+8", "+9", "+10", "+11", "+12", ]
    let pickerOctaveNamesArray = ["I", "II", "III", "IV", "V", "VI", "VII"]
    
    
    // Действие при нажатии на миниатюру аккорда
    @objc func selectChord(n: NSNotification) {
//        songTableView.reloadData()
        
        noteSelectPickerView(willShowing: true, animated: true)
        //print("Select Chord")
    }
    
    func chordMenuSetup() {
        // print ("Chord Menu Setup ")
        sets.isChordPicking = true
        view.addSubview(outView)
        outView.translatesAutoresizingMaskIntoConstraints = false
        noteSelectPickerView(willShowing: false, animated: false)
    }
    

    
    @IBAction func pickerDoneBtn(_ sender: Any) {
        //changePickerSize(withDuration: 0.4)
        noteSelectPickerView(willShowing: false, animated: true)
        let displaingMode = 1
        let chordMode = 1
        let noteMode = 2
        let octaveMode = 3
        
        if sets.pickedNote[displaingMode] == chordMode {
            let chord = sets.pickingChord
            files.loadedSong.songStrings[chord[0]].chords[chord[1]].label = chordTextL + chordTextR
            files.loadedSong.songStrings[chord[0]].chords[chord[1]].type = pickerType
            files.loadedSong.songStrings[chord[0]].chords[chord[1]].chordTonality = pickerChordTonality
            files.loadedSong.songStrings[chord[0]].chords[chord[1]].chordType = pickerChordType
            files.loadedSong.songStrings[chord[0]].chords[chord[1]].preset = pickerPreset
            files.loadedSong.songStrings[chord[0]].chords[chord[1]].transpose = pickerTranspose - 12
            
            files.resaveSong()
            let index = IndexPath(item: chord[0], section: 0)
            songTableView.reloadRows(at: [index], with: .automatic)
            
            
        } else if sets.pickedNote[1] == noteMode {
            let data = sets.pickedNote
            files.loadedSong.songStrings[data[0]].notes[data[2]].notes[data[3]].label = pickerNoteLabel
            
            files.resaveSong()
            let index = IndexPath(item: data[0], section: 0)
            songTableView.reloadRows(at: [index], with: .automatic)
            
            
        } else if sets.pickedNote[1] == octaveMode {
            let data = sets.pickedNote
            files.loadedSong.songStrings[data[0]].notes[data[2]].label = pickerOctaveLabel

            files.resaveSong()
            let index = IndexPath(item: data[0], section: 0)
            songTableView.reloadRows(at: [index], with: .automatic)
        }
    }
    
    
    var offsetFromOctave = 0
    
    
    
    // MARK: -  Воспроизведение нот
    
    @IBAction func pianoKeyDown(_ sender: HRButton) {
        let note = UInt8(sender.tag + 60)
        midiIn.receivedMIDINoteOn(noteNumber: note, velocity: 110, channel: 0)
    }
    
    @IBAction func pianoKeyUp(_ sender: HRButton) {
        let note = UInt8(sender.tag + 60)
        midiIn.receivedMIDINoteOff(noteNumber: note, velocity: 0, channel: 0)
    }
    
    
    @IBAction func showNoteOctavePicker(_ sender: Any) {
        if sets.isSongEditing {
            print("showNoteOctavePicker")
        }
    }
    
    
}
