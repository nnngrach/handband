// Всплывающее окно с пэдами для игры

import UIKit

class LittlePadsVC: UIViewController {
    @IBOutlet var pads: [HRButton]!
    let sets = GlobalSettings.singleton
    let midiIn = AudioInOut.singleton
    let midiIdentifier = DetecterComboKeyNumbers.singleton
    
    let hk = HotKeyController.singleton
    var notesForButton = [[UInt8]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadNotesToPlay()
    }

    @IBAction func padsDown(_ sender: HRButton) {
        onAllNotes(in: notesForButton[sender.tag] )
    }
    
    @IBAction func padsUp(_ sender: HRButton) {
        offAllNotes(in: notesForButton[sender.tag] )
    }
    

   
    
    func onAllNotes (in array:[UInt8]) {
        for note in array {
            midiIn.receivedMIDINoteOn(noteNumber: note, velocity: 110, channel: 0)
        }
    }
    
    
    
    func offAllNotes (in array:[UInt8]) {
        for note in array {
            midiIn.receivedMIDINoteOff(noteNumber: note, velocity: 0, channel: 0)
        }
    }
    

    func loadNotesToPlay() {
        let note = midiIdentifier.customNoteNumbersForComboKeys
        notesForButton = [ [note[0]],
                           [note[1]],
                           [note[2]],
                           [note[3]],
                           [note[4]],
                           [note[5]],
                           [note[6]],
                           [note[7]] ]
    }
}
