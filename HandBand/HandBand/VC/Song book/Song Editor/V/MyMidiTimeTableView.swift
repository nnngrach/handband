// Сделать наследование от MIDITimeTableView чтобы переопределить стандартные методы

import UIKit

class MyMidiTimeTableView: MIDITimeTableView {
    private let sets = GlobalSettings.singleton
    private let files = HRFileOperations.shared
    var currentRow = 0
    let rowHeight: CGFloat = 60
    
    
    
    // Переопределить действие при нажатие на ноту
    override func midiTimeTableCellViewDidTap(_ midiTimeTableCellView: MIDITimeTableCellView) {
        
        if sets.isSongEditing && !sets.isChordPicking {
            for cell in cellViews.flatMap({ $0 }) {
                cell.isSelected = cell == midiTimeTableCellView
                
                // При выделении ячейки сохранить ее координаты в формате
                // Страка в песеннике - строка в MidiTableView - номер ноты в MidiTableView
                
                // Адрес ноты
                if cell.isSelected {
                    sets.pickedNote[0] = currentRow
                    sets.pickedNote[1] = files.loadedSong.songStrings[currentRow].mode
                    sets.pickedNote[2] = (cellIndex(of: cell)?.row)!
                    sets.pickedNote[3] = (cellIndex(of: cell)?.index)!
                    //print(sets.pickingNote)
                }
            }
            
            NotificationCenter.default.post(name: NSNotification.Name.init("Reload strings without selected note"),
                                            object: nil)
            NotificationCenter.default.post(name: NSNotification.Name.init("Show note or chord selector menu"),
                                            object: nil)
        }
    }
    
    
    
    
    // Разрешить перетаскивание и изменение длительности ноты пока только в режиме редактирования
    
    override func midiTimeTableCellViewDidMove(_ midiTimeTableCellView: MIDITimeTableCellView, pan: UIPanGestureRecognizer) {
        if sets.isSongEditing && !sets.isChordPicking {
             super.midiTimeTableCellViewDidMove(midiTimeTableCellView, pan: pan)
        }
//        if sets.isSongEditing {
//            super.midiTimeTableCellViewDidMove(midiTimeTableCellView, pan: pan)
//        }
    }
    
    
    override func midiTimeTableCellViewDidResize(_ midiTimeTableCellView: MIDITimeTableCellView, pan: UIPanGestureRecognizer) {
        if sets.isSongEditing && !sets.isChordPicking {
            super.midiTimeTableCellViewDidResize(midiTimeTableCellView, pan: pan)
        }
//        if sets.isSongEditing  {
//            super.midiTimeTableCellViewDidResize(midiTimeTableCellView, pan: pan)
//        }
    }
    
    
}
