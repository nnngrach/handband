// Строка песенника
import UIKit

class SongViewCell: UITableViewCell {
    
    @IBOutlet weak var viewText: UIView!
    @IBOutlet weak var viewChordes: UIView!
    @IBOutlet weak var viewNotes: UIView!
    @IBOutlet weak var timeTableView: MyMidiTimeTableView!
    @IBOutlet weak var hamburger: UIButton!
    @IBOutlet weak var songTextField: UITextField!
    @IBOutlet var chordCells: [UIView]!
    @IBOutlet var chordImages: [UIImageView]!
    @IBOutlet weak var underHamburgerView: UIView!
    
    @IBOutlet weak var testNotes: UIView!
    @IBOutlet weak var gestureNoteView: UIView!
    
    
    let sets = GlobalSettings.singleton
    let files = HRFileOperations.shared
    let chorder = NoteGenerator.singleton
    let k = HRKeyEquality.singleton
    let adapter = PianoRollFormatAdapter()
    var currentRow = 0
    var timeTableHeaderWidth: CGFloat = 40
    var rowData: [MIDITimeTableRowData] = []

    
    
    
    
    
   
    // MARK: Смена отображаемых слоев/режимов в ячейке
    
    func hideAll() {
        viewText.isHidden = true
        viewChordes.isHidden = true
        viewNotes.isHidden = true
    }
    
    
    func enableEditing() {
        songTextField.isEnabled = sets.isSongEditing
        // // // // hamburger.isHidden = !sets.isSongEditing || sets.isSongStringMoving
        
        if sets.isSongEditing {
            viewChordes.backgroundColor = #colorLiteral(red: 0.6666166186, green: 0.6666959524, blue: 0.6665801406, alpha: 1)
            viewText.backgroundColor = #colorLiteral(red: 0.6666166186, green: 0.6666959524, blue: 0.6665801406, alpha: 1)
            
        } else {
            viewChordes.backgroundColor = .white
            viewText.backgroundColor = .white
        }
    }
    
    
    

    // Сменить стиль ячеки: текст -> аккорды -> ноты
    func displaySelectedMode() {
        switch files.loadedSong.songStrings[currentRow].mode {
        case 1:
            hideAll()
            viewChordes.isHidden = false
        case 2:
            hideAll()
            viewNotes.isHidden = false
        default:
            hideAll()
            viewText.isHidden = false
        }
    }
    
    
   

    
    // Обработка текстового поля
    @IBAction func songTextFieldAct(_ sender: UITextField) {
        if let text = songTextField.text {
            files.loadedSong.songStrings[currentRow].text = text
            files.resaveSong()
        }
    }
    
    
    
    
    
    // Всплывающее меню при долгом нажатии на название октавы в пианоролле
    
    var delegate: OctaveMenuDelegate?
    
    
    func addLongPressGesture(){
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(sender:)))
        gestureNoteView.addGestureRecognizer(longPress)
    }
    
    
    
    @objc func handleLongPress(sender: UILongPressGestureRecognizer) {
            if sender.state == .began  &&  sets.isSongEditing {
                
                let touchCoordinate = sender.location(in: self.timeTableView)
                let octaveCount = CGFloat(files.loadedSong.songStrings[currentRow].notes.count)
                let selectedOctave = Int(( touchCoordinate.y / self.frame.height) * octaveCount)
              
                delegate?.showMenu(for: currentRow, to: selectedOctave)
            }
    }
    
    
    
    // Показать PickerView с выбором октавы при коротком нажатии на название октавы в пианоролле
    
    func addShortPressGesture(){
        let shortPress = UITapGestureRecognizer(target: self, action: #selector(handleShortPress(sender:)))
        gestureNoteView.addGestureRecognizer(shortPress)
    }
    
    
    
    @objc func handleShortPress(sender: UITapGestureRecognizer) {
        if sets.isSongEditing {
            
            let touchCoordinate = sender.location(in: self.timeTableView)
            let octaveCount = CGFloat(files.loadedSong.songStrings[currentRow].notes.count)
            let selectedOctave = Int(( touchCoordinate.y / self.frame.height) * octaveCount)

            sets.pickedNote[0] = currentRow
            sets.pickedNote[1] = 3
            sets.pickedNote[2] = selectedOctave
            
            NotificationCenter.default.post(name: NSNotification.Name.init("Show note or chord selector menu"), object: nil)
        }
    }

   
    
   
    
    
    // MARK: Загрузить миниатюру аккорда
    
    func addNibChords() {
        
        for chord in chordImages {
            let chordData = files.loadedSong.songStrings[currentRow].chords[chord.tag]
           
            if chordData.label != "" || chordData.label != "- - -- - -" {
                let name = sets.modeLetter + chordData.label + ".png"
                chord.image = UIImage(named: name)
            }
        }
    }
    
    
    
    
    // MARK: - При нажатии на миниатюру аккорда
    @IBAction func chordButtonsDown(_ sender: UIButton) {

        // В обычном режиме
        if !sets.isSongEditing {
            // chorder.playScreenNotesOn(row: currentRow, chordInRow: sender.tag)
            DispatchQueue.main.async {
                self.chorder.playScreenNotesOn(row: self.currentRow, chordInRow: sender.tag)
            }
            
        } else {
            // а в режиме редактирования открыть меню с настройками отображаемого аккорда.
            if !sets.isChordPicking {
                sets.pickingChord = [currentRow, sender.tag]
                NotificationCenter.default.post(name: NSNotification.Name.init("Select Chord Button"),  object: nil)
            }
        }
    }
    
    
    

    
    // При отпускании кнопки прерываем звучание нот
    @IBAction func chordButtons(_ sender: UIButton) {
        if !sets.isSongEditing {
            // chorder.playScreenNotesOff(row: currentRow, chordInRow: sender.tag)

            DispatchQueue.main.async {
                self.chorder.playScreenNotesOff(row: self.currentRow, chordInRow: sender.tag)
            }
        }
    }
}
