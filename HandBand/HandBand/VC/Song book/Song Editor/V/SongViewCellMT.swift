// Настройки фреймворка MIDITimeTableView для отображения окошка перетаскиваемыми нотами

import UIKit

extension SongViewCell: MIDITimeTableViewDataSource, MIDITimeTableViewDelegate {
    
    // Настройка отображения в режиме Piano Roll
    
    func midiTableViewSetup(row: Int) {
        rowData = adapter.convertStoringToDisplayingNoteFormat(atRow: row)
        
        timeTableView?.dataSource = self
        timeTableView?.timeTableDelegate = self
        timeTableView.currentRow = row
        
        timeTableView?.showsMeasure = false
        // timeTableView?.showsHeaders = false
        timeTableView?.holdsHistory = false
        timeTableView?.gridLayer.showsSubbeatLines = false
        timeTableView?.reloadData()

        timeTableView?.backgroundColor = UIColor(red: 18.0/255.0, green: 20.0/255.0, blue: 19.0/255.0, alpha: 1)
        timeTableView?.measureView.backgroundColor = UIColor(red: 26.0/255.0, green: 28.0/255.0, blue: 27.0/255.0, alpha: 1)
        timeTableView?.measureView.tintColor = UIColor(red: 119.0/255.0, green: 121.0/255.0, blue: 120.0/255.0, alpha: 1)
        timeTableView?.gridLayer.rowLineColor = .black
        timeTableView?.gridLayer.barLineColor = UIColor(red: 42.0/255.0, green: 42.0/255.0, blue: 42.0/255.0, alpha: 1)
        timeTableView?.gridLayer.beatLineColor = UIColor(red: 42.0/255.0, green: 42.0/255.0, blue: 42.0/255.0, alpha: 1)
        timeTableView?.playheadView.tintColor = UIColor.gray.withAlphaComponent(0.5)
        timeTableView?.rangeheadView.tintColor = UIColor.gray.withAlphaComponent(0.3)
       
    
        
        // Чтобы в окне показывалось только два такта. И был отключен зум
        let barWidth = (UIScreen.main.bounds.width - 2 * timeTableHeaderWidth) / 2
        timeTableView?.minMeasureWidth = barWidth
        timeTableView?.maxMeasureWidth = barWidth
        timeTableView?.measureWidth = barWidth
        
        //timeTableView.contentSize = CGSize(width: 100, height: 60)
        

        // Двойной щелчок. чтобы добавить новую ноту
        let doubleTap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        doubleTap.numberOfTapsRequired = 2
        timeTableView?.addGestureRecognizer(doubleTap)
        timeTableView?.isUserInteractionEnabled = true
    }
    
    
    

    // Добавить новую ноту
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        let touchCoordinate = sender.location(in: self.timeTableView)
        let numberOfRows = CGFloat(files.loadedSong.songStrings[currentRow].notes.count)
        let clckedAtRowNumber = Int(touchCoordinate.y / (timeTableView.frame.height / numberOfRows) )

        let numberOfBars: CGFloat = 8
        let screenPosition = Double(( (touchCoordinate.x - timeTableHeaderWidth) / (timeTableView.frame.width - timeTableHeaderWidth)) * numberOfBars)
       
        
        var noteOnFreePlace = true
        
        if sets.isSongEditing {
            
            // проверить не пересекаются ли координаты двойного тапа с имеющимися нотами
            for i in 0 ..< rowData[clckedAtRowNumber].cells.count {
                let positon = rowData[clckedAtRowNumber].cells[i].position
                let duration = rowData[clckedAtRowNumber].cells[i].duration
                
                if screenPosition >= positon  &&  screenPosition <= (positon + duration) {
                    noteOnFreePlace = false
                    break
                }
            }
            
            // если нет - то можно добавить новую ноту
            if noteOnFreePlace {
                rowData.appendCell(MIDITimeTableCellData(data: "A", position: screenPosition, duration: 0.5), row: clckedAtRowNumber)
                
                timeTableView.reloadData()
                saveNoteData()
            }
        }
    }
    
    
    
    func saveNoteData() {
        adapter.convertDisplayingToStoringNoteFormat(atRow: currentRow, pianoRollData: rowData)
        files.resaveSong()
    }
    
    
 
    
    
    
    // MARK: MIDITimeTableViewDataSource
    
    func numberOfRows(in midiTimeTableView: MIDITimeTableView) -> Int {
        return rowData.count
    }
    
    func timeSignature(of midiTimeTableView: MIDITimeTableView) -> MIDITimeTableTimeSignature {
        return MIDITimeTableTimeSignature(beats: 4, noteValue: .quarter)
    }
    
    func midiTimeTableView(_ midiTimeTableView: MIDITimeTableView, rowAt index: Int) -> MIDITimeTableRowData {
        let row = rowData[index]
        return row
    }
    
    
    
    
    
    // MARK: MIDITimeTableViewDelegate
    
    func midiTimeTableViewHeightForRows(_ midiTimeTableView: MIDITimeTableView) -> CGFloat {
        return 60
    }
    
    func midiTimeTableViewHeightForMeasureView(_ midiTimeTableView: MIDITimeTableView) -> CGFloat {
        return 20
    }
    
    func midiTimeTableViewWidthForRowHeaderCells(_ midiTimeTableView: MIDITimeTableView) -> CGFloat {
        // return 100   // Default value
        return timeTableHeaderWidth
    }
    
    func midiTimeTableView(_ midiTimeTableView: MIDITimeTableView, didDelete cells: [MIDITimeTableCellIndex]) {
        rowData.removeCells(at: cells)
        saveNoteData()
        timeTableView?.reloadData()
    }

    
    func midiTimeTableView(_ midiTimeTableView: MIDITimeTableView, didEdit cells: [MIDITimeTableViewEditedCellData]) {
        var removingCells = [MIDITimeTableCellIndex]()
        for cell in cells {
            rowData[cell.index].duration = cell.newDuration
            rowData[cell.index].position = cell.newPosition

            // update cell row
            if cell.index.row != cell.newRowIndex {
                rowData.appendCell(rowData[cell.index], row: cell.newRowIndex)
                removingCells.append(cell.index)
            }
        }
        
        rowData.removeCells(at: removingCells)
        timeTableView?.reloadData()
        saveNoteData()
    }
    
   
    
    func midiTimeTableView(_ midiTimeTableView: MIDITimeTableView, didUpdatePlayhead position: Double) {
        return
    }
    
    func midiTimeTableView(_ midiTimeTableView: MIDITimeTableView, didUpdateRangeHead position: Double) {
        return
    }
    
    func midiTimeTableView(_ midiTimeTableView: MIDITimeTableView, historyDidChange history: MIDITimeTableHistory) {
        rowData = history.currentItem
    }

}
