// Окно выбора папки с песнями

import UIKit

class SongFoldersVC: UIViewController{
    @IBOutlet weak var folderTableView: UITableView!
    let disk = HRFiles.shared
    let sets = GlobalSettings.singleton
    let files = HRFileOperations.shared
    let poster = NotificationCenter.default
    let postName = NSNotification.Name.self
    var folderList = [String]()
    var movingFolder = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        folderTableView.dataSource = self
        folderTableView.delegate = self
        
        let index = files.songFoldersList.index(of: files.folder)
        let indexPath = IndexPath(item: index!, section: 0)
        folderTableView.selectRow(at: indexPath, animated: true, scrollPosition: .top)
    }

    @IBAction func addFolderBtn(_ sender: Any) {
        files.addSongFolder()
        let indexPath = IndexPath(item: 0, section: 0)
        folderTableView.insertRows(at: [indexPath], with: .automatic)
    }
    
    
    
    
    
    // Всплывающие при переименовании окна
    func alertRename() {
        let alert = UIAlertController(title: "Rename selected song", message: nil, preferredStyle: .alert)
        
        alert.addTextField(configurationHandler: { (textField) in
            textField.placeholder = "New song name"
        })
        
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            let myTextField = alert.textFields![0] as UITextField
            
            if self.disk.checkFile(subFolder1: "Songs", subFolder2: nil, fileName: myTextField.text!) != nil {
                self.alertWrong()
            
            }else{
                self.files.renameSongFolder(oldName:self.movingFolder, newName:myTextField.text!)
                self.folderTableView.reloadData()
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
  
    
    
    // Всплывающее оно с ошибкой
    func alertWrong() {
        let alert = UIAlertController(title: "Error", message: "A file with this name already exists", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            self.alertRename()
        }))
        self.present(alert, animated: true, completion: nil)
    }
}
