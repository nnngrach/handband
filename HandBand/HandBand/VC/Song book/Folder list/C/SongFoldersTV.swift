import UIKit
extension SongFoldersVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return files.songFoldersList.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = folderTableView.dequeueReusableCell(withIdentifier: "folderCell") as! SongFolderCell
        let text = files.songFoldersList[indexPath.row]
        cell.folderLabel.text = text
        
        if text == files.folder {
            cell.isSelected = true
            cell.contentView.backgroundColor = #colorLiteral(red: 0.4352941176, green: 0.3411764706, blue: 0.3450980392, alpha: 1)
        } else {
            cell.isSelected = false
            cell.contentView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        }
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let deleteAction = UITableViewRowAction(style: .normal, title: "Delete") { (action:UITableViewRowAction, indexPath:IndexPath) in
            self.files.deleteSongFolder(index: indexPath.row)
            self.folderTableView.deleteRows(at: [indexPath], with: .automatic)
        }
        
        let renameAction = UITableViewRowAction(style: .normal, title: "Rename") { (action:UITableViewRowAction, indexPath:IndexPath) in
            
            self.movingFolder = self.files.songFoldersList[indexPath.row]
            //self.sets.loadSong()
            self.alertRename()
        }
        
        renameAction.backgroundColor = UIColor.lightGray
        deleteAction.backgroundColor = #colorLiteral(red: 0.6940000057, green: 0.3880000114, blue: 0.3840000033, alpha: 1)
        return [deleteAction, renameAction]
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        files.folder = files.songFoldersList[indexPath.row]
        poster.post(name: postName.init("Song folder changed"), object: nil)
        
        let selectedCell:UITableViewCell = tableView.cellForRow(at: indexPath)!
        selectedCell.contentView.backgroundColor = #colorLiteral(red: 0.4352941176, green: 0.3411764706, blue: 0.3450980392, alpha: 1)
    }
    
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let selectedCell:UITableViewCell = tableView.cellForRow(at: indexPath)!
        selectedCell.contentView.backgroundColor = UIColor.clear
    }
    
}
