// Активация одного из восьми доступных для песни пресета.
// Или переход к странице редактирования выборанного пресета.
import UIKit

class PresetsVC: UIViewController {
    @IBOutlet weak var presetTanle: UITableView!
    
    let disk = HRFiles.shared
    let sets = GlobalSettings.singleton
    let files = HRFileOperations.shared
    let poster = NotificationCenter.default
    let postName = NSNotification.Name.self
    var renamingPresetNumber = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presetTanle.delegate = self
        presetTanle.dataSource = self
        poster.addObserver(self, selector: #selector(updateUI(n:)), name: postName.init("Preset Changed"), object: nil)
    }
    
    @objc func updateUI(n: NSNotification) {
        presetTanle.reloadData()
    }
  

    
    // MARK: - Alerts
    
    // Сохранить пресет
    func alertEnterName() {
        let alert = UIAlertController(title: "Save selected preset to templates", message: "Enter name for new preset", preferredStyle: .alert)
        
        alert.addTextField(configurationHandler: { (textField) in
            textField.text = self.files.loadedSong.presetList[self.renamingPresetNumber].name
        })
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            let myTextField = alert.textFields![0] as UITextField
            self.files.exportPreset(with: myTextField.text!)
            self.presetTanle.reloadData()
            self.alertGood()
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func alertGood() {
        let alert = UIAlertController(title: "Succes", message: "The preset is saved", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func alertWrong() {
        let alert = UIAlertController(title: "Error", message: "A file with this name already exists", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            self.alertEnterName()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    // Переименовать пресет
    func alertRename() {
        let alert = UIAlertController(title: "Rename selected preset", message: nil, preferredStyle: .alert)
        
        alert.addTextField(configurationHandler: { (textField) in
            textField.placeholder = "New preset name"
        })
        
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            let myTextField = alert.textFields![0] as UITextField
            let preset = self.renamingPresetNumber
            self.files.loadedSong.presetList[preset].name = myTextField.text!
            self.files.resaveSong()
            self.presetTanle.reloadData()
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

}
