
import UIKit
extension PresetsVC: UITableViewDelegate, UITableViewDataSource  {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = presetTanle.dequeueReusableCell(withIdentifier: "cell") as! PresetCell
        let name = files.loadedSong.presetList[indexPath.row].name
        cell.presetLabel.text = "Preset \(indexPath.row + 1) - " + name
        
        if indexPath.row == files.preset {
            cell.contentView.backgroundColor = #colorLiteral(red: 0.4352941176, green: 0.3411764706, blue: 0.3450980392, alpha: 1)
        } else {
            cell.contentView.backgroundColor = UIColor.clear
        }
        
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let renameAction = UITableViewRowAction(style: .normal, title: "Rename") { (action:UITableViewRowAction, indexPath:IndexPath) in
            self.renamingPresetNumber = indexPath.row
            self.alertRename()
        }
        
        let saveAction = UITableViewRowAction(style: .normal, title: "Save") { (action:UITableViewRowAction, indexPath:IndexPath) in
            self.renamingPresetNumber = indexPath.row
            self.alertEnterName()
        }
        
        let loadAction = UITableViewRowAction(style: .normal, title: "Load") { (action:UITableViewRowAction, indexPath:IndexPath) in
            
            self.files.preset = indexPath.row
            self.performSegue(withIdentifier: "To Preset List", sender: self)
        }
        
        renameAction.backgroundColor = UIColor.lightGray
        loadAction.backgroundColor = #colorLiteral(red: 0.4004528569, green: 0.5802476264, blue: 0.6941176471, alpha: 1)
        saveAction.backgroundColor = #colorLiteral(red: 0.2705882353, green: 0.3607843137, blue: 0.4823529412, alpha: 1)
        
        return [saveAction, loadAction, renameAction]
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedCell:UITableViewCell = tableView.cellForRow(at: indexPath)!
        presetTanle.reloadData()
        selectedCell.contentView.backgroundColor = #colorLiteral(red: 0.4352941176, green: 0.3411764706, blue: 0.3450980392, alpha: 1)
        files.preset = indexPath.row
    }
    
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let selectedCell:UITableViewCell = tableView.cellForRow(at: indexPath)!
        selectedCell.contentView.backgroundColor = UIColor.clear
    }
    
}
