import UIKit
extension SongListVC: UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return files.songList.count
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = songListTable.dequeueReusableCell(withIdentifier: "songlistCell") as! SongLIstCell
        let filename = files.songList[indexPath.row]
        let lengh = filename.count - files.songFile.count - 1
        cell.songLabel.text = filename[0 ..< lengh]
        cell.button2.addTarget(self, action: #selector(buttonTapped(_:)), for: .touchUpInside)
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let deleteAction = UITableViewRowAction(style: .normal, title: "Delete") { (action:UITableViewRowAction, indexPath:IndexPath) in
            self.files.deleteSong(index: indexPath.row)
            self.songListTable.deleteRows(at: [indexPath], with: .automatic)
        }
        
        let copyAction = UITableViewRowAction(style: .normal, title: "Copy") { (action:UITableViewRowAction, indexPath:IndexPath) in
            
            // print("copy row ", indexPath.row)
            self.files.loadSongBy(number: indexPath.row)
            self.files.dublicateCurrentSong()
            let indexPath = IndexPath(item: 0, section: 0)
            self.songListTable.insertRows(at: [indexPath], with: .automatic)
        }
        
        let renameAction = UITableViewRowAction(style: .normal, title: "Rename") { (action:UITableViewRowAction, indexPath:IndexPath) in
            self.files.loadSongBy(number: indexPath.row)
            self.alertRename()
        }
        
        renameAction.backgroundColor = UIColor.lightGray
        copyAction.backgroundColor = #colorLiteral(red: 0.4004528569, green: 0.5802476264, blue: 0.6941176471, alpha: 1)
        deleteAction.backgroundColor = #colorLiteral(red: 0.6940000057, green: 0.3880000114, blue: 0.3840000033, alpha: 1)
        return [deleteAction, copyAction, renameAction]
    }
    
}
