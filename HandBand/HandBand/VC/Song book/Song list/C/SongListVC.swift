// Окно выбора песни в указанной папке

import UIKit

class SongListVC: UIViewController {
    @IBOutlet weak var songListTable: UITableView!
    let disk = HRFiles.shared
    let sets = GlobalSettings.singleton
    let files = HRFileOperations.shared
    let poster = NotificationCenter.default
    let postName = NSNotification.Name.self
    var songList = [String]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        songListTable.dataSource = self
        songListTable.delegate = self
        files.viewSelectedSongFolder()
        

        poster.addObserver(self, selector: #selector(updateUI(n:)), name: postName.init("Song folder changed"), object: nil)
        poster.addObserver(self, selector: #selector(updateUI(n:)), name: postName.init("Song is saved"), object: nil)
        poster.addObserver(self, selector: #selector(gotoSongEditor(n:)), name: postName.init("Segue to Song Editor"), object: nil)
    }


    
    @objc func updateUI(n: NSNotification) {
        files.viewSelectedSongFolder()
        songListTable.reloadData()
    }
    
    
    @objc func gotoSongEditor(n: NSNotification) {
        files.loadSong()
    }
    
    

    
    
    @IBAction func addSongButton(_ sender: Any) {
        files.addSong()
        let indexPath = IndexPath(item: 0, section: 0)
        songListTable.insertRows(at: [indexPath], with: .automatic)
    }
    
    @IBAction func copySongButton(_ sender: Any) {
        files.dublicateCurrentSong()
        let indexPath = IndexPath(item: 0, section: 0)
        songListTable.insertRows(at: [indexPath], with: .automatic)
    }
    
    
    
    // Определяет индекс по координате ячейки. Использует расширение TableView
    @IBAction func buttonTapped(_ button: UIButton) {
        if let indexPath = self.songListTable.indexPathForView(button) {
            //print("Button tapped at indexPath \(indexPath)")

            sets.songString = 0
            files.loadSongBy(number: indexPath.row)
            performSegue(withIdentifier: "toSongEditor", sender: self)
        }
        else {
            print("Button indexPath not found")
        }
    }
    

    
    
   // Всплывающие при переименовании окна
    func alertRename() {
        let alert = UIAlertController(title: "Rename selected song", message: nil, preferredStyle: .alert)
        
        alert.addTextField(configurationHandler: { (textField) in
            textField.placeholder = "New song name"
        })
        
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            let myTextField = alert.textFields![0] as UITextField
            let subfolder2 = self.files.folder
            
            if self.disk.checkFile(subFolder1: "Songs", subFolder2: subfolder2, fileName: myTextField.text!) != nil {
                self.alertWrong()
                
            }else{
                self.files.renameCurrentSong(newName: myTextField.text!)
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    func alertWrong() {
        let alert = UIAlertController(title: "Error", message: "A file with this name already exists", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            self.alertRename()
        }))
        self.present(alert, animated: true, completion: nil)
    }
}
