// Папка со всеми хранящимися на устройстве файлами пресетов
// Можно удалить, загрузить или прослушать их звучание
import UIKit

class PresetListVC: UIViewController {
    @IBOutlet weak var presetTableView: UITableView!
    let disk = HRFiles.shared
    let sets = GlobalSettings.singleton
    let files = HRFileOperations.shared
    let saves = UserDefaults.standard
    let poster = NotificationCenter.default
    let postName = NSNotification.Name.self
    var renamingPresetNumber = 0
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        presetTableView.delegate = self
        presetTableView.dataSource = self
        
        poster.addObserver(self, selector: #selector(updateUI(n:)), name: postName.init("Preset Updated"), object: nil)
        poster.post(name: postName.init("selectMode"), object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        poster.post(name: postName.init("Preset Changed"), object: nil)
    }
    
    @objc func updateUI(n: NSNotification) {
        presetTableView.reloadData()
    }
    
    
    
    // MARK: File functions
    
    @IBAction func addPresetButton(_ sender: Any) {
        files.addPreset()
        let indexPath = IndexPath(item: 0, section: 0)
        presetTableView.insertRows(at: [indexPath], with: .automatic)
    }
    
    
    @IBAction func dublicatePresetButton(_ sender: Any) {
        
        files.dublicatePreset()
        let indexPath = IndexPath(item: 0, section: 0)
        presetTableView.insertRows(at: [indexPath], with: .automatic)
    }

    
    
    
    
    // MARK: Alerts
    
    func alertRename() {
        let alert = UIAlertController(title: "Rename selected preset", message: nil, preferredStyle: .alert)
        
        alert.addTextField(configurationHandler: { (textField) in
            textField.placeholder = "New preset name"
        })
        
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            let myTextField = alert.textFields![0] as UITextField
            
            if self.disk.checkFile(subFolder1: "Presets", subFolder2: nil, fileName: myTextField.text!) != nil {
                self.alertWrong()
            } else {
                self.files.deletePreset(at: self.renamingPresetNumber)
                self.files.exportPreset(with: myTextField.text!)
                self.presetTableView.reloadData()
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func alertWrong() {
        let alert = UIAlertController(title: "Error", message: "A file with this name already exists", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            self.alertRename()
        }))
        self.present(alert, animated: true, completion: nil)
    }

}
