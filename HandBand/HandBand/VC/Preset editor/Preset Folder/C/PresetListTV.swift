import UIKit
extension PresetListVC: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return files.presetFolderList.count
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = presetTableView.dequeueReusableCell(withIdentifier: "cell") as! PresetListCell
        let filename = files.presetFolderList[indexPath.row]
        let lengh = filename.count - files.presetFile.count - 1
        cell.label.text = filename[0 ..< lengh]
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let deleteAction = UITableViewRowAction(style: .normal, title: "Delete") { (action:UITableViewRowAction, indexPath:IndexPath) in
            self.files.deletePreset(at: indexPath.row)
            self.presetTableView.deleteRows(at: [indexPath], with: .automatic)
        }
        
        let renameAction = UITableViewRowAction(style: .normal, title: "Rename") { (action:UITableViewRowAction, indexPath:IndexPath) in
            self.renamingPresetNumber = indexPath.row
            self.files.loadPreset(index: indexPath.row)
            self.alertRename()
        }
        
        renameAction.backgroundColor = UIColor.lightGray
        deleteAction.backgroundColor = #colorLiteral(red: 0.6940000057, green: 0.3880000114, blue: 0.3840000033, alpha: 1)
        return [deleteAction, renameAction]
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        files.loadPreset(index: indexPath.row)
        files.resaveSong()
        
        let selectedCell:UITableViewCell = tableView.cellForRow(at: indexPath)!
        selectedCell.contentView.backgroundColor = #colorLiteral(red: 0.4352941176, green: 0.3411764706, blue: 0.3450980392, alpha: 1)
    }
    
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let selectedCell:UITableViewCell = tableView.cellForRow(at: indexPath)!
        selectedCell.contentView.backgroundColor = UIColor.clear
    }
    
}
