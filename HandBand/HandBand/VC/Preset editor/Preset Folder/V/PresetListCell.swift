import UIKit

class PresetListCell: UITableViewCell {
    let sets = GlobalSettings.singleton
    let files = HRFileOperations.shared
    let saves = UserDefaults.standard
    let poster = NotificationCenter.default
    let postName = NSNotification.Name.self

    
    // сюда таблица присылает номер строки
    var selectedRow = 0
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var fastBankLabel: UIButton!
    
    
    @IBAction func gotoPresetSettings(_ sender: Any) {
        sets.showingPreset = selectedRow
    }
    
    // Передаем номер пресета, интерфес для которого следует загружать
    @IBAction func gotoInstrumentsList(_ sender: Any) {
        files.preset = selectedRow
        poster.post(name: postName.init("PresetChanged"), object: nil)
    }
    

    
    // генерируем  название для бирки
    // с сочетанием клавиш для быстрого вызова пресета.
    // нулевой индекс будет обозначаться знаком "-"
    
//    func generateBankLabel(for showedPreset: Int) {
//        var letter = "-"
//        var number = "-"
//
//        let storedLetter = files.loadedSong.presetList[showedPreset].storeBankLetter
//        let storedNumber = files.loadedSong.presetList[showedPreset].storeBankNumber
//
//        switch storedLetter {
//        case 1:
//            letter = "A"
//        case 2:
//            letter = "B"
//        case 3:
//            letter = "C"
//        case 4:
//            letter = "D"
//        default:
//            letter = "-"
//        }
//
//
//        switch storedNumber {
//        case 0:
//            number = "-"
//        default:
//            number = "\(storedNumber)"
//        }
//
//
//        let resultText = (letter + " " + number)
//
//        fastBankLabel.setTitle(resultText, for: .normal)
//    }
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    

}
