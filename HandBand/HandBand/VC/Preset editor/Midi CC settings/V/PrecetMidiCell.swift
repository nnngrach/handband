import UIKit

class PrecetMidiCell: UITableViewCell {
    @IBOutlet weak var switcher: UISwitch!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var ccBtn: UIButton!
    @IBOutlet weak var valueBtn: UIButton!
    
    let files = HRFileOperations.shared
    let sets = GlobalSettings.singleton
    var ccArray: [toggleCC] = []
    var startArray: [presetMidiCC] = []
    var displayPage = 0
    var currentRow = 0
    var instrument = 0
    var preset = 0
    
    
    
    

    func setupCell() {
        ccArray = files.loadedSong.presetList[preset].instrumentsMidiSets[instrument].usedCC
        startArray = files.loadedSong.presetList[preset].instrumentsMidiSets[instrument].startPresetMidiMessages
        
        switch displayPage {
        case 0:
            // для меню  "Send and Receive MIDI messages"
            label.text = ccArray[currentRow].name
            switcher.isOn = ccArray[currentRow].isOn
            valueBtn.isHidden = true
            ccBtn.isHidden = true
            
            
        default:
            // для меню  "Send MIDI CC when preset loaded"
            label.text = startArray[currentRow].name
            ccBtn.setTitle( String(startArray[currentRow].ccNumber), for: .normal)
            switcher.isOn = startArray[currentRow].isOn
            valueBtn.isHidden = false
            ccBtn.isHidden = currentRow < 2 ? true : false
            
            
            // Смещение для второй строки Transpose, которая содержит отрицательные значения
            if currentRow != 1 {
                valueBtn.setTitle( String(startArray[currentRow].value), for: .normal)
            } else {
                valueBtn.setTitle( String(startArray[currentRow].value - 12), for: .normal)
            }
        }
    }
    
    
    func buttonPressed(number: Int){
        sets.pickingCC = [currentRow, number]
        NotificationCenter.default.post(name: NSNotification.Name.init("Show CC Picker"),  object: nil)
    }
    
    
  
    
    
    @IBAction func switcherAct(_ sender: UISwitch) {
        switch displayPage {
        case 0:
            files.loadedSong.presetList[preset].instrumentsMidiSets[instrument].usedCC[currentRow].isOn = switcher.isOn
        
        default:            files.loadedSong.presetList[preset].instrumentsMidiSets[instrument].startPresetMidiMessages[currentRow].isOn = switcher.isOn
        }
        
        files.resaveSong()
    }
    
    
    
    @IBAction func ccBtnAct(_ sender: Any) {
        buttonPressed(number: 0)
    }
    
    @IBAction func valueBtnAct(_ sender: Any) {
        buttonPressed(number: 1)
    }
    

    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
