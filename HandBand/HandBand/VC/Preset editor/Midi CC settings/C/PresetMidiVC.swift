// Подменю из меню Midi Settings
// Используется сразу для двух похожих меню:
// "Send and Receive MIDI messages"
// "Send MIDI CC when preset loaded"

import UIKit

class PresetMidiVC: UIViewController {
    @IBOutlet weak var ccTableView: UITableView!
    @IBOutlet weak var pickerOutView: UIView!
    @IBOutlet weak var picker: UIPickerView!
    
    let sets = GlobalSettings.singleton
    let files = HRFileOperations.shared
    var startArray: [presetMidiCC] = []
    var ccArray: [toggleCC] = []
    
    var displayPage = 0
    var preset = 0
    var instrument = 0
    
    var pickedRow = 0
    var pickerShowed = false
    var pickerSize = CGRect()
    let ccNumbers = Array(0 ... 127)
    let transposeNumbers = Array(-12 ... 12)
    
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        pickerSetup()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(showPicker(n:)),
                                               name: NSNotification.Name.self.init("Show CC Picker"), object: nil)
    }
    
    
    
    
    // MARK: Всплывающее меню
    
    // При нажатии на окошко со значением настроек
    // появляется всплывающее меню с PickerView для выбора новых значений
    @objc func showPicker(n: NSNotification) {
        if !pickerShowed {
            picker.reloadComponent(0)
            selectPickerRow()
        }
        changePickerSize(withDuration: 0.4)
    }
    
    
    // Настройки высплывающего меню
    func pickerSetup() {
        ccArray = files.loadedSong.presetList[preset].instrumentsMidiSets[instrument].usedCC
        startArray = files.loadedSong.presetList[preset].instrumentsMidiSets[instrument].startPresetMidiMessages
        pickerShowed = true
        view.addSubview(pickerOutView)
        pickerOutView.translatesAutoresizingMaskIntoConstraints = false
        selectPickerRow()
        changePickerSize(withDuration: 0)
    }

    
    
    func changePickerSize(withDuration: Double) {
        pickerShowed = !pickerShowed
        
        if pickerShowed {
            pickerSize = CGRect(x: 0,
                             y: self.view.bounds.height * 3 / 4,
                             width: self.view.bounds.width,
                             height: self.view.bounds.height / 4)
            
        } else {
            pickerSize = CGRect(x: 0,
                             y: self.view.bounds.height,
                             width: self.view.bounds.width,
                             height: self.view.bounds.height / 4)
        }
        
        
        UIView.animate(withDuration: withDuration) {
            self.pickerOutView.frame = self.pickerSize
        }
    }
    
    
    
    func selectPickerRow() {
        startArray = files.loadedSong.presetList[preset].instrumentsMidiSets[instrument].startPresetMidiMessages
        var value = 0
        if sets.pickingCC[1] == 0 {
            value = startArray[sets.pickingCC[0]].ccNumber
        } else {
            value = startArray[sets.pickingCC[0]].value
        }
        picker.selectRow(value, inComponent: 0, animated: true)
    }
    
    

    
    
    @IBAction func donePickerBtn(_ sender: Any) {
        let row = sets.pickingCC[0]
        let slot = sets.pickingCC[1]
        
        if slot == 0 {
            files.loadedSong.presetList[preset].instrumentsMidiSets[instrument].startPresetMidiMessages[row].ccNumber = pickedRow
        } else{
            files.loadedSong.presetList[preset].instrumentsMidiSets[instrument].startPresetMidiMessages[row].value = pickedRow
        }
        
        files.resaveSong()
        let index = IndexPath(item: row, section: 0)
        ccTableView.reloadRows(at: [index], with: .automatic)
        
        changePickerSize(withDuration: 0.4)
    }

}
