
import UIKit
extension PresetMidiVC: UITableViewDelegate, UITableViewDataSource  {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch displayPage {
        case 0:
            return ccArray.count
        default:
            return startArray.count
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = ccTableView.dequeueReusableCell(withIdentifier: "cell") as! PrecetMidiCell
        cell.displayPage = displayPage
        cell.currentRow = indexPath.row
        cell.instrument = instrument
        cell.preset = preset
        cell.setupCell()
        return cell
    }
}
