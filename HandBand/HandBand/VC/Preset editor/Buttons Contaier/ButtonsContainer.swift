// На каждой из страниц меню настроек
// присутствует панель с кнопками для предпрослушивания
// всех пэдов, а также клавиш правой руки

import UIKit

class ButtonsContainer: UIViewController {
    
    @IBOutlet var allButtons: [HRButton]!
    @IBOutlet weak var opacityBlackKeysButton: HRButton!
    @IBOutlet var pianoKeysWhite: [HRButton]!
    @IBOutlet var pianoKeysBlack: [HRButton]!
    
    
    let sets = GlobalSettings.singleton
    let files = HRFileOperations.shared
    let midiIn = AudioInOut.singleton
    let midiIdentifier = DetecterComboKeyNumbers.singleton
    let hk = HotKeyController.singleton
    let poster = NotificationCenter.default
    let postName = NSNotification.Name.self
    
    var notesForButton = [[UInt8]]()
    var inSelectMode = true
    var presetNumber = 0
    
    
    override func viewWillLayoutSubviews() {
        triggerMode()
        blackKeysBlocker()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadNotesToPlay()
        
        poster.addObserver(self, selector: #selector(selectMode(n:)), name: postName.init("selectMode"), object: nil)
        poster.addObserver(self, selector: #selector(blockMode(n:)), name: postName.init("blockMode"), object: nil)
    }

    
    
    
    // MARK: Holding down buttons
    
    // Сделать все кнопки доступными для игры
    @objc func selectMode(n: NSNotification) {
        inSelectMode = true
        triggerMode()
        blackKeysBlocker()
    }

    // Сделать доступной для тгры только кнопку выбранного пэда.
    // Все остальные кнопки заблокировать и погасить
    @objc func blockMode(n: NSNotification) {
        inSelectMode = false
        triggerMode()
    }
    
    
    func NotifyPadScreenUpdated() {
        poster.post(name: postName.init("PadChanged"), object: nil)
    }
    
    
    // Заблокировать кнопку настроек для черных клавиш правой руки
    func blackKeysBlocker() {
        let preset = files.preset
        let useSepareteScaleFoBlackKeys = files.loadedSong.presetList[preset].useSmartScaleForBlackKeys
        var color = UIColor()
       
        if useSepareteScaleFoBlackKeys {
            opacityBlackKeysButton.isEnabled = true
            color = #colorLiteral(red: 0.4777653813, green: 0.2681455612, blue: 0.2628641129, alpha: 1)
        } else {
            opacityBlackKeysButton.isEnabled = false
            color = #colorLiteral(red: 0.2979272008, green: 0.3213181496, blue: 0.3433414698, alpha: 1)

        }
        
        for button in pianoKeysBlack {
            button.backgroundColor = color
        }
    }
    
    
    
    
    
    
    // MARK: - Coloring buttons
    
    // Подсветить нужгые клавиши в зависимости от выборанного режима
    func triggerMode() {
        if inSelectMode {
            lightSelected()
        } else {
            lightSelected()
            holdNonActiveButtons()
        }
    }
    
    
    // Выделить цветом все выбранные кнопки
    func lightSelected() {
        for button in allButtons {
            if button.tag == sets.showingPad {
                lightPadNumber(number: button.tag, active: true)
            } else {
                lightPadNumber(number: button.tag, active: false)
            }
        }
        blackKeysBlocker()
    }
    
    // Выделить кнопки по ее номеру
    func lightPadNumber(number: Int, active: Bool) {
        if number == 10 {
            makeWhiteKeysHold(hold: active)
        } else if number == 11 {
            makeBlackKeysHold(hold: active)
        } else {
            allButtons[number].holdDown = active
        }
        allButtons[number].isEnabled = true
    }
  
    
    
    // Подсветить белые клавиши
    func makeWhiteKeysHold(hold:Bool) {
        for key in pianoKeysWhite {
            key.holdDown = hold
        }
    }
    
    // Подсветить черные клавиши
    func makeBlackKeysHold(hold:Bool) {
        for key in pianoKeysBlack {
            key.holdDown = hold
        }
    }

    
    
    // Погасить все неактивные кнопки
    func holdNonActiveButtons() {
        for button in allButtons {
            if button.tag != sets.showingPad {
                button.isEnabled = false
            }
        }
    }
    
    
    
    

    
    // MARK: Playin notes
    
    // Генерация номеров нот для предпрослушивания игры всех пэдов
    func loadNotesToPlay() {
        let note = midiIdentifier.customNoteNumbersForComboKeys
        
        notesForButton = [ [note[0]],
                           [note[1]],
                           [note[2]],
                           [note[3]],
                           [note[4]],
                           [note[5]],
                           [note[6]],
                           [note[7]],
                           [note[10], note[11]],
                           [note[15], note[0], note[3], note[4], note[7]],
                           [96],
                           [97] ]
    }
    
    
    // Воспроизвести все ноты, сгенерированные для данного пэда
    func onAllNotes (in array:[UInt8]) {
        for note in array {
            midiIn.receivedMIDINoteOn(noteNumber: note, velocity: 110, channel: 0)
        }
    }
    
    
    // Остановить запущенные ноты
    func offAllNotes (in array:[UInt8]) {
        for note in array {
            midiIn.receivedMIDINoteOff(noteNumber: note, velocity: 0, channel: 0)
        }
    }
    
    
    
    
    
    
    // При нажатии кнопки  -  воспроизвести и подсветить
    @IBAction func buttonDown(_ sender: HRButton) {
        presetNumber = files.preset
        onAllNotes(in: notesForButton[sender.tag] )
        sets.showingPad = sender.tag
        NotifyPadScreenUpdated()
        //print(sender.tag)
        
        if inSelectMode {
            lightSelected()
        }
        
        if sender.tag == 10 {
            makeWhiteKeysHold(hold: true)
        } else if sender.tag == 11 {
            makeBlackKeysHold(hold: true)
        }
        
        files.preset = presetNumber
    }
    
    
    
    
    // При отпускании кнопки
    @IBAction func buttonUp(_ sender: HRButton) {
        offAllNotes(in: notesForButton[sender.tag] )
    }
}
