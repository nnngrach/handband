import UIKit

extension StyleDrumVC: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 4
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 87
    }
    
    
    // ноты в списке с названиями начинаются не сначала, а с 21-го номера
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let noteNumber = String(row + 21)
        let noteName = noteNum.noteNameFromMidiNote(row + 21)
        return noteNumber + " - " + noteName
    }
    
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let noteNumber = String(row + 21)
        let noteName = noteNum.noteNameFromMidiNote(row + 21)
        let titleData = noteNumber + " - " + noteName
        
        let myTitle = NSAttributedString(string: titleData, attributes: [NSAttributedString.Key.font:UIFont(name: "Georgia", size: 15.0)!,NSAttributedString.Key.foregroundColor:UIColor.white])
        return myTitle
    }
    
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch component {
        case 0:
            files.loadedSong.presetList[preset].instrumentsMidiSets[instrument].pads[pad].drumm1Note = row
            files.resaveSong()
        case 1:
            files.loadedSong.presetList[preset].instrumentsMidiSets[instrument].pads[pad].drumm2Note = row
            files.resaveSong()
        case 2:
            files.loadedSong.presetList[preset].instrumentsMidiSets[instrument].pads[pad].drumm3Note = row
            files.resaveSong()
        default:
            files.loadedSong.presetList[preset].instrumentsMidiSets[instrument].pads[pad].drumm4Note = row
            files.resaveSong()
        }
    }
}
