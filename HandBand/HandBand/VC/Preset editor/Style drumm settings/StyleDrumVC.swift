//  Настройка стиля игры для ударных инструментов

import UIKit

class StyleDrumVC: UIViewController {
    
    let sets = GlobalSettings.singleton
    let files = HRFileOperations.shared
    let poster = NotificationCenter.default
    let postName = NSNotification.Name.self
    let noteNum = NoteNames()
    
    var preset = 0
    var pad = 0
    var instrument = 0
    
    @IBOutlet weak var volume: UISlider!
    @IBOutlet weak var picker: UIPickerView!
    @IBOutlet weak var switch1: UISwitch!
    @IBOutlet weak var switch2: UISwitch!
    @IBOutlet weak var switch3: UISwitch!
    @IBOutlet weak var switch4: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUi()
        poster.post(name: postName.init("blockMode"), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        poster.post(name: postName.init("selectMode"), object: nil)
    }
    

    func updateUi() {
        preset = files.preset
        pad = sets.showingPad
        instrument = sets.showingInstrument
        volume.value = files.loadedSong.presetList[preset].instrumentsMidiSets[instrument].pads[pad].relativeVelocity
        
        switch1.isOn = files.loadedSong.presetList[preset].instrumentsMidiSets[instrument].pads[pad].drumm1IsOn
        switch2.isOn = files.loadedSong.presetList[preset].instrumentsMidiSets[instrument].pads[pad].drumm2IsOn
        switch3.isOn = files.loadedSong.presetList[preset].instrumentsMidiSets[instrument].pads[pad].drumm3IsOn
        switch4.isOn = files.loadedSong.presetList[preset].instrumentsMidiSets[instrument].pads[pad].drumm4IsOn
        
        picker.selectRow(files.loadedSong.presetList[preset].instrumentsMidiSets[instrument].pads[pad].drumm1Note,
                         inComponent: 0, animated: true)
        picker.selectRow(files.loadedSong.presetList[preset].instrumentsMidiSets[instrument].pads[pad].drumm2Note,
                         inComponent: 1, animated: true)
        picker.selectRow(files.loadedSong.presetList[preset].instrumentsMidiSets[instrument].pads[pad].drumm3Note,
                         inComponent: 2, animated: true)
        picker.selectRow(files.loadedSong.presetList[preset].instrumentsMidiSets[instrument].pads[pad].drumm4Note,
                         inComponent: 3, animated: true)
    }
    
    
    
    
    
    @IBAction func volumeAct(_ sender: Any) {
        files.loadedSong.presetList[preset].instrumentsMidiSets[instrument].pads[pad].relativeVelocity = volume.value
        files.resaveSong()
    }
    @IBAction func switch1Act(_ sender: Any) {
        files.loadedSong.presetList[preset].instrumentsMidiSets[instrument].pads[pad].drumm1IsOn = switch1.isOn
        files.resaveSong()
    }
    @IBAction func switch2Act(_ sender: Any) {
        files.loadedSong.presetList[preset].instrumentsMidiSets[instrument].pads[pad].drumm2IsOn = switch2.isOn
        files.resaveSong()
    }
    @IBAction func switch3Act(_ sender: Any) {
        files.loadedSong.presetList[preset].instrumentsMidiSets[instrument].pads[pad].drumm3IsOn = switch3.isOn
        files.resaveSong()
    }
    @IBAction func switch4Act(_ sender: Any) {
        files.loadedSong.presetList[preset].instrumentsMidiSets[instrument].pads[pad].drumm4IsOn = switch4.isOn
        files.resaveSong()
    }
}
