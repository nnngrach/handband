import UIKit

class ScaleTransformVC: UIViewController {
    @IBOutlet weak var switch1: UISwitch!
    @IBOutlet weak var switch2: UISwitch!
    @IBOutlet weak var switch3: UISwitch!
    @IBOutlet weak var scalePicker: UIPickerView!
    
    let scales = ScaleTransformer.singleton
    let files = HRFileOperations.shared
    let sets = GlobalSettings.singleton
    let poster = NotificationCenter.default
    let postName = NSNotification.Name.self
    var preset = 0
    var instrument = 0
    var pad = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        poster.post(name: postName.init("selectMode"), object: nil)
    }
    
    
    // Загрузить настройки и обновить интерфейс
    func updateUI() {
        preset = files.preset
        pad = sets.showingPad
        instrument = sets.showingInstrument
        
        switch1.isOn = files.loadedSong.presetList[preset].useSmartScaleForWhiteKeys
        switch2.isOn = files.loadedSong.presetList[preset].useSmartScaleForBlackKeys
        switch3.isOn = files.loadedSong.presetList[preset].useRegularScaleFowWhite
        scalePicker.selectRow(files.loadedSong.presetList[preset].regularScaleNumber, inComponent: 0, animated: true)
    }
    

    // При включении переключателя "Обычный режим"
    // остальные два переключателя отщелкиваются
    func switchBlocker(regularMode: Bool) {
        if regularMode {
            switch1.isOn = false
            switch2.isOn = false
            files.loadedSong.presetList[preset].useSmartScaleForWhiteKeys = switch1.isOn
            files.loadedSong.presetList[preset].useSmartScaleForBlackKeys = switch2.isOn
        } else {
            switch3.isOn = false
            files.loadedSong.presetList[preset].useRegularScaleFowWhite = switch3.isOn
        }
    }
    
    
    
    
    
    // MARK: - Actions
    
    @IBAction func switch1Act(_ sender: UISwitch) {
        switchBlocker(regularMode: false)
        files.loadedSong.presetList[preset].useSmartScaleForWhiteKeys = switch1.isOn
        files.resaveSong()
    }
    
    @IBAction func switch2Act(_ sender: UISwitch) {
        switchBlocker(regularMode: false)
        files.loadedSong.presetList[preset].useSmartScaleForBlackKeys = switch2.isOn
        files.resaveSong()
    }
    
    @IBAction func switch3Act(_ sender: UISwitch) {
        switchBlocker(regularMode: true)
        files.loadedSong.presetList[preset].useRegularScaleFowWhite = switch3.isOn
        files.resaveSong()
    }
    
    
    
    
    // Дописать текстовые описания для всех пунктов настроек
    @IBAction func helpButtons(_ sender: UIButton) {
    }
    
}
