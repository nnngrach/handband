import UIKit

class InstrumentMidiSetVC: UIViewController {
    let sets = GlobalSettings.singleton
    let files = HRFileOperations.shared
    let saves = UserDefaults.standard
    let poster = NotificationCenter.default
    let postName = NSNotification.Name.self
    
    var preset = 0
    var pad = 0
    var instrument = 0
    var maxNoteInRange = 0
    
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var segment: UISegmentedControl!
    @IBOutlet weak var rootOctaveStepper: UIStepper!
    @IBOutlet weak var rootOctaveLavel: UILabel!
    @IBOutlet weak var channelStepper: UIStepper!
    @IBOutlet weak var channelLabel: UILabel!
    @IBOutlet weak var maxVelocity: UISlider!
    @IBOutlet weak var midiCompressor: UISegmentedControl!
    @IBOutlet weak var lesfrRangeStepper: UIStepper!
    @IBOutlet weak var leftRangeLabel: UILabel!
    @IBOutlet weak var changeInversionSw: UISwitch!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
        poster.post(name: postName.init("blockMode"), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        poster.post(name: postName.init("selectMode"), object: nil)
    }
    
    
    // Загрузить настройки и обновить интерфейс
    func updateUI () {
        preset = files.preset
        pad = sets.showingPad
        instrument = sets.showingInstrument
        
        textField.text = files.loadedSong.presetList[preset].instrumentsMidiSets[instrument].name
        segment.selectedSegmentIndex = files.loadedSong.presetList[preset].instrumentsMidiSets[instrument].instrumentType
        rootOctaveStepper.value = Double(files.loadedSong.presetList[preset].instrumentsMidiSets[instrument].rootOctave)
        rootOctaveLavel.text = String(files.loadedSong.presetList[preset].instrumentsMidiSets[instrument].rootOctave)
        channelStepper.value = Double(files.loadedSong.presetList[preset].instrumentsMidiSets[instrument].midiChannel)
        channelLabel.text = String(files.loadedSong.presetList[preset].instrumentsMidiSets[instrument].midiChannel)
        maxVelocity.value = files.loadedSong.presetList[preset].instrumentsMidiSets[instrument].maxVelocity
        midiCompressor.selectedSegmentIndex = files.loadedSong.presetList[preset].instrumentsMidiSets[instrument].midiCompressorMode
        lesfrRangeStepper.value = Double(files.loadedSong.presetList[preset].instrumentsMidiSets[instrument].noteRange)
        changeInversionSw.isOn = files.loadedSong.presetList[preset].instrumentsMidiSets[instrument].useAutoInversion

        rangeLabelsGenerator()
    }
    
    
    func saveChanges() {
        files.resaveSong()
        NotificationCenter.default.post(name: NSNotification.Name.init("PadChanged"), object: nil)
    }
    
    
    // Сгенерировать диапозон нот доступных для левой руки
    func rangeLabelsGenerator() {
        let minRange = Int(lesfrRangeStepper.value)
        maxNoteInRange = minRange + 11

        let minNote = midiNumbers[minRange]
        let maxNote = midiNumbers[maxNoteInRange]
        leftRangeLabel.text = minNote + " - " + maxNote
    }
    
    
    


   
    
    
    
    // MARK: - Кнопки
    
    @IBAction func TextFieldAct(_ sender: Any) {
        files.loadedSong.presetList[preset].instrumentsMidiSets[instrument].name = textField.text!
        saveChanges()
    }
    
    @IBAction func segmentAct(_ sender: Any) {
        files.loadedSong.presetList[preset].instrumentsMidiSets[instrument].instrumentType = segment.selectedSegmentIndex
        saveChanges()
    }
    
    @IBAction func octaveAct(_ sender: Any) {
        files.loadedSong.presetList[preset].instrumentsMidiSets[instrument].rootOctave = Int(rootOctaveStepper.value)
        rootOctaveLavel.text = String(Int(rootOctaveStepper.value))
        saveChanges()
    }
    
    @IBAction func channelAct(_ sender: Any) {
        files.loadedSong.presetList[preset].instrumentsMidiSets[instrument].midiChannel = Int(channelStepper.value)
        channelLabel.text = String(Int(channelStepper.value))
        saveChanges()
    }

    @IBAction func maxVelocityAct(_ sender: Any) {
        files.loadedSong.presetList[preset].instrumentsMidiSets[instrument].maxVelocity = maxVelocity.value
        saveChanges()
    }
    
    @IBAction func midiCompressorAct(_ sender: UISegmentedControl) {
        files.loadedSong.presetList[preset].instrumentsMidiSets[instrument].midiCompressorMode = midiCompressor.selectedSegmentIndex
        saveChanges()
    }
    
    
    @IBAction func leftRangeStepperAct(_ sender: UIStepper) {
        files.loadedSong.presetList[preset].instrumentsMidiSets[instrument].noteRange = Int(lesfrRangeStepper.value)
        rangeLabelsGenerator()
        saveChanges()
    }
    
    
    @IBAction func changeInversionAct(_ sender: Any) {
        files.loadedSong.presetList[preset].instrumentsMidiSets[instrument].useAutoInversion = changeInversionSw.isOn
        saveChanges()
    }
    
    
    @IBAction func helpButtons(_ sender: UIButton) {
        let alert = UIAlertController(title: helpTitles[sender.tag], message: helpDescriptions[sender.tag], preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    
    
    // MARK: - Выбор страницы для перехода
    
    var pageToShow = 0
    
    
    @IBAction func toUseCCBtn(_ sender: Any) {
        gotoCCpage(number: 0)
    }
    
    @IBAction func toPresetCCBtn(_ sender: Any) {
        gotoCCpage(number: 1)
    }
    
    func gotoCCpage (number: Int) {
        pageToShow = number
        performSegue(withIdentifier: "toPresenCCScreen", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let nextScreen = segue.destination as? PresetMidiVC {
            nextScreen.displayPage = pageToShow
            nextScreen.instrument = instrument
            nextScreen.preset = preset
        }
    }
    
    
    
    
    
    // MARK: - Подписи
    // Текстовое описание для каждого пункта настроек. Дописать! %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    let helpTitles = ["Name" ,
                            "Type",
                            "Octave",
                            "Channel",
                            "Send sustain",
                            "Listen sustain",
                            "Max velocity",
                            "Min velocity",
                            "Left hand note range",
                            "Chane inversion after note",
                            "Send midi CC" ]
    
    let helpDescriptions = ["Name" ,
                            "Type",
                            "Octave",
                            "Channel",
                            "Send sustain",
                            "Listen sustain",
                            "Max velocity",
                            "Min velocity",
                            "Left hand note range",
                            "Chane inversion after note",
                            "Send midi CC" ]
    
    
    // номера нот для переключателя диапозона левой руки
    let midiNumbers = ["C1" , "C#1", "D1", "D#1", "E1", "F1", "F#1", "G1", "G#1", "A1", "A#1", "B1",
                       "C2" , "C#2", "D2", "D#2", "E2", "F2", "F#2", "G2", "G#2", "A2", "A#2", "B2" ]

}
