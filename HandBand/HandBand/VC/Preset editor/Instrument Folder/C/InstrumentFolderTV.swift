import UIKit

extension InstrumentFolderVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return files.instrumentFolderList.count
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = instrumentTable.dequeueReusableCell(withIdentifier: "cell") as! InstrumentFolderCell
        let filename = files.instrumentFolderList[indexPath.row]
        let lengh = filename.count - files.instrumentFile.count - 1
        cell.InstrumentLabel.text = filename[0 ..< lengh]
        
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let deleteAction = UITableViewRowAction(style: .normal, title: "Delete") { (action:UITableViewRowAction, indexPath:IndexPath) in
            self.clearCells()
            self.files.deleteInstrumentFile(at: indexPath.row)
            self.instrumentTable.deleteRows(at: [indexPath], with: .automatic)
        }
        
        let renameAction = UITableViewRowAction(style: .normal, title: "Rename") { (action:UITableViewRowAction, indexPath:IndexPath) in
            self.clearCells()
            self.remamingNumber = indexPath.row
            self.files.instrument = indexPath.row
            self.files.loadInstrument(index: indexPath.row)
            self.alertRename()
        }
        
        renameAction.backgroundColor = UIColor.lightGray
        deleteAction.backgroundColor = #colorLiteral(red: 0.6940000057, green: 0.3880000114, blue: 0.3840000033, alpha: 1)
        return [deleteAction, renameAction]
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        files.loadInstrument(index: indexPath.row)
        let selectedCell:UITableViewCell = tableView.cellForRow(at: indexPath)!
        selectedCell.contentView.backgroundColor = #colorLiteral(red: 0.4352941176, green: 0.3411764706, blue: 0.3450980392, alpha: 1)
    }
    
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        clearCells()
    }
    
    
    func clearCells()  {
        let count = files.instrumentFolderList.count
        for i in 0 ..< count {
            let indexPath = IndexPath(item: i, section: 0)
            instrumentTable.cellForRow(at: indexPath)?.contentView.backgroundColor = UIColor.clear
        }
    }
}
