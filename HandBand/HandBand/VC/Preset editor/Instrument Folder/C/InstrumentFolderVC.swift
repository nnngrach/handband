// Папка со всеми хранящимися на устройстве файлами с настройками инструментов.
import UIKit

class InstrumentFolderVC: UIViewController {
    @IBOutlet weak var instrumentTable: UITableView!
    
    let disk = HRFiles.shared
    let sets = GlobalSettings.singleton
    let files = HRFileOperations.shared
    let poster = NotificationCenter.default
    let postName = NSNotification.Name.self
    var remamingNumber = 0
    

    override func viewDidLoad() {
        super.viewDidLoad()
        instrumentTable.delegate = self
        instrumentTable.dataSource = self
    }

    override func viewDidDisappear(_ animated: Bool) {
        poster.post(name: postName.init("PadChanged"), object: nil)
    }
    
    
    
    
    
    @IBAction func addInstrumentButton(_ sender: Any) {
        files.addInstrument()
        let indexPath = IndexPath(item: 0, section: 0)
        instrumentTable.insertRows(at: [indexPath], with: .automatic)
        clearCells()
    }
    
    
    @IBAction func dublicateInstrumentButton(_ sender: Any) {
        files.dublicateInstrumentFile()
        let indexPath = IndexPath(item: 0, section: 0)
        instrumentTable.insertRows(at: [indexPath], with: .automatic)
        clearCells()
    }
    
    
    
    
    // MARK: - Alert view
    
    func alertRename() {
        let alert = UIAlertController(title: "Rename selected instrument", message: nil, preferredStyle: .alert)
        
        alert.addTextField(configurationHandler: { (textField) in
            textField.placeholder = "New instrument name"
        })
        
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            let myTextField = alert.textFields![0] as UITextField
            
            if self.disk.checkFile(subFolder1: "Instruments", subFolder2: nil, fileName: myTextField.text!) != nil {
                self.alertWrong()
            } else {
                self.files.deleteInstrumentFile(at: self.remamingNumber)
                self.files.exportInstrument(with: myTextField.text!)
                self.instrumentTable.reloadData()
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    func alertWrong() {
        let alert = UIAlertController(title: "Error", message: "A file with this name already exists", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            self.alertRename()
        }))
        self.present(alert, animated: true, completion: nil)
    }
}
