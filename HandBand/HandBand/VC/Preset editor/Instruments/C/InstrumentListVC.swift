// Список имеющихся в песне инструментов.
// Переход к настройкам выбранного инструмента.
import UIKit

class InstrumentListVC: UIViewController {
    let sets = GlobalSettings.singleton
    let disk = HRFiles.shared
    let files = HRFileOperations.shared
    let saves = UserDefaults.standard
    let poster = NotificationCenter.default
    let postName = NSNotification.Name.self
    
    var presetNumber = 0
    var showingPad = 0
    var instrument = 0
    @IBOutlet weak var instrumentsTableView: UITableView!
    @IBOutlet weak var container: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        instrumentsTableView.delegate = self
        instrumentsTableView.dataSource = self
        updateUI()
        poster.addObserver(self, selector: #selector(updateUI(n:)), name: postName.init("PresetChanged"), object: nil)
        poster.addObserver(self, selector: #selector(updateUI(n:)), name: postName.init("PadChanged"), object: nil)
        poster.addObserver(self, selector: #selector(smartScalesChanged(n:)), name: postName.init("smartScalesChanged"), object: nil)
        poster.post(name: postName.init("selectMode"), object: nil)
    }
    

    
    
    @objc func updateUI(n: NSNotification) {
        updateUI()
    }
    
    func updateUI() {
        presetNumber = files.preset
        showingPad = sets.showingPad
        instrument = sets.showingInstrument
        instrumentsTableView.reloadData()
    }
    
    
    // Измененть положение переключателя умного транспонирования с помощью сочетания клвиш
    @objc func smartScalesChanged(n: NSNotification) {
        if showingPad == 10 {
            DispatchQueue.main.async(execute: {
                self.instrumentsTableView.reloadData()
            })
        }
    }
    
    
    
    
 

    
    // MARK: - Make segue
    // Определяет индекс ячейки по координате на экране. Использует расширение TableView.
    
    @IBAction func buttonTapped(_ button: UIButton) {
        if let indexPath = self.instrumentsTableView.indexPathForView(button) {
            let row = indexPath.row
            let count = files.loadedSong.presetList[presetNumber].instrumentsMidiSets.count
            sets.showingInstrument = row
            
            // для строки Scale transform на пэде с белыми клавишами
            if row == count {
                let vc = storyboard?.instantiateViewController(withIdentifier: "ScaleTransformerVC")
                self.navigationController?.pushViewController(vc!, animated: true)
                
            // для всех остальных инструментов
            } else {
                let drummMode = files.loadedSong.presetList[presetNumber].instrumentsMidiSets[row].instrumentType
                
                if drummMode == 0 {
                    let vc = storyboard?.instantiateViewController(withIdentifier: "IntervalsVC")
                    self.navigationController?.pushViewController(vc!, animated: true)
                }else{
                    let vc = storyboard?.instantiateViewController(withIdentifier: "DrummerVC")
                    self.navigationController?.pushViewController(vc!, animated: true)
                }
            }
        }
        else {
            print("Button indexPath not found")
        }
        // print("Button tapped at indexPath \(indexPath)")
    }

    
    
    
    
    
    
    // MARK: - Alerts
    
    // Всплывающее окно  Экспортировать настройки инструмента
    func alertEnterName() {
        let alert = UIAlertController(title: "Save selected instrument to templates", message: "Enter name for new instrument", preferredStyle: .alert)
        
        
        alert.addTextField(configurationHandler: { (textField) in
            textField.text = self.files.loadedSong.presetList[self.presetNumber].instrumentsMidiSets[self.instrument].name
        })
        
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            let myTextField = alert.textFields![0] as UITextField
            let name = myTextField.text! + "." + self.files.instrumentFile
            
            if self.disk.checkFile(subFolder1: "Instruments", subFolder2: nil, fileName: name) != nil {
                self.alertWrong(filename: myTextField.text!)
            } else {
                self.files.exportInstrument(with: myTextField.text!)
                self.instrumentsTableView.reloadData()
                self.alertGood()
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    
    // Успешно
    func alertGood() {
        let alert = UIAlertController(title: "Succes", message: "The instrument is saved", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    // Неуспешно
    func alertWrong(filename: String) {
        let alert = UIAlertController(title: "Error", message: "A file with this name already exists", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Rename", style: .default, handler: { (action) in
            self.alertEnterName()
        }))
        
        alert.addAction(UIAlertAction(title: "Overwrite", style: .default, handler: { (action) in
            self.files.exportInstrument(with: filename)
            self.instrumentsTableView.reloadData()
            self.alertGood()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    
    // Переименовывание инструмента
    func alertRename() {
        let alert = UIAlertController(title: "Rename selected instrument", message: nil, preferredStyle: .alert)
        
        alert.addTextField(configurationHandler: { (textField) in
            textField.placeholder = "New instrument name"
        })
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            let myTextField = alert.textFields![0] as UITextField
            if myTextField.text != "" {
                self.files.loadedSong.presetList[self.presetNumber].instrumentsMidiSets[self.instrument].name = myTextField.text!
                self.updateUI()
                self.files.resaveSong()
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
   
    
    
    
    
    
    // MARK: - Buttons
    
    @IBAction func editBtn(_ sender: Any) {
        instrumentsTableView.isEditing = !instrumentsTableView.isEditing
    }
    
    
    
    @IBAction func addInstrumentButton(_ sender: Any) {
        files.insertInstrument()
        var count = self.instrumentsTableView.numberOfRows(inSection: 0)
        if showingPad == 10 {
            count -= 1
        }
        let indexPath = IndexPath(row: count, section: 0)
        instrumentsTableView.insertRows(at: [indexPath], with: .automatic)
        clearCells()
    }
    
    
    
    @IBAction func copyInstrumentButton(_ sender: Any) {
        files.dublicateInstrument()
        let indexPath = IndexPath(row: self.instrumentsTableView.numberOfRows(inSection: 0), section: 0)
        instrumentsTableView.insertRows(at: [indexPath], with: .automatic)
        clearCells()
    }
    
    
    @IBAction func midiPanic(_ sender: Any) {
        NoteGenerator.singleton.midiPanic()
    }
    
}
