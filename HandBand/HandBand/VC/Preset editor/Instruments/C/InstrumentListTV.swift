import UIKit
extension InstrumentListVC: UITableViewDelegate, UITableViewDataSource {
    
    
    // В паде № 10 (соло-клавиши) будет отображаться на одну строку больше.
    // Ее будет занимать выснесенный пункт из настроек "Включить умное транспонирование"
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if showingPad != 10 {
            return files.loadedSong.presetList[presetNumber].instrumentsMidiSets.count
        } else {
            return files.loadedSong.presetList[presetNumber].instrumentsMidiSets.count + 1
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = instrumentsTableView.dequeueReusableCell(withIdentifier: "cell") as! InstrumentListCell
        let listCount = files.loadedSong.presetList[presetNumber].instrumentsMidiSets.count
        
        switch indexPath.row {
        case listCount:
            // строка с настройками лада для белых клавиш
            
            cell.instrument = -1
            cell.label.text = "SCALE TRANSFORMER"
            cell.midiSetButton.isHidden = true
            cell.forwardButton.isHidden = false
            cell.switcher.isHidden = false
            cell.switcher.isOn = files.loadedSong.presetList[presetNumber].useSmartScales
            cell.infoButton.isHidden = false
            
        default:
            // обычные строки
            
            cell.instrument = indexPath.row
            cell.label.text = files.loadedSong.presetList[presetNumber].instrumentsMidiSets[indexPath.row].name
            cell.switcher.isOn = files.loadedSong.presetList[presetNumber].instrumentsMidiSets[indexPath.row].pads[showingPad].isOn
            
            cell.forwardButton.isHidden = false
            cell.midiSetButton.isHidden = false
            cell.infoButton.isHidden = true
        }
        
        return cell
    }
    
    
    
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let renameAction = UITableViewRowAction(style: .normal, title: "Rename") { (action:UITableViewRowAction, indexPath:IndexPath) in
            self.clearCells()
            self.instrument = indexPath.row
            self.alertRename()
        }
        
        let copyAction = UITableViewRowAction(style: .normal, title: "Copy") { (action:UITableViewRowAction, indexPath:IndexPath) in
            self.sets.showingInstrument = indexPath.row
            self.files.dublicateInstrument()
            var count = self.instrumentsTableView.numberOfRows(inSection: 0)
            if self.sets.showingPad == 10 {
                count -= 1
            }
            let indexPath = IndexPath(row: count, section: 0)
            self.instrumentsTableView.insertRows(at: [indexPath], with: .automatic)
            self.clearCells()
        }
        
        
        let loadAction = UITableViewRowAction(style: .normal, title: "Load") { (action:UITableViewRowAction, indexPath:IndexPath) in
            self.clearCells()
            self.instrument = indexPath.row
            self.sets.showingInstrument = indexPath.row
            self.performSegue(withIdentifier: "To Instrument Folder", sender: self)
        }
        
        
        let saveAction = UITableViewRowAction(style: .normal, title: "Save") { (action:UITableViewRowAction, indexPath:IndexPath) in
            self.clearCells()
            self.instrument = indexPath.row
            self.sets.showingInstrument = indexPath.row
            self.files.instrument = indexPath.row
            self.alertEnterName()
        }
        
        
        
        
        let deleteAction = UITableViewRowAction(style: .normal, title: "Delete") { (action:UITableViewRowAction, indexPath:IndexPath) in
            self.clearCells()
            self.files.deleteInstrument(at: indexPath.row)
            self.instrumentsTableView.deleteRows(at: [indexPath], with: .automatic)
            self.files.resaveSong()
        }
        
        
        
        renameAction.backgroundColor = UIColor.lightGray
        copyAction.backgroundColor = #colorLiteral(red: 0.5411764706, green: 0.6705882353, blue: 0.5529411765, alpha: 1)
        loadAction.backgroundColor = #colorLiteral(red: 0.4004528569, green: 0.5802476264, blue: 0.6941176471, alpha: 1)
        saveAction.backgroundColor = #colorLiteral(red: 0.2705882353, green: 0.3607843137, blue: 0.4823529412, alpha: 1)
        deleteAction.backgroundColor = #colorLiteral(red: 0.6940000057, green: 0.3880000114, blue: 0.3840000033, alpha: 1)
        
        
        if indexPath.row < self.files.loadedSong.presetList[presetNumber].instrumentsMidiSets.count {
            return [deleteAction, saveAction, loadAction, copyAction, renameAction]
        } else {
            return []
        }
    }
    
    

    
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let deletedRow = self.files.loadedSong.presetList[self.presetNumber].instrumentsMidiSets.remove(at: sourceIndexPath.row)
        self.files.loadedSong.presetList[self.presetNumber].instrumentsMidiSets.insert(deletedRow, at: destinationIndexPath.row)
        self.files.resaveSong()
    }
    
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }
    
    
    func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    
    
    func clearCells()  {
        let count = files.loadedSong.presetList[presetNumber].instrumentsMidiSets.count
        for i in 0 ..< count {
            let indexPath = IndexPath(item: i, section: 0)
            instrumentsTableView.cellForRow(at: indexPath)?.contentView.backgroundColor = UIColor.clear
        }
    }
    
    
    
    
    //    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    //        sets.showingInstrument = indexPath.row
    //        let selectedCell:UITableViewCell = tableView.cellForRow(at: indexPath)!
    //        selectedCell.contentView.backgroundColor = #colorLiteral(red: 0.4352941176, green: 0.3411764706, blue: 0.3450980392, alpha: 1)
    //    }
    //
    //    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
    //        clearCells()
    //        instrumentsTableView.reloadData()
    //    }
}
