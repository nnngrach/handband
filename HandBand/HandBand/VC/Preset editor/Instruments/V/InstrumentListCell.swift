import UIKit

class InstrumentListCell: UITableViewCell {
    let sets = GlobalSettings.singleton
    let files = HRFileOperations.shared
    let saves = UserDefaults.standard
    let poster = NotificationCenter.default
    let postName = NSNotification.Name.self
    var instrument = 0
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var switcher: UISwitch!
    @IBOutlet weak var midiSetButton: UIButton!
    @IBOutlet weak var infoButton: UIButton!
    @IBOutlet weak var forwardButton: UIButton!
    
    
    
    @IBAction func swinthAct(_ sender: Any) {
        let preset = files.preset
        let pad = sets.showingPad
        if instrument != -1 {
            files.loadedSong.presetList[preset].instrumentsMidiSets[instrument].pads[pad].isOn = switcher.isOn
        } else {
            files.loadedSong.presetList[preset].useSmartScales = switcher.isOn
            ScaleTransformer.singleton.calculateCurrentScale()
            NotificationCenter.default.post(name: NSNotification.Name.init("smartScalesChanged"),  object: nil)
        }
        files.resaveSong()
    }
    
    
    @IBAction func gotoInstrumenMidiSet(_ sender: Any) {
        sets.showingInstrument = instrument
    }
    

    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
