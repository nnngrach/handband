// Настройки стиля игры для мелодических инструментов

import UIKit

class StyleInstrumentVC: UIViewController{
    
    @IBOutlet weak var octave: UISegmentedControl!
    @IBOutlet weak var inversion: UISegmentedControl!
    @IBOutlet weak var volume: UISlider!
    @IBOutlet weak var picker: UIPickerView!
    
    let sets = GlobalSettings.singleton
    let files = HRFileOperations.shared
    let poster = NotificationCenter.default
    let postName = NSNotification.Name.self
    
    var preset = 0
    var pad = 0
    var instrument = 0
    
    var style = ["Unison", "Third", "Fifth", "Octave",
                 "Power chord", "3 notes chord ", "4 notes chord",  "Wide chord",
                 "Very wide chord", "Guitar's like chords"]
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
        poster.post(name: postName.init("blockMode"), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        poster.post(name: postName.init("selectMode"), object: nil)
    }
    
    
    func updateUI() {
        preset = files.preset
        pad = sets.showingPad
        instrument = sets.showingInstrument
        
        octave.selectedSegmentIndex = files.loadedSong.presetList[preset].instrumentsMidiSets[instrument].pads[pad].relativeOctave
        inversion.selectedSegmentIndex = files.loadedSong.presetList[preset].instrumentsMidiSets[instrument].pads[pad].inversion
        volume.value = files.loadedSong.presetList[preset].instrumentsMidiSets[instrument].pads[pad].relativeVelocity
        
        picker.selectRow(files.loadedSong.presetList[preset].instrumentsMidiSets[instrument].pads[pad].style,
                         inComponent: 0, animated: true)
    }
    
    
    
 

    
    
    @IBAction func octaveAct(_ sender: Any) {
        files.loadedSong.presetList[preset].instrumentsMidiSets[instrument].pads[pad].relativeOctave = octave.selectedSegmentIndex
        files.resaveSong()
    }
    
    @IBAction func inversionAct(_ sender: Any) {
        files.loadedSong.presetList[preset].instrumentsMidiSets[instrument].pads[pad].inversion = inversion.selectedSegmentIndex
        files.resaveSong()
    }
    
    @IBAction func volumeAct(_ sender: Any) {
        files.loadedSong.presetList[preset].instrumentsMidiSets[instrument].pads[pad].relativeVelocity = volume.value
        files.resaveSong()
    }
    
}
