// Подгрузка NIB файла с табличкой для генерации клавишной миниатюры

import UIKit

class ChordPiano: UIView {
    
    @IBOutlet weak var chordsLabel: UILabel!
    @IBOutlet var buttons: [HRView]!
    
    @IBOutlet weak var b1: HRView!
    @IBOutlet weak var b2: HRView!
    
    
    
    var keys = HRKeyEquality.singleton
    var nibName: String = "ChordPiano"
    var view: UIView!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func loadFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return view
    }
    
    
    func setup (label: String, data: Int) {
        view = loadFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
//        print("- ", label)
//        chordsLabel.text = label
//        let combo = keys.combo[label]![0] as! [Int]
//        for key in buttons {
//            if combo[key.tag] == 1 {
//                key.backgroundColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
//            }
//        }
        
        b1.backgroundColor = UIColor.red
        addSubview(view)
    }
}
