// Подгрузка NIB файла с табличкой для генерации барабанной миниатюры

import UIKit

@IBDesignable class ChordPad: UIView {
    
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var lavelOneViev: HRView!
    @IBOutlet weak var labelTwoView: HRView!
    @IBOutlet weak var labelOne: UILabel!
    @IBOutlet weak var labelTwo: UILabel!
    @IBOutlet var padsCells: [HRView]!

    var keys = HRKeyEquality.singleton
    var nibName: String = "ChordPad"
    var view: UIView!
    
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    func loadFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return view
    }
    

    
    
    // Настройка таблички.
    // Для разных аккордов - разный тип отрисовки
    func setup(type: Int, label: String, data: Int) {
        view = loadFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]

        switch type {
        case 1:
            lavelOneViev.isHidden = false
            labelOne.text = label
            if label.hasSuffix("g") {
                labelOne.font = UIFont(name: "HelveticaNeue", size: 12.0)
            } else {
                labelOne.font = UIFont(name: "HelveticaNeue", size: 14.0)
            }
            draw(chord: label)
            
        case 2:
            labelTwoView.isHidden = false
            labelTwo.text =  label
            labelTwo.font = UIFont(name: "HelveticaNeue", size: 14.0)
            draw(chord: label)
            
        case 3:
            lavelOneViev.isHidden = false
            switch data {
            case ...(-10) :
                labelOne.text = "Transp " + "\(data)"
                labelOne.font = UIFont(name: "HelveticaNeue", size: 11.0)
                draw(chord: "Transp -")
            case (-9)...(-1) :
                labelOne.text = "Transp " + "\(data)"
                labelOne.font = UIFont(name: "HelveticaNeue", size: 12.0)
                draw(chord: "Transp -")
            case 1...9 :
                labelOne.text = "Transp +" + "\(data)"
                labelOne.font = UIFont(name: "HelveticaNeue", size: 12.0)
                draw(chord: "Transp +")
            case 10... :
                labelOne.text = "Transp +" + "\(data)"
                labelOne.font = UIFont(name: "HelveticaNeue", size: 11.0)
                draw(chord: "Transp +")
            default:
                labelOne.text = "Transp 0"
                labelOne.font = UIFont(name: "HelveticaNeue", size: 13.0)
                draw(chord: "Transp 0")
            }

        default:
            mainView.isHidden = true
            draw(chord: "- - -- - -")
        }


        addSubview(view)
    }
    
    

    
    // Отрисовка настроенной миниатюры
    func draw(chord: String) {
        if let value = keys.comboChords[chord] {
            let combo = value[0] as! [Int]
            
            mainView.isHidden = false
            
            for i in 0 ..< padsCells.count {
                if chord == "- - -- - -" {
                    padsCells[i].isHidden = true
                    lavelOneViev.isHidden = true
                    labelTwoView.isHidden = true
                    
                } else if combo[i] == 1 {
                    padsCells[i].backgroundColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
                }
            }
            
            
        } else {
            mainView.isHidden = true
        }
    }

    
}
