// Скрытая утилита
// для генерации миниатюр аккордов
// в и сохранения их в формате PDF

import UIKit

class PngGeneratorVC: UIViewController {
    
    let disk = HRFiles.shared
    let k = HRKeyEquality.singleton
    @IBOutlet weak var drumpadView: ChordPad!
    @IBOutlet weak var pianoView: ChordPiano!
    @IBOutlet weak var hbView: UIView!
    
    @IBOutlet weak var pianoLabel: UILabel!
    @IBOutlet var pianoKeys: [HRView]!
    @IBOutlet var pianoBlackKeys: [HRView]!
    
    @IBOutlet weak var hbLabel: UILabel!
    @IBOutlet var hbKeys: [HRView]!
    @IBOutlet var hbBlackKeys: [HRView]!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    

    // Запустить генерацию
    @IBAction func drumpadGenerate(_ sender: UIButton) {
        disk.printDocsPatch()
        mkDir()
        iterateChords()
        //savePngWith(name: "NewImage")
    }
    

    
    
    
    // MARK: Алгоритм генерации миниатюр для разных режимов
    func iterateChords() {
        for chord in k.comboChords {
            switch chord.key {
            case "Sharp", "Preset sound", "Chord OFF", "Transp +", "Transp -", "Transp 0", "Preset 5 keys", "Preset 6 keys", "Preset 7 keys", "Preset 8 keys":
                break
                
            case "Preset 1", "Preset 2","Preset 3","Preset 4":
                drumpadView.setup(type: 2, label: chord.key, data: 0)
                saveDrumPngWith(name: chord.key)
                pianoSetup(label: chord.key, data: 0)
                savePianoPngWith(name: chord.key)
                
            case "Preset 5","Preset 6","Preset 7","Preset 8":
                drumpadView.setup(type: 2, label: chord.key, data: 0)
                saveDrumPngWith(name: chord.key)
                pianoSetup(label: chord.key, data: 0)
                savePianoPngWith(name: chord.key)
                
                
            default:
                drumpadView.setup(type: 1, label: chord.key, data: 0)
                saveDrumPngWith(name: chord.key)
                pianoSetup(label: chord.key, data: 0)
                savePianoPngWith(name: chord.key)
                print("")
            }
        }
        
        for i in -12 ... 12 {
            drumpadView.setup(type: 3, label: "Transpose", data: i)
            saveDrumPngWith(name: "Transp " + "\(i)")
            pianoSetup(label: "Transpose", data: i)
            savePianoPngWith(name: "Transp " + "\(i)")
        }
    }
    
    

    
    
    
    
    // Дополнительные настройки для фортепианных миниатюр
    func pianoSetup(label: String, data: Int) {
        pianoLabel.text = label
        hbLabel.text = label
        
        var newLabel = label
        let differentCombos = ["Preset 5", "Preset 6", "Preset 7", "Preset 8"]
        if differentCombos.contains(label) {
            newLabel = label + " keys"
            hbLabel.text = label + " keys"
        }
        
        if label == "Transpose" {
            pianoLabel.text = "Transp " + "\(data)"
            hbLabel.text = "Transp " + "\(data)"
            
            switch data {
            case 0:
                newLabel = "Transp 0"
            case ...0:
                newLabel = "Transp -"
            case 0...:
                newLabel = "Transp +"
            default:
                break
            }
        }
        
        let combo = k.comboChords[newLabel]![0] as! [Int]
        for key in pianoKeys {
            if combo[key.tag] == 1 {
                key.backgroundColor = #colorLiteral(red: 0.3348928287, green: 0.360532436, blue: 0.3950891693, alpha: 1)
            } else {
                key.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            }
        }
        
        for key in pianoBlackKeys {
            if combo[key.tag] == 1 {
                key.backgroundColor = #colorLiteral(red: 0.2116911978, green: 0.2279751342, blue: 0.2507726451, alpha: 1)
            } else {
                key.backgroundColor = #colorLiteral(red: 0.6666166186, green: 0.6666959524, blue: 0.6665801406, alpha: 1)
            }
        }
        
        for key in hbKeys {
            if combo[key.tag] == 1 {
                key.backgroundColor = #colorLiteral(red: 0.3348928287, green: 0.360532436, blue: 0.3950891693, alpha: 1)
            } else {
                key.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            }
        }
        
        for key in hbBlackKeys {
            if combo[key.tag] == 1 {
                key.backgroundColor = #colorLiteral(red: 0.2116911978, green: 0.2279751342, blue: 0.2507726451, alpha: 1)
            } else {
                key.backgroundColor = #colorLiteral(red: 0.6666166186, green: 0.6666959524, blue: 0.6665801406, alpha: 1)
            }
        }
        
        
        
        
    }
    
    
    
    
    
    // MARK: Сохранение сгенерированных файлов
    
    
    // Создать директурию для сохранения
    func mkDir() {
        if disk.checkFile(subFolder1: nil, subFolder2: nil, fileName: "Images") == nil {
            print("MkDir")
            disk.createDirectory(subFolder: nil, folderName: "Images")
        }
    }
    
    
    func saveDrumPngWith(name:String) {
        let filename = "d " + name + ".png"
        let pngData = drumpadView.convetToPng()
        //let pngData = testSquare.convetToPng()
        disk.saveData(data: pngData, fileName: filename, subFolder1: "Images", subFolder2: nil)
    }
    
    
    func savePianoPngWith(name:String) {
        let filename = "p " + name + ".png"
        let pngData = pianoView.convetToPng()
        disk.saveData(data: pngData, fileName: filename, subFolder1: "Images", subFolder2: nil)
        
        let filename2 = "h " + name + ".png"
        let pngData2 = hbView.convetToPng()
        disk.saveData(data: pngData2, fileName: filename2, subFolder1: "Images", subFolder2: nil)
    }

}
